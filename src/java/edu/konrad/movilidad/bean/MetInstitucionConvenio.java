/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */
public class MetInstitucionConvenio {
    
    private String pk;
    private String codigo;
   // private MetInfUsuario metInfUsuario;
    private String nombre;
    private String existeConvenio;
    private MetPais metPais;
    private MetCiudad metCiudad;
    private String otraCiudad;
    private String nombreContacto;
    private String apellidoContacto;
    private String cargoContacto;
    private String emailContacto;
    private String telContacto;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
       
    public MetInstitucionConvenio(){   
    /*    this.ciudad = new Ciudad();*/
    }
    
    public String getTelContacto() {
        return telContacto;
    }

    public void setTelContacto(String telContacto) {
        this.telContacto = telContacto;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public MetPais getMetPais() {
        return metPais;
    }

    public void setMetPais(MetPais metPais) {
        this.metPais = metPais;
    }

    public MetCiudad getMetCiudad() {
        return metCiudad;
    }

    public void setMetCiudad(MetCiudad metCiudad) {
        this.metCiudad = metCiudad;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getExisteConvenio() {
        return existeConvenio;
    }

    public void setExisteConvenio(String existeConvenio) {
        this.existeConvenio = existeConvenio;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getApellidoContacto() {
        return apellidoContacto;
    }

    public void setApellidoContacto(String apellidoContacto) {
        this.apellidoContacto = apellidoContacto;
    }

    public String getCargoContacto() {
        return cargoContacto;
    }

    public void setCargoContacto(String cargoContacto) {
        this.cargoContacto = cargoContacto;
    }

    public String getEmailContacto() {
        return emailContacto;
    }

    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }

  
    public String getOtraCiudad() {
        return otraCiudad;
    }

    public void setOtraCiudad(String otraCiudad) {
        this.otraCiudad = otraCiudad;
    }
 
    
}
