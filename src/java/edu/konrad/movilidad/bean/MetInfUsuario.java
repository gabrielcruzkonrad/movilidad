/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

import java.util.Date;

/**
 *
 * @author Jose.suspes
 */
public class MetInfUsuario {
    
    private String usuarioPK;
    private String usuario;
    private String contrasenia;
    private String nombres;
    private String apellidos;
    private MetGenero metGenero;
    private MetTipoIdentificacion metTipoIdentificacion;
    private String otroDocIdentificacion;
    private String numeroIdentificacion;
    private MetPais metPais;
   // private MetCiudad metCiudadNacimiento ;
    private String ciudadNacimiento;
    private String otraCiudad;
    private String nacionalidad;
    private Date fechaNacimiento;
    private String direccionResidencia;
    private String telCasa;
    private String celular;
    private String emailInstitucional;
    private String emailAlternativo;
    private String tipoSangre;
    private String estado;
    private MetRolUsuario metRolUsuario;
    private String OtraCiudadNacimiento;
    private MetTipoSolicitud metTipoSolicitud;
    private MetSolicitud metsolicitud;
    private String codigoEstudiantil;
    private String lugarNacimiento;
    
    /*Saliente*/
    private String programaEstSaliente;
    private String facultadEstSaliente;
    private String semestreEstSaliente;
    private String tipoPrograma;
    private String lugarExpedicion;
   
    
    public MetInfUsuario() {
        this.metGenero = new MetGenero();
        this.metTipoIdentificacion = new MetTipoIdentificacion();
      //  this.metCiudadNacimiento = new MetCiudad();
        this.metPais = new MetPais();
        this.metTipoSolicitud = new MetTipoSolicitud();
        this.metRolUsuario = new MetRolUsuario();
    }

    public String getTipoPrograma() {
        return tipoPrograma;
    }

    public void setTipoPrograma(String tipoPrograma) {
        this.tipoPrograma = tipoPrograma;
    }

    public String getSemestreEstSaliente() {
        return semestreEstSaliente;
    }

    public void setSemestreEstSaliente(String semestreEstSaliente) {
        this.semestreEstSaliente = semestreEstSaliente;
    }

    public String getProgramaEstSaliente() {
        return programaEstSaliente;
    }

    public void setProgramaEstSaliente(String programaEstSaliente) {
        this.programaEstSaliente = programaEstSaliente;
    }

    public String getFacultadEstSaliente() {
        return facultadEstSaliente;
    }

    public void setFacultadEstSaliente(String facultadEstSaliente) {
        this.facultadEstSaliente = facultadEstSaliente;
    }
    
    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }
   
    public String getOtraCiudadNacimiento() {
        return OtraCiudadNacimiento;
    }

    public void setOtraCiudadNacimiento(String OtraCiudadNacimiento) {
        this.OtraCiudadNacimiento = OtraCiudadNacimiento;
    }

    public String getCodigoEstudiantil() {
        return codigoEstudiantil;
    }

    public void setCodigoEstudiantil(String codigoEstudiantil) {
        this.codigoEstudiantil = codigoEstudiantil;
    }

    public MetSolicitud getMetsolicitud() {
        return metsolicitud;
    }

    public void setMetsolicitud(MetSolicitud metsolicitud) {
        this.metsolicitud = metsolicitud;
    }

    public MetTipoSolicitud getMetTipoSolicitud() {
        return metTipoSolicitud;
    }

    public void setMetTipoSolicitud(MetTipoSolicitud metTipoSolicitud) {
        this.metTipoSolicitud = metTipoSolicitud;
    } 

    public MetPais getMetPais() {
        return metPais;
    }

    public void setMetPais(MetPais metPais) {
        this.metPais = metPais;
    }

    public MetRolUsuario getMetRolUsuario() {
        return metRolUsuario;
    }

    public void setMetRolUsuario(MetRolUsuario metRolUsuario) {
        this.metRolUsuario = metRolUsuario;
    }

   /* public MetCiudad getMetCiudadNacimiento() {
        return metCiudadNacimiento;
    }

    public void setMetCiudadNacimiento(MetCiudad metCiudadNacimiento) {
        this.metCiudadNacimiento = metCiudadNacimiento;
    }*/

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public MetGenero getMetGenero() {
        return metGenero;
    }

    public void setMetGenero(MetGenero metGenero) {
        this.metGenero = metGenero;
    }

    public MetTipoIdentificacion getMetTipoIdentificacion() {
        return metTipoIdentificacion;
    }

    public void setMetTipoIdentificacion(MetTipoIdentificacion metTipoIdentificacion) {
        this.metTipoIdentificacion = metTipoIdentificacion;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getUsuarioPK() {
        return usuarioPK;
    }

    public void setUsuarioPK(String usuarioPK) {
        this.usuarioPK = usuarioPK;
    }

    public String getOtroDocIdentificacion() {
        return otroDocIdentificacion;
    }

    public void setOtroDocIdentificacion(String otroDocIdentificacion) {
        this.otroDocIdentificacion = otroDocIdentificacion;
    }

    public String getOtraCiudad() {
        return otraCiudad;
    }

    public void setOtraCiudad(String otraCiudad) {
        this.otraCiudad = otraCiudad;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDireccionResidencia() {
        return direccionResidencia;
    }

    public void setDireccionResidencia(String direccionResidencia) {
        this.direccionResidencia = direccionResidencia;
    }

    public String getTelCasa() {
        return telCasa;
    }

    public void setTelCasa(String telCasa) {
        this.telCasa = telCasa;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmailInstitucional() {
        return emailInstitucional;
    }

    public void setEmailInstitucional(String emailInstitucional) {
        this.emailInstitucional = emailInstitucional;
    }

    public String getEmailAlternativo() {
        return emailAlternativo;
    }

    public void setEmailAlternativo(String emailAlternativo) {
        this.emailAlternativo = emailAlternativo;
    }

    public String getTipoSangre() {
        return tipoSangre;
    }

    public void setTipoSangre(String tipoSangre) {
        this.tipoSangre = tipoSangre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getLugarExpedicion() {
        return lugarExpedicion;
    }

    public void setLugarExpedicion(String lugarExpedicion) {
        this.lugarExpedicion = lugarExpedicion;
    }   

    public String getCiudadNacimiento() {
        return ciudadNacimiento;
    }

    public void setCiudadNacimiento(String ciudadNacimiento) {
        this.ciudadNacimiento = ciudadNacimiento;
    }
    
    
}
