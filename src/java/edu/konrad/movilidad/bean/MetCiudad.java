/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */
public class MetCiudad {
    
    public String pk;
    public MetPais pais;
    public String nombre;
    public String ciudadDistrito;

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public MetPais getPais() {
        return pais;
    }

    public void setPais(MetPais pais) {
        this.pais = pais;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCiudadDistrito() {
        return ciudadDistrito;
    }

    public void setCiudadDistrito(String ciudadDistrito) {
        this.ciudadDistrito = ciudadDistrito;
    }
    
    
}
