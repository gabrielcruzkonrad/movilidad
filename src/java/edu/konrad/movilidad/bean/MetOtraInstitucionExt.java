/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */

public class MetOtraInstitucionExt {
    private MetSolicitud metSolicitud;
    private MetPais metPais;
    private String nombreInstitucion;
    private String ciudadInstitucion;
    private String nombreContacto;
    private String cargoContacto;
    private String emailContacto;
    private String telContacto;
    private String existeConvenio;
    
    
    public MetOtraInstitucionExt() {
        this.metSolicitud = new MetSolicitud();
        this.metPais = new MetPais();
    }

    public MetSolicitud getMetSolicitud() {
        return metSolicitud;
    }

    public void setMetSolicitud(MetSolicitud metSolicitud) {
        this.metSolicitud = metSolicitud;
    }

    public MetPais getMetPais() {
        return metPais;
    }

    public void setMetPais(MetPais metPais) {
        this.metPais = metPais;
    }

    public String getCiudadInstitucion() {
        return ciudadInstitucion;
    }

    public void setCiudadInstitucion(String ciudadInstitucion) {
        this.ciudadInstitucion = ciudadInstitucion;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getCargoContacto() {
        return cargoContacto;
    }

    public void setCargoContacto(String cargoContacto) {
        this.cargoContacto = cargoContacto;
    }

    public String getEmailContacto() {
        return emailContacto;
    }

    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }

    public String getTelContacto() {
        return telContacto;
    }

    public void setTelContacto(String telContacto) {
        this.telContacto = telContacto;
    }

    public String getNombreInstitucion() {
        return nombreInstitucion;
    }

    public void setNombreInstitucion(String nombreInstitucion) {
        this.nombreInstitucion = nombreInstitucion;
    }

    public String getExisteConvenio() {
        return existeConvenio;
    }

    public void setExisteConvenio(String existeConvenio) {
        this.existeConvenio = existeConvenio;
    }

    
}
