/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */
public class MetInfoCursoKL {
       
    String pk;
    MetSolicitud metSolicitud;
    MetCursosKL metCursoKL;
    String nombre;
    String codigo;
    String creditos;
    String estado;

    public MetInfoCursoKL(){
        this.metSolicitud = new MetSolicitud();
        this.metCursoKL = new MetCursosKL();
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCreditos() {
        return creditos;
    }

    public void setCreditos(String creditos) {
        this.creditos = creditos;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public MetSolicitud getMetSolicitud() {
        return metSolicitud;
    }

    public void setMetSolicitud(MetSolicitud metSolicitud) {
        this.metSolicitud = metSolicitud;
    }

    public MetCursosKL getMetCursoKL() {
        return metCursoKL;
    }

    public void setMetCursoKL(MetCursosKL metCursoKL) {
        this.metCursoKL = metCursoKL;
    }
   
    
}
