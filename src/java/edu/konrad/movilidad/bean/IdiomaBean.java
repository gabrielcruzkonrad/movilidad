/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

/**
 *
 * @author leidy.sarmiento
 */
@ManagedBean(name = "idiomaBean")
@SessionScoped
public class IdiomaBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private String codigoIdioma, varIdioma;

    private Map<String, Object> listaIdiomas;

    public IdiomaBean() {
        super();
        this.codigoIdioma = FacesContext.getCurrentInstance().getViewRoot().getLocale().getLanguage();
        this.listaIdiomas = new LinkedHashMap<String, Object>();
        this.listaIdiomas.put("Español", new Locale("es"));
        this.listaIdiomas.put("Inglés", new Locale("en"));
    }

    public String getCodigoIdioma() {
        return codigoIdioma;
    }

    public void setCodigoIdioma(String codigoIdioma) {
        this.codigoIdioma = codigoIdioma;
    }

    public Map<String, Object> getListaIdiomas() {
        return listaIdiomas;
    }

    public String getVarIdioma() {
        return varIdioma;
    }

    public void setVarIdioma(String varIdioma) {
        this.varIdioma = varIdioma;
    }

    public void doCambioIdiomaLink(String nuevoIdioma) {
        for (Map.Entry<String, Object> entry : listaIdiomas.entrySet()) {
            if (entry.getValue().toString().equals(nuevoIdioma)) {
                this.codigoIdioma = nuevoIdioma;
                FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale) entry.getValue());
            }
        }
    }

    /* public void doCambioIdiomaConLista(ValueChangeEvent e) {
        String newCodigoIdioma = e.getNewValue().toString();
        System.out.println("newCodigoIdioma=" + newCodigoIdioma);
        System.out.println("idiomaBean=" + this.toString());

        //loop country map to compare the locale code
        for (Map.Entry<String, Object> entry : listaIdiomas.entrySet()) {

            if (entry.getValue().toString().equals(newCodigoIdioma)) {
                System.out.println("Asignando nueva locale al contexto de Faces.");
                FacesContext.getCurrentInstance().getViewRoot().setLocale((Locale) entry.getValue());
            }
        }
    }*/
 /* @Override
    public String toString() {
        return "IdiomaBean [codigoIdioma=" + codigoIdioma + ", listaIdiomas=" + listaIdiomas + "]";
    }*/
}
