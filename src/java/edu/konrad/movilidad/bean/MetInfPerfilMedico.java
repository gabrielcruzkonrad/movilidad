/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */
public class MetInfPerfilMedico {
    private String pk;
    private MetSolicitud metSolicitud;
    private String presenatAlteracion;

    public MetInfPerfilMedico(){
        this.metSolicitud = new MetSolicitud();
    }
    
    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public MetSolicitud getMetSolicitud() {
        return metSolicitud;
    }

    public void setMetSolicitud(MetSolicitud metSolicitud) {
        this.metSolicitud = metSolicitud;
    }

    public String getPresenatAlteracion() {
        return presenatAlteracion;
    }

    public void setPresenatAlteracion(String presenatAlteracion) {
        this.presenatAlteracion = presenatAlteracion;
    }    
    
}
