/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

import java.util.Date;

/**
 *
 * @author leidy.sarmiento
 */
public class MetObservacionSolicitud {
    
    private String pk;
    private MetSolicitud metSolicitud;
    private String observacion;
    private Date fechaObservacion;
    private MetInfUsuario usuarioTransRealizada;
    private String usuarioObsRealizada;
    
    public MetObservacionSolicitud(){
        this.metSolicitud = new MetSolicitud();
    }

    public String getUsuarioObsRealizada() {
        return usuarioObsRealizada;
    }

    public void setUsuarioObsRealizada(String usuarioObsRealizada) {
        this.usuarioObsRealizada = usuarioObsRealizada;
    }  
    
    public MetInfUsuario getUsuarioTransRealizada() {
        return usuarioTransRealizada;
    }

    public void setUsuarioTransRealizada(MetInfUsuario usuarioTransRealizada) {
        this.usuarioTransRealizada = usuarioTransRealizada;
    }
   
    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public MetSolicitud getMetSolicitud() {
        return metSolicitud;
    }

    public void setMetSolicitud(MetSolicitud metSolicitud) {
        this.metSolicitud = metSolicitud;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechaObservacion() {
        return fechaObservacion;
    }

    public void setFechaObservacion(Date fechaObservacion) {
        this.fechaObservacion = fechaObservacion;
    }
    
}
