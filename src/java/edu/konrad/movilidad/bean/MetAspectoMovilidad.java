/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */
public class MetAspectoMovilidad {
    
    private String pk;
    private String codigo;
    private String orden;
    private String nombre;
    private String estado;
    private String descripcion;
    private MetInformacionAdicional metInfAdicional;

    /*public MetAspectoMovilidad(){
        this.metInfAdicional = new MetInformacionAdicional();
    }*/
    
    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public MetInformacionAdicional getMetInfAdicional() {
        return metInfAdicional;
    }

    public void setMetInfAdicional(MetInformacionAdicional metInfAdicional) {
        this.metInfAdicional = metInfAdicional;
    }
    
    
    
}
