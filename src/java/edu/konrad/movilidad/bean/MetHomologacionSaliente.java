/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */
public class MetHomologacionSaliente {

    String pk;
    MetProgramaInstOrigen metProgramaInstOrigen;
    MetCursosKL metCursosKL;
    MetSolicitud metSolicitud;
    String namHomologacion;
    String descripcion;
    String SoporteAdjunto;
    String estado;
    
    public MetHomologacionSaliente(){
        this.metProgramaInstOrigen = new MetProgramaInstOrigen();
        this.metSolicitud = new MetSolicitud();
        this.metCursosKL = new MetCursosKL();
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getSoporteAdjunto() {
        return SoporteAdjunto;
    }

    public void setSoporteAdjunto(String SoporteAdjunto) {
        this.SoporteAdjunto = SoporteAdjunto;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }
    
    public MetProgramaInstOrigen getMetProgramaInstOrigen() {
        return metProgramaInstOrigen;
    }

    public void setMetProgramaInstOrigen(MetProgramaInstOrigen metProgramaInstOrigen) {
        this.metProgramaInstOrigen = metProgramaInstOrigen;
    }

    public MetCursosKL getMetCursosKL() {
        return metCursosKL;
    }

    public void setMetCursosKL(MetCursosKL metCursosKL) {
        this.metCursosKL = metCursosKL;
    }

    public MetSolicitud getMetSolicitud() {
        return metSolicitud;
    }

    public void setMetSolicitud(MetSolicitud metSolicitud) {
        this.metSolicitud = metSolicitud;
    }

    public String getNamHomologacion() {
        return namHomologacion;
    }

    public void setNamHomologacion(String namHomologacion) {
        this.namHomologacion = namHomologacion;
    }
    
}
