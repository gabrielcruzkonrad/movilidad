/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

import java.util.Date;

/**
 *
 * @author Jose.suspes
 */
public class MetRolUsuario {
    private String pk;
    private String Codigo;
    private String nombre;
    private Date NamVigenciaUsuarioDesde;
    private Date NamVigenciaUsuarioHasta;

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getCodigo() {
        return Codigo;
    }

    public void setCodigo(String Codigo) {
        this.Codigo = Codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
   
    public Date getNamVigenciaUsuarioDesde() {
        return NamVigenciaUsuarioDesde;
    }

    public void setNamVigenciaUsuarioDesde(Date NamVigenciaUsuarioDesde) {
        this.NamVigenciaUsuarioDesde = NamVigenciaUsuarioDesde;
    }

    public Date getNamVigenciaUsuarioHasta() {
        return NamVigenciaUsuarioHasta;
    }

    public void setNamVigenciaUsuarioHasta(Date NamVigenciaUsuarioHasta) {
        this.NamVigenciaUsuarioHasta = NamVigenciaUsuarioHasta;
    }
    
    
    
    
}
