/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */
public class MetInfoIdioma {

    private String pk;
    private MetSolicitud metSolicitud;
    //private MetInfoIdioma metIdioma;
    private String idiomaNativo;
    private String porcentajeHabla;
    private String porcentajeEscribe;
    private String porcentajeLee;
    private String codigo;
    private String namDescripcion;

    public MetInfoIdioma() {
        this.metSolicitud = new MetSolicitud();
       // this.metIdioma = new MetInfoIdioma();
    }

    public String getNamDescripcion() {
        return namDescripcion;
    }

    public void setNamDescripcion(String namDescripcion) {
        this.namDescripcion = namDescripcion;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public MetSolicitud getMetSolicitud() {
        return metSolicitud;
    }

    public void setMetSolicitud(MetSolicitud metSolicitud) {
        this.metSolicitud = metSolicitud;
    }

    public String getPorcentajeHabla() {
        return porcentajeHabla;
    }

    public void setPorcentajeHabla(String porcentajeHabla) {
        this.porcentajeHabla = porcentajeHabla;
    }

    public String getPorcentajeEscribe() {
        return porcentajeEscribe;
    }

    public void setPorcentajeEscribe(String porcentajeEscribe) {
        this.porcentajeEscribe = porcentajeEscribe;
    }

    public String getPorcentajeLee() {
        return porcentajeLee;
    }

    public void setPorcentajeLee(String porcentajeLee) {
        this.porcentajeLee = porcentajeLee;
    }

    public String getIdiomaNativo() {
        return idiomaNativo;
    }

    public void setIdiomaNativo(String idiomaNativo) {
        this.idiomaNativo = idiomaNativo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

}
