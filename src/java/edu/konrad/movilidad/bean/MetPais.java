/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

public class MetPais {

    public String pk;
    public String codigo;
    public String codigoPostal;
    public String nombre;
    

    public String nombreOtroIdioma;
    public String nombreEspaniol;
    public String continente;
    public String region;

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombreOtroIdioma() {
        return nombreOtroIdioma;
    }

    public void setNombreOtroIdioma(String nombreOtroIdioma) {
        this.nombreOtroIdioma = nombreOtroIdioma;
    }

    public String getNombreEspaniol() {
        return nombreEspaniol;
    }

    public void setNombreEspaniol(String nombreEspaniol) {
        this.nombreEspaniol = nombreEspaniol;
    }

    public String getContinente() {
        return continente;
    }

    public void setContinente(String continente) {
        this.continente = continente;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

}
