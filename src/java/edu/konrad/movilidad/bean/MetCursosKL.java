 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */
public class MetCursosKL {
    
    String pk;
    String codigo;
    String nombre;
    String creditos;
    MetProgramaKl metProgramaKL;
    
     public MetCursosKL(){
        this.metProgramaKL = new MetProgramaKl();
    }
    
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCreditos() {
        return creditos;
    }

    public void setCreditos(String creditos) {
        this.creditos = creditos;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public MetProgramaKl getMetProgramaKL() {
        return metProgramaKL;
    }

    public void setMetProgramaKL(MetProgramaKl metProgramaKL) {
        this.metProgramaKL = metProgramaKL;
    }
    
    
}
