/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

import edu.konrad.movilidad.controller.InicioMB;
import java.util.Date;

/**
 *
 * @author Jose.suspes
 */
public class MetSolicitud {

    private String solicitudPK;
    private Date fechaSolicitud;
    private String consecutivo;
    private String varFechaSolicitud;
    private MetInfUsuario metInfUsuario;
    private MetInstitucionConvenio metInstitucionConvenio;
    private String otraInstitucionEntrante;
    private MetFacultadInstOrigen metFacultadInstConvenio;
    private String otraFacultadEntrante;
    private MetProgramaInstOrigen metProgramaInstOrigen; /*Programa institucion externa*/
    private String otroProgramaInstConvenio;
    private MetProgramaKl metProgramaKl;
    private MetTipoSolicitud metTipoSolicitud;
    private MetTipoMovilidad metTipoMovilidad;
    private String otroTipoMoviidad;
    private String promedioAcumulado;
    private String promedioUltSemestre;
    private Date fechaInicioMovilidad;
    private Date fechaFinMovilidad;
    private String duracionMovilidad;
    private String motivacionEstudiante;
    private String codigoEstSaliente;
    
    private String semestreEntrante;
    private String existeConvenio;
    private MetNivelEstudioEntrante metNivelEstEntrante;
    private String otroNivelEstudio;
    private String seguroMedico;
    private Date vigenciaDesde;
    private Date vigenciaHasta;
    private String dirAlojamiento;
    private String telAlojamiento;
    private String paisDestino;
    private MetTipoEstadoSolicitud metTipoEstadoSolicitud;
    private MetEstadoSolicitud metEstadoSolicitud;
    private MetObservacionSolicitud metObservacionSolicitud;

    private String alergias;
    private String tieneAlergia;
    private String enfermedades;
    private String tieneEnfermedades;
    private String medicamentos;
    private String tieneMedicamento;
    private String restriccionAlimentaria;
    private String tieneRestriccionAlm;

    private Date FechaLlegada;
    private Date horaLlegada;
    private String aerolineaLlegada;
    private String vueloLlegada;
    private Date FechaSalida;
    private Date horaSalida;
    private String aerolineaSalida;
    private String vueloSalida;

    private String nombreContactoEntrante;
    private String apellidoContactoEntrante;
    private String cargoContactoEntrante;
    private String emailContactoEntrante;
    private String telContactoEntrante;
    
    
    public MetSolicitud() {
        this.metInfUsuario = new MetInfUsuario();
        this.metInstitucionConvenio = new MetInstitucionConvenio();
        this.metFacultadInstConvenio = new MetFacultadInstOrigen();
        this.metProgramaInstOrigen = new MetProgramaInstOrigen();
        this.metProgramaKl = new MetProgramaKl();
        this.metTipoSolicitud = new MetTipoSolicitud();
        this.metTipoMovilidad = new MetTipoMovilidad();
        this.metNivelEstEntrante = new MetNivelEstudioEntrante();
        this.metTipoEstadoSolicitud = new MetTipoEstadoSolicitud();
       // this.inicioMB = new InicioMB();
        //  this.metObservacionSolicitud = new MetObservacionSolicitud();
    }

    public String getConsecutivo() {
        return consecutivo;
    }

    public void setConsecutivo(String consecutivo) {
        this.consecutivo = consecutivo;
    }

    public String getVarFechaSolicitud() {
        return varFechaSolicitud;
    }

    public void setVarFechaSolicitud(String varFechaSolicitud) {
        this.varFechaSolicitud = varFechaSolicitud;
    }
    
    public Date getFechaLlegada() {
        return FechaLlegada;
    }

    public void setFechaLlegada(Date FechaLlegada) {
        this.FechaLlegada = FechaLlegada;
    }

    public Date getFechaSalida() {
        return FechaSalida;
    }

    public void setFechaSalida(Date FechaSalida) {
        this.FechaSalida = FechaSalida;
    }

    public String getAerolineaLlegada() {
        return aerolineaLlegada;
    }

    public void setAerolineaLlegada(String aerolineaLlegada) {
        this.aerolineaLlegada = aerolineaLlegada;
    }

    public String getVueloLlegada() {
        return vueloLlegada;
    }

    public void setVueloLlegada(String vueloLlegada) {
        this.vueloLlegada = vueloLlegada;
    }

    public Date getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(Date horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public Date getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(Date horaSalida) {
        this.horaSalida = horaSalida;
    }
 public String getAerolineaSalida() {
        return aerolineaSalida;
    }

    public void setAerolineaSalida(String aerolineaSalida) {
        this.aerolineaSalida = aerolineaSalida;
    }

    public String getVueloSalida() {
        return vueloSalida;
    }

    public void setVueloSalida(String vueloSalida) {
        this.vueloSalida = vueloSalida;
    }

    public String getEnfermedades() {
        return enfermedades;
    }

    public void setEnfermedades(String enfermedades) {
        this.enfermedades = enfermedades;
    }

    public String getTieneEnfermedades() {
        return tieneEnfermedades;
    }

    public void setTieneEnfermedades(String tieneEnfermedades) {
        this.tieneEnfermedades = tieneEnfermedades;
    }
    
    public String getAlergias() {
        return alergias;
    }

    public void setAlergias(String alergias) {
        this.alergias = alergias;
    }

    public String getTieneAlergia() {
        return tieneAlergia;
    }

    public void setTieneAlergia(String tieneAlergia) {
        this.tieneAlergia = tieneAlergia;
    }

    public String getMedicamentos() {
        return medicamentos;
    }

    public void setMedicamentos(String medicamentos) {
        this.medicamentos = medicamentos;
    }

    public String getTieneMedicamento() {
        return tieneMedicamento;
    }

    public void setTieneMedicamento(String tieneMedicamento) {
        this.tieneMedicamento = tieneMedicamento;
    }

    public String getRestriccionAlimentaria() {
        return restriccionAlimentaria;
    }

    public void setRestriccionAlimentaria(String restriccionAlimentaria) {
        this.restriccionAlimentaria = restriccionAlimentaria;
    }

    public String getTieneRestriccionAlm() {
        return tieneRestriccionAlm;
    }

    public void setTieneRestriccionAlm(String tieneRestriccionAlm) {
        this.tieneRestriccionAlm = tieneRestriccionAlm;
    }

    public MetObservacionSolicitud getMetObservacionSolicitud() {
        return metObservacionSolicitud;
    }

    public void setMetObservacionSolicitud(MetObservacionSolicitud metObservacionSolicitud) {
        this.metObservacionSolicitud = metObservacionSolicitud;
    }

    public String getOtroNivelEstudio() {
        return otroNivelEstudio;
    }

    public void setOtroNivelEstudio(String otroNivelEstudio) {
        this.otroNivelEstudio = otroNivelEstudio;
    }

    public MetTipoEstadoSolicitud getMetTipoEstadoSolicitud() {
        return metTipoEstadoSolicitud;
    }

    public void setMetTipoEstadoSolicitud(MetTipoEstadoSolicitud metTipoEstadoSolicitud) {
        this.metTipoEstadoSolicitud = metTipoEstadoSolicitud;
    }

    public MetEstadoSolicitud getMetEstadoSolicitud() {
        return metEstadoSolicitud;
    }

    public void setMetEstadoSolicitud(MetEstadoSolicitud metEstadoSolicitud) {
        this.metEstadoSolicitud = metEstadoSolicitud;
    }

    public String getSeguroMedico() {
        return seguroMedico;
    }

    public void setSeguroMedico(String seguroMedico) {
        this.seguroMedico = seguroMedico;
    }

    public Date getVigenciaDesde() {
        return vigenciaDesde;
    }

    public void setVigenciaDesde(Date vigenciaDesde) {
        this.vigenciaDesde = vigenciaDesde;
    }

    public Date getVigenciaHasta() {
        return vigenciaHasta;
    }

    public void setVigenciaHasta(Date vigenciaHasta) {
        this.vigenciaHasta = vigenciaHasta;
    }

    public String getDirAlojamiento() {
        return dirAlojamiento;
    }

    public void setDirAlojamiento(String dirAlojamiento) {
        this.dirAlojamiento = dirAlojamiento;
    }

    public String getTelAlojamiento() {
        return telAlojamiento;
    }

    public void setTelAlojamiento(String telAlojamiento) {
        this.telAlojamiento = telAlojamiento;
    }

    public String getPaisDestino() {
        return paisDestino;
    }

    public void setPaisDestino(String paisDestino) {
        this.paisDestino = paisDestino;
    }

    public String getSolicitudPK() {
        return solicitudPK;
    }

    public void setSolicitudPK(String solicitudPK) {
        this.solicitudPK = solicitudPK;
    }

    public Date getFechaSolicitud() {
        return fechaSolicitud;
    }

    public void setFechaSolicitud(Date fechaSolicitud) {
        this.fechaSolicitud = fechaSolicitud;
    }

    public MetInfUsuario getMetInfUsuario() {
        return metInfUsuario;
    }

    public void setMetInfUsuario(MetInfUsuario metInfUsuario) {
        this.metInfUsuario = metInfUsuario;
    }

    public MetInstitucionConvenio getMetInstitucionConvenio() {
        return metInstitucionConvenio;
    }

    public void setMetInstitucionConvenio(MetInstitucionConvenio metInstitucionConvenio) {
        this.metInstitucionConvenio = metInstitucionConvenio;
    }

    public MetFacultadInstOrigen getMetFacultadInstConvenio() {
        return metFacultadInstConvenio;
    }

    public void setMetFacultadInstConvenio(MetFacultadInstOrigen metFacultadInstConvenio) {
        this.metFacultadInstConvenio = metFacultadInstConvenio;
    }

    public MetProgramaInstOrigen getMetProgramaInstOrigen() {
        return metProgramaInstOrigen;
    }

    public void setMetProgramaInstOrigen(MetProgramaInstOrigen metProgramaInstOrigen) {
        this.metProgramaInstOrigen = metProgramaInstOrigen;
    }

    public MetProgramaKl getMetProgramaKl() {
        return metProgramaKl;
    }

    public void setMetProgramaKl(MetProgramaKl metProgramaKl) {
        this.metProgramaKl = metProgramaKl;
    }

    public MetTipoSolicitud getMetTipoSolicitud() {
        return metTipoSolicitud;
    }

    public void setMetTipoSolicitud(MetTipoSolicitud metTipoSolicitud) {
        this.metTipoSolicitud = metTipoSolicitud;
    }

    public MetTipoMovilidad getMetTipoMovilidad() {
        return metTipoMovilidad;
    }

    public void setMetTipoMovilidad(MetTipoMovilidad metTipoMovilidad) {
        this.metTipoMovilidad = metTipoMovilidad;
    }

    public Date getFechaInicioMovilidad() {
        return fechaInicioMovilidad;
    }

    public void setFechaInicioMovilidad(Date fechaInicioMovilidad) {
        this.fechaInicioMovilidad = fechaInicioMovilidad;
    }

    public Date getFechaFinMovilidad() {
        return fechaFinMovilidad;
    }

    public void setFechaFinMovilidad(Date fechaFinMovilidad) {
        this.fechaFinMovilidad = fechaFinMovilidad;
    }

    public String getDuracionMovilidad() {
        return duracionMovilidad;
    }

    public void setDuracionMovilidad(String duracionMovilidad) {
        this.duracionMovilidad = duracionMovilidad;
    }

    public String getMotivacionEstudiante() {
        return motivacionEstudiante;
    }

    public void setMotivacionEstudiante(String motivacionEstudiante) {
        this.motivacionEstudiante = motivacionEstudiante;
    }

    public String getCodigoEstSaliente() {
        return codigoEstSaliente;
    }

    public void setCodigoEstSaliente(String codigoEstSaliente) {
        this.codigoEstSaliente = codigoEstSaliente;
    }

    public String getOtraInstitucionEntrante() {
        return otraInstitucionEntrante;
    }

    public void setOtraInstitucionEntrante(String otraInstitucionEntrante) {
        this.otraInstitucionEntrante = otraInstitucionEntrante;
    }

    public String getOtraFacultadEntrante() {
        return otraFacultadEntrante;
    }

    public void setOtraFacultadEntrante(String otraFacultadEntrante) {
        this.otraFacultadEntrante = otraFacultadEntrante;
    }

    public String getOtroProgramaInstConvenio() {
        return otroProgramaInstConvenio;
    }

    public void setOtroProgramaInstConvenio(String otroProgramaInstConvenio) {
        this.otroProgramaInstConvenio = otroProgramaInstConvenio;
    }

    public String getOtroTipoMoviidad() {
        return otroTipoMoviidad;
    }

    public void setOtroTipoMoviidad(String otroTipoMoviidad) {
        this.otroTipoMoviidad = otroTipoMoviidad;
    }

    public String getNombreContactoEntrante() {
        return nombreContactoEntrante;
    }

    public void setNombreContactoEntrante(String nombreContactoEntrante) {
        this.nombreContactoEntrante = nombreContactoEntrante;
    }

    public String getApellidoContactoEntrante() {
        return apellidoContactoEntrante;
    }

    public void setApellidoContactoEntrante(String apellidoContactoEntrante) {
        this.apellidoContactoEntrante = apellidoContactoEntrante;
    }

    public String getCargoContactoEntrante() {
        return cargoContactoEntrante;
    }

    public void setCargoContactoEntrante(String cargoContactoEntrante) {
        this.cargoContactoEntrante = cargoContactoEntrante;
    }

    public String getEmailContactoEntrante() {
        return emailContactoEntrante;
    }

    public void setEmailContactoEntrante(String emailContactoEntrante) {
        this.emailContactoEntrante = emailContactoEntrante;
    }

    public String getTelContactoEntrante() {
        return telContactoEntrante;
    }

    public void setTelContactoEntrante(String telContactoEntrante) {
        this.telContactoEntrante = telContactoEntrante;
    }

    public String getSemestreEntrante() {
        return semestreEntrante;
    }

    public void setSemestreEntrante(String semestreEntrante) {
        this.semestreEntrante = semestreEntrante;
    }

    public String getExisteConvenio() {
        return existeConvenio;
    }

    public void setExisteConvenio(String existeConvenio) {
        this.existeConvenio = existeConvenio;
    }

    public MetNivelEstudioEntrante getMetNivelEstEntrante() {
        return metNivelEstEntrante;
    }

    public void setMetNivelEstEntrante(MetNivelEstudioEntrante metNivelEstEntrante) {
        this.metNivelEstEntrante = metNivelEstEntrante;
    }

    public String getPromedioAcumulado() {
        return promedioAcumulado;
    }

    public void setPromedioAcumulado(String promedioAcumulado) {
        this.promedioAcumulado = promedioAcumulado;
    }

    public String getPromedioUltSemestre() {
        return promedioUltSemestre;
    }

    public void setPromedioUltSemestre(String promedioUltSemestre) {
        this.promedioUltSemestre = promedioUltSemestre;
    }
    
    

}
