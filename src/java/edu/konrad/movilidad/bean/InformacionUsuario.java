/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

import java.util.Date;

/**
 *
 * @author leidy.sarmiento
 */
public class InformacionUsuario {

    String namNombres;
    String namApellidos;
    String namNumeroIdentificacion;
    Date namFechaNacimiento;
    String namNacionalidad;
    String namDireccionPaisOrigen;
    String namTelCasa;
    String namCelular;
    String namEmailInstitucional;
    String namEmailAlternativo;

    public InformacionUsuario(String namNombres, String namApellidos, String namNumeroIdentificacion, Date namFechaNacimiento, String namNacionalidad, String namDireccionPaisOrigen,
            String namTelCasa, String namCelular, String namEmailInstitucional, String namEmailAlternativo) {
        this.namNombres = namNombres;
        this.namApellidos = namApellidos;
        this.namNumeroIdentificacion = namNumeroIdentificacion;
        this.namFechaNacimiento = namFechaNacimiento;
        this.namNacionalidad = namNacionalidad;
        this.namDireccionPaisOrigen = namDireccionPaisOrigen;
        this.namTelCasa = namTelCasa;
        this.namCelular = namCelular;
        this.namEmailInstitucional = namEmailInstitucional;
        this.namEmailAlternativo = namEmailAlternativo;

    }

    public InformacionUsuario() {
    }

    public String getNamNombres() {
        return namNombres;
    }

    public void setNamNombres(String namNombres) {
        this.namNombres = namNombres;
    }

    public String getNamApellidos() {
        return namApellidos;
    }

    public void setNamApellidos(String namApellidos) {
        this.namApellidos = namApellidos;
    }

    public String getNamNumeroIdentificacion() {
        return namNumeroIdentificacion;
    }

    public void setNamNumeroIdentificacion(String namNumeroIdentificacion) {
        this.namNumeroIdentificacion = namNumeroIdentificacion;
    }

    public Date getNamFechaNacimiento() {
        return namFechaNacimiento;
    }

    public void setNamFechaNacimiento(Date namFechaNacimiento) {
        this.namFechaNacimiento = namFechaNacimiento;
    }

    public String getNamNacionalidad() {
        return namNacionalidad;
    }

    public void setNamNacionalidad(String namNacionalidad) {
        this.namNacionalidad = namNacionalidad;
    }

    public String getNamDireccionPaisOrigen() {
        return namDireccionPaisOrigen;
    }

    public void setNamDireccionPaisOrigen(String namDireccionPaisOrigen) {
        this.namDireccionPaisOrigen = namDireccionPaisOrigen;
    }

    public String getNamTelCasa() {
        return namTelCasa;
    }

    public void setNamTelCasa(String namTelCasa) {
        this.namTelCasa = namTelCasa;
    }

    public String getNamCelular() {
        return namCelular;
    }

    public void setNamCelular(String namCelular) {
        this.namCelular = namCelular;
    }

    public String getNamEmailInstitucional() {
        return namEmailInstitucional;
    }

    public void setNamEmailInstitucional(String namEmailInstitucional) {
        this.namEmailInstitucional = namEmailInstitucional;
    }

    public String getNamEmailAlternativo() {
        return namEmailAlternativo;
    }

    public void setNamEmailAlternativo(String namEmailAlternativo) {
        this.namEmailAlternativo = namEmailAlternativo;
    }

    
}
