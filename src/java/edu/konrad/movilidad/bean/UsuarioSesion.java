/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author Jose.suspes
 */
public class UsuarioSesion {
    
    private String namUsuario;
    private String namContrasenia;
    private String namNombres;
    private String namApellidos;
    private MetGenero metGenero;
    private MetTipoIdentificacion tipoIdetificacion;
    private String namNumeroIDentificacion;

    public String getNamUsuario() {
        return namUsuario;
    }

    public void setNamUsuario(String namUsuario) {
        this.namUsuario = namUsuario;
    }

    public String getNamContrasenia() {
        return namContrasenia;
    }

    public void setNamContrasenia(String namContrasenia) {
        this.namContrasenia = namContrasenia;
    }

    public String getNamNombres() {
        return namNombres;
    }

    public void setNamNombres(String namNombres) {
        this.namNombres = namNombres;
    }

    public String getNamApellidos() {
        return namApellidos;
    }

    public void setNamApellidos(String namApellidos) {
        this.namApellidos = namApellidos;
    }

    public MetGenero getMetGenero() {
        return metGenero;
    }

    public void setMetGenero(MetGenero metGenero) {
        this.metGenero = metGenero;
    }

    public MetTipoIdentificacion getTipoIdetificacion() {
        return tipoIdetificacion;
    }

    public void setTipoIdetificacion(MetTipoIdentificacion tipoIdetificacion) {
        this.tipoIdetificacion = tipoIdetificacion;
    }

    public String getNamNumeroIDentificacion() {
        return namNumeroIDentificacion;
    }

    public void setNamNumeroIDentificacion(String namNumeroIDentificacion) {
        this.namNumeroIDentificacion = namNumeroIDentificacion;
    }    
}
