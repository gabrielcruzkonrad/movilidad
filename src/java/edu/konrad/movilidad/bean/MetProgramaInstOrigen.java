/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */
public class MetProgramaInstOrigen {
    
    private String pk;
    private MetInstitucionConvenio metInstitucionOrigen;
    private MetFacultadInstOrigen metFacultadInstitucionOrigen;
    private MetNivelEstudioEntrante metNivelEstudioEntrante;
    private String namSemestre;
    private String namPrograma;
    
     public MetProgramaInstOrigen() {
        this.metInstitucionOrigen = new MetInstitucionConvenio();
        this.metFacultadInstitucionOrigen = new MetFacultadInstOrigen();
        this.metNivelEstudioEntrante = new MetNivelEstudioEntrante();
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public MetInstitucionConvenio getMetInstitucionOrigen() {
        return metInstitucionOrigen;
    }

    public void setMetInstitucionOrigen(MetInstitucionConvenio metInstitucionOrigen) {
        this.metInstitucionOrigen = metInstitucionOrigen;
    }

    public MetFacultadInstOrigen getMetFacultadInstitucionOrigen() {
        return metFacultadInstitucionOrigen;
    }

    public void setMetFacultadInstitucionOrigen(MetFacultadInstOrigen metFacultadInstitucionOrigen) {
        this.metFacultadInstitucionOrigen = metFacultadInstitucionOrigen;
    }

    public MetNivelEstudioEntrante getMetNivelEstudioEntrante() {
        return metNivelEstudioEntrante;
    }

    public void setMetNivelEstudioEntrante(MetNivelEstudioEntrante metNivelEstudioEntrante) {
        this.metNivelEstudioEntrante = metNivelEstudioEntrante;
    }

    public String getNamSemestre() {
        return namSemestre;
    }

    public void setNamSemestre(String namSemestre) {
        this.namSemestre = namSemestre;
    }

    public String getNamPrograma() {
        return namPrograma;
    }

    public void setNamPrograma(String namPrograma) {
        this.namPrograma = namPrograma;
    }
   


}