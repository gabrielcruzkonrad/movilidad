/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

//import java.util.Date;

import java.util.Date;


/**
 *
 * @author leidy.sarmiento
 */
public class MetEstadoSolicitud {
    private String pk;
    private MetTipoEstadoSolicitud metTipoEstadoSolicitud;
    private MetSolicitud metSolicitud;
    private Date FechaEnvio;
    private MetInfUsuario usuarioTransRealizada;
    
    
    public MetEstadoSolicitud() {
        this.metTipoEstadoSolicitud = new MetTipoEstadoSolicitud();
        this.metSolicitud = new MetSolicitud();
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public MetTipoEstadoSolicitud getMetTipoEstadoSolicitud() {
        return metTipoEstadoSolicitud;
    }

    public void setMetTipoEstadoSolicitud(MetTipoEstadoSolicitud metTipoEstadoSolicitud) {
        this.metTipoEstadoSolicitud = metTipoEstadoSolicitud;
    }

    public MetSolicitud getMetSolicitud() {
        return metSolicitud;
    }

    public void setMetSolicitud(MetSolicitud metSolicitud) {
        this.metSolicitud = metSolicitud;
    }

    public MetInfUsuario getUsuarioTransRealizada() {
        return usuarioTransRealizada;
    }

    public void setUsuarioTransRealizada(MetInfUsuario usuarioTransRealizada) {
        this.usuarioTransRealizada = usuarioTransRealizada;
    }
 
    public Date getFechaEnvio() {
        return FechaEnvio;
    }

    public void setFechaEnvio(Date FechaEnvio) {
        this.FechaEnvio = FechaEnvio;
    }
    
    
    
    
}
