/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */
public class MetPerfilMedico {
    
    private String pk;
    private MetAspectoMedico metAspectoMedico;
    private MetSolicitud metSolicitud;

    
    public MetPerfilMedico(){
        this.metAspectoMedico = new MetAspectoMedico();
        this.metSolicitud = new MetSolicitud();
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public MetAspectoMedico getMetAspectoMedico() {
        return metAspectoMedico;
    }

    public void setMetAspectoMedico(MetAspectoMedico metAspectoMedico) {
        this.metAspectoMedico = metAspectoMedico;
    }

    public MetSolicitud getMetSolicitud() {
        return metSolicitud;
    }

    public void setMetSolicitud(MetSolicitud metSolicitud) {
        this.metSolicitud = metSolicitud;
    }

 
    
}
