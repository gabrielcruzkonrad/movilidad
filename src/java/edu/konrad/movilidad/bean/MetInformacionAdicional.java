/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.bean;

/**
 *
 * @author leidy.sarmiento
 */
public class MetInformacionAdicional {
    private String pk;
    private MetAspectoMovilidad metAspectoMovilidad;
    private MetSolicitud metSolicitud;
    private String estado;
    private String infoAdicional;
    
   
    public MetInformacionAdicional(){
        this.metAspectoMovilidad = new MetAspectoMovilidad();
        this.metSolicitud = new MetSolicitud();      
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getInfoAdicional() {
        return infoAdicional;
    }

    public void setInfoAdicional(String infoAdicional) {
        this.infoAdicional = infoAdicional;
    }
    
    

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public MetAspectoMovilidad getMetAspectoMovilidad() {
        return metAspectoMovilidad;
    }

    public void setMetAspectoMovilidad(MetAspectoMovilidad metAspectoMovilidad) {
        this.metAspectoMovilidad = metAspectoMovilidad;
    }

    public MetSolicitud getMetSolicitud() {
        return metSolicitud;
    }

    public void setMetSolicitud(MetSolicitud metSolicitud) {
        this.metSolicitud = metSolicitud;
    }
    
}
