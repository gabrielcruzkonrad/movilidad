/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.constants;

/**
 *
 * @author jose.suspes
 */
public interface Constantes {

    public static final String ZERO = "0";
    public static final String OK = "OK";
    public static final String FAILED = "FAILED";
    public static final String ROL_BASE = "1";
    public static final String ESTADO_BASE = "A";
    public static final String TIPO_SOLICITUD_ENTRANTE = "1";
    public static final String TIPO_SOLICITUD_SALIENTE = "2";

    public static final String COD_IDIOMA_ESPANIOL = "ESP";
    public static final String COD_IDIOMA_INGLES = "ING";
    public static final String COD_IDIOMA_OTRO = "OTR";
    public static final String DES_IDIOMA_ESPANIOL = "ESPAÑOL";
    public static final String DES_IDIOMA_INGLES = "INGLÉS";

    public static final String FECHA_LLEGADA_BOGOTA = "F_LL";
    public static final String HORA_LLEGADA = "H_LL";
    public static final String AEROLINEA_LLEGADA = "A_LL";
    public static final String VUELO_LLEGADA = "V_LL";
    public static final String FECHA_SALIDA_BOGOTA = "F_SAL";
    public static final String HORA_SALIDA = "H_SAL";
    public static final String AEROLINEA_SALIDA = "A_SAL";
    public static final String VUELO_SALIDA = "V_SAL";

    public static final String ESTADOSOL_APROBADA = "1";
    public static final String ESTADOSOL_ENVIADA = "2";
    public static final String ESTADOSOL_GUARDADA = "3";
    public static final String ESTADOSOL_RECHAZADA = "4";
    public static final String ESTADOSOL_SINREVISAR = "5";

    public static final String INSTITUCION_ENTRANTE_NULL = "57";
    public static final String FACULTAD_ENTRANTE_NULL = "15";
    public static final String PROGRAMA_ENTRANTE_NULL = "4";
    public static final String NIVELEST_ENTRANTE_NULL = "4";

    public static final String ALTERNATIVO = "ALT";
    public static final String NOALTERNATIVO = "NAL";
    public static final String PATHFILE = "C:\\temp\\";

    public static final String TA_PASAPORTE = "PAS";
    public static final String TA_SEGUROINT = "SGI";
    public static final String TA_CERTNOTAS = "CRN";
    public static final String TA_CARTAPRESENTACION = "CTP";
    public static final String TA_CARTAMOTIVACION = "CTM";
    public static final String TA_CARTACOMPROMISO = "CTC";
    public static final String TA_PRUEBASUFICIENCIA = "PDS";
    public static final String TA_PLANHOMOLOGACION = "PLH";
    public static final String TA_CARTASOLECONOMICA = "CSE";
    public static final String TA_FORMULARIOSOLICITUDU = "FSUD";


    public static final String DES_PRUEBASUFICIENCIA = "Prueba de sufucuencia";

    public static final String DES_FORMULARIOSOLICITUDU = "Formulario de solicitud de la universidad destino";
    public static final String COD_DISTINCION = "DIS";

    public static final String COD_CARTA_INCENTIVO = "CI";
    public static final String DES_CARTA_INCENTIVO = "Carta de solicitud de incentivo a la internacionalización";

    public static final String COD_CERTIFICADO_MOT = "CM";
    public static final String DES_CERTIFICADO_MOT = "Certificado de motivación";

    public static final String COD_CERTIFICADO_NOTAS = "CN";
    public static final String DES_CERTIFICADO_NOTAS = "Certificado de notas oficial";

    public static final String COD_FOTOCOPIA_CEDULA = "FC";
    public static final String DES_FOTOCOPIA_CEDULA = "Fotocopia cédula de acudiente";

    public static final String COD_FOTOCOPIA_PASAPORTE = "FP";
    public static final String DES_FOTOCOPIA_PASAPORTE = "Fotocopia del pasaporte";

    public static final String COD_PLANHOMOLOGACION = "PH";
    public static final String DES_PLANHOMOLOGACION = "Plan de homologación";

    public static final String COD_CARTACOMPROMISO = "CMP";
    public static final String DES_CARTACOMPROMISO = "Carta de compromiso";
    
    public static final String COD_CARTASOLVENCIA = "CSE";
    public static final String DES_CARTASOLVENCIA = "Carta de solvencia económica";

    public static final String ACTIVO = "Activo";
    public static final String INACTIVO = "Inactivo";
}
