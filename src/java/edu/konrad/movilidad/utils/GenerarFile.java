/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.utils;

import edu.konrad.movilidad.servlet.GenerarReporte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.print.DocFlavor;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

/**
 *
 * @author leidy.sarmiento
 */
public class GenerarFile {

    private final static Logger log = Logger.getLogger(GenerarFile.class);

    public File processRequest(Map parameters, String reporte)
            throws Exception {

        Connection cn;
        String directorio = new String("");
       File reportFile = new File("");
       File initialFile = new File("");
        try {
            cn = ConexionInformes.getConexion();      
            ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();         
            directorio = servletContext.getRealPath("/") + "reports/" + reporte + ".jasper";
            reportFile = new File(directorio);
            parameters.put("IMAGE_DIR", servletContext.getRealPath("/") + "reports");
            //response.reset();
            JasperReport jasperReport = (JasperReport) JRLoader.loadObject(reportFile);
            //response.reset();
            byte[] bytes = JasperRunManager.runReportToPdf(jasperReport, parameters, cn);
            InputStream initialStream = new ByteArrayInputStream(bytes);
            initialFile = new File("Report_Mobility_" + parameters.get("USUARIO_ESTUDIANTE")+ ".pdf");
            FileUtils.copyInputStreamToFile(initialStream, initialFile);
            ConexionInformes.closeConexion();  
            log.info("Genero reporte");
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                ConexionInformes.closeConexion();
            } catch (Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
            }
        }  
        return initialFile;
        }
    
    }
