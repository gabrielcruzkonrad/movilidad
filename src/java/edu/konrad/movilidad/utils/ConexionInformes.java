/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.utils;

/**
 *
 * @author leidy.sarmiento
 */
import edu.konrad.movilidad.framework.configuracion.ConfiguradorIbatis;
import java.sql.*;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

public class ConexionInformes {
      private static Connection conexion;
    
    
    public static Connection getConexion() throws SQLException {
        
        SqlSessionFactory sqlSessionFactory = ConfiguradorIbatis.getInstance().getSqlSessionFactory();
        SqlSession session = sqlSessionFactory.openSession(true);
	conexion=session.getConnection();
        return conexion;
        }
    
    public static void closeConexion() throws SQLException {
        conexion.close();
    }
}
