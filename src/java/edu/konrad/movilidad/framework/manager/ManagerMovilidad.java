/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.framework.manager;


import edu.konrad.movilidad.framework.dao.DaoMovilidad;
import java.util.List;

/**
 *
 * @author jsuspes
 */
public class ManagerMovilidad extends ManagerStandard {

    private static final ManagerMovilidad managerAdmisiones = new ManagerMovilidad();

    /**
     * @type Constructor de la clase FacadeGeneral
     * @name FacadeGeneral
     * @return void
     * @desc Crea una instancia de FacadeGeneral
     */
    private ManagerMovilidad() {
        super();
    }

    /**
     * @type Mï¿½todo de la clase ManagerBP
     * @name getManager
     * @return ManagerBP
     * @desc permite obtener la instancia del objeto ManagerBP (singlenton)
     */
    public static ManagerMovilidad getManager() {
        return managerAdmisiones;
    }

    public List obtenerListado(String sqlName, Object objeto) throws Exception {
        DaoMovilidad serviciosDao = new DaoMovilidad();
        return (List) serviciosDao.obtenerListado(sqlName, objeto);
    }

    public List obtenerListado(String sqlName) throws Exception {
        DaoMovilidad serviciosDao = new DaoMovilidad();
        return (List) serviciosDao.obtenerListado(sqlName);
    }

    public Object ejecutarProcedimiento(String sqlName) throws Exception {
        DaoMovilidad serviciosDao = new DaoMovilidad();
        return serviciosDao.ejecutarProcedimiento(sqlName);
    }

    public Object obtenerRegistro(String sqlName, Object object) throws Exception {
        DaoMovilidad serviciosDao = new DaoMovilidad();
        return serviciosDao.obtenerRegistro(sqlName, object);
    }

    public Object obtenerRegistro(String sqlName) throws Exception {
        DaoMovilidad serviciosDao = new DaoMovilidad();
        return serviciosDao.obtenerRegistro(sqlName);
    }

    public Object insertarRegistro(String qryName, Object objeto)
            throws Exception {
        DaoMovilidad serviciosDao = new DaoMovilidad();
        return serviciosDao.insertarRegistro(qryName, objeto);
    }

    public Object actualizarRegistro(String qryName, Object objeto)
            throws Exception {
        DaoMovilidad serviciosDao = new DaoMovilidad();
        return serviciosDao.actualizarRegistro(qryName, objeto);
    }
    public void borrarRegistro(String sqlName, Object object) throws Exception {
        DaoMovilidad serviciosDao = new DaoMovilidad();
        serviciosDao.borrarRegistro(sqlName, object);
    }

}
