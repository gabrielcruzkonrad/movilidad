/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.servicios;

import edu.konrad.movilidad.bean.MetContactoEmergencia;
import edu.konrad.movilidad.bean.MetCursosKL;
import edu.konrad.movilidad.bean.MetDistinciones;
import edu.konrad.movilidad.bean.MetEstadoSolicitud;
import edu.konrad.movilidad.bean.MetFacultadInstOrigen;
import edu.konrad.movilidad.bean.MetGenero;
import edu.konrad.movilidad.bean.MetHomologacionSaliente;
import edu.konrad.movilidad.bean.MetInfUsuario;
import edu.konrad.movilidad.bean.MetInfoCursoKL;
import edu.konrad.movilidad.bean.MetInfoIdioma;
import edu.konrad.movilidad.bean.MetInformacionAdicional;
import edu.konrad.movilidad.bean.MetInstitucionConvenio;
import edu.konrad.movilidad.bean.MetNivelEstudioEntrante;
import edu.konrad.movilidad.bean.MetObservacionSolicitud;
import edu.konrad.movilidad.bean.MetPais;
import edu.konrad.movilidad.bean.MetParentesco;
import edu.konrad.movilidad.bean.MetProgramaInstOrigen;
import edu.konrad.movilidad.bean.MetProgramaKl;
import edu.konrad.movilidad.bean.MetSolicitud;
import edu.konrad.movilidad.bean.MetTipoIdentificacion;
import edu.konrad.movilidad.bean.MetTipoMovilidad;
import edu.konrad.movilidad.bean.MetTipoSolicitud;
import edu.konrad.movilidad.bean.UsuarioSesion;
import edu.konrad.movilidad.bean.MetOtraInstitucionExt;
import edu.konrad.movilidad.bean.MetSoporteAdjunto;
import java.util.List;

/**
 *
 * @author Jose.suspes
 */
public interface ServiciosMovilidad {

    /*LISTAS*/
    public String validaUsuario(UsuarioSesion usuarioSesion);

    public List<MetGenero> tiposGenero();

    public List<MetParentesco> tiposParentesco();

    public List<MetTipoMovilidad> tiposMovilidad();

    public List<MetTipoIdentificacion> tiposIdentificacion();

    public List<MetTipoIdentificacion> tiposIdentificacionIngles();

    public List<MetTipoSolicitud> tiposSolicitud();

    public List<MetNivelEstudioEntrante> nivelesEstudio();

    public List<MetPais> listaPais();

    public List<MetInstitucionConvenio> listInstitucionConvenio();

    public List<MetInfUsuario> listarSolicitudSinRevisar();

   // public List<MetInfUsuario> listarSolicitudRechazada();

    public List<MetInfUsuario> listarSolicitudAprobada();

    public String obtenerUsuario(MetInfUsuario metInfUsuario);

    public MetInfUsuario obtenerInfoUsuario(String usuario);
    
    public MetInfUsuario obtenerUsuarioXEmail(MetInfUsuario usuario);

    public MetInstitucionConvenio obtenerInfInstitucionConvenio(String InstitucionConvenio);

    public MetInformacionAdicional obtenerInfoAdicional(String informacionAdicional);

    public MetInfUsuario consultarSolicitudEnviada(String metInfUsuario);

    public String actualizarInfPersonalEst(MetInfUsuario metUsuario);

    public String actualizarInfoAdicional(MetInformacionAdicional metInfoAdicional);

    public String actualizarContactoEmergencia(MetContactoEmergencia metContactoEmergencia);

    public String insertMetInfUsuario(MetInfUsuario metUsuario);

    public String insertMetInfInstitucionConvenio(MetInstitucionConvenio metInstitucionConvenio);

    public String insertMetProgramaInstOrigen(MetProgramaInstOrigen metProgramaInstOrigen);

    public String insertMetSolicitud(MetSolicitud metSolicitud);

    public String insertMetInstitucionOrigen(MetSolicitud metsolicitud);

    public String insertarMetSolicitud(MetSolicitud solicitud);

    public String insertarMetInfoIdioma(MetInfoIdioma infoIdioma);

    public String insertSolicitud(MetSolicitud metSolicitud);

    /*ADMINISTRADOR */
    public MetInfUsuario obtenerAdmPersonalEntrante(String InfPersonalEntrante);

    public MetContactoEmergencia obtenerContactoEmergenciaSol(String infContactoEmergencia);

    public String contSolicitudEnviada();

    /*INSERT NUEVA SOLICITUD*/
    public String insertNuevaSolicitud(MetSolicitud newSolicitud);

    /*FORMULARIO ENTRANTE*/
    public MetSolicitud contSolicitudPK(String solicitudpk);

    public String insertNewSolicitud(MetSolicitud metNewSolicitud);

    public MetSolicitud consultarPKSolicitud(String usuarioPK);

    public String insertFechaEnvio(MetSolicitud metSolicitud);

    public String updateInfoIdioma(MetInfoIdioma metInfoIdioma);

    public String insertInfoIdioma(MetInfoIdioma metinfIdioma);

    public MetSolicitud obtenerInfSolicitud(String metsolicitudpk);
    
    public MetSolicitud obtenerPromedioAcumulado(String codigoEstudiante);
     public MetSolicitud obtenerPromedioAcumulado2(String codigoEstudiante);
    
    public MetSolicitud obtenerPromedioUltSemestre(String codigoEstudiante);



    public String insertInfIdioma(MetInfoIdioma metInfIdioma);

    public MetContactoEmergencia obtenerContactoEmergenciaFent(String metContatoEmergencia);

    public String enviarSolicitud(MetSolicitud metSolicitud);

    public String aprobarSolicitud(MetSolicitud metSolicitud);

    public String rechazarSolicitud(MetSolicitud metSolicitud);

    public List<MetSolicitud> listarSolicitudesEnviadasFent(String solEnviada);


    /* INSERTS*/
    public String insertEstadoSolicitud(MetEstadoSolicitud metEstadoSolicitud);

    public String insertPKObservacion(MetObservacionSolicitud metObservacion);

    public String insertPKContactoEmergencia(MetContactoEmergencia metContactoEmergencia);

    public String insertDistinciones(MetDistinciones metDistinciones);

    public String insertPKEstadoSolicitud(MetEstadoSolicitud metEstadoSolicitud);

    public String insertInfoCursoKL(MetInfoCursoKL metInfoCursoKL);

    public String insertInfoCursoKLAlt(MetInfoCursoKL metInfoCursoKL);

    public String insertMetInfoIdiomas(MetInfoIdioma metInfoIdioma);

    public String insertHomologacionS(MetHomologacionSaliente metHomologacion);

    public String insertHomologacionSALT(MetHomologacionSaliente metHomologacion);

    /*UPDATES*/
    public String cambiarEstadoSolicitud(MetEstadoSolicitud metEstadoSolicitud);

    public String updateMetSolicitud(MetSolicitud metSolicitud);

    public String updatePKMetInstConvenio(MetSolicitud metSolicitud);

    public String updatePKMetFacultadConvenio(MetSolicitud metSolicitud);

    public String updatePKMetProgramaConvenio(MetSolicitud metSolicitud);

    public String updatePKMetNivelEstudio(MetSolicitud metSolicitud);

    public String updateContactoEmergencia(MetContactoEmergencia metContactoEmergencia);

    public String updateMetObservacion(MetObservacionSolicitud metObservacionSolicitud);


    /*LISTAS*/
    public List<MetInstitucionConvenio> listaInstitucionesConvenio();

    public List<MetFacultadInstOrigen> listaFacultadesConvenio();

    public List<MetProgramaInstOrigen> listaProgramasConvenio();

    public List<MetNivelEstudioEntrante> listaNivelEstudio();

    public List<MetProgramaKl> listarProgramasKL();

    public List<MetCursosKL> listarCursosKL();

    /*CONSULTAS*/
    public String validarInfoIdioma(MetInfoIdioma metInfIdioma);

    public MetInfoIdioma obtenerMetInfoIdiomaEsp(String infIdioma);

    public MetInfoIdioma obtenerMetInfoIdiomaIng(String infIdioma);

    public MetInfoIdioma obtenerMetInfoIdiomaOtr(String infIdioma);

    public MetSolicitud obtenerPKSolicitud(String solicitud);

    public MetEstadoSolicitud obtenerEstadoPkSol(String estado);

    public List<MetDistinciones> obtenerDistincion(String pkSolicitud);

    public List<MetInfoCursoKL> obtenerInfoCursoKL(String pkSolicitud);

    public List<MetInfoCursoKL> obtenerInfoCursoKLAlt(String pkSolicitud);

    public MetInfUsuario obtenerEstSaliente(String estSaliente);

    public String validarExisteSolicitud(String metInfUsuario);

    public List<MetHomologacionSaliente> obtenerHomologacion(String pkSolicitud);

    public List<MetHomologacionSaliente> obtenerHomologacionALT(String pkSolicitud);

    /*DELETE */
    public String deleteDistincion(String metdistincion);

    public String deleteInfoCurso(String metInfoCurso);

    public String deleteHomologacion(String pkSolicitud);

    /*COUNTS*/
    public String validaDistincion(String distincion);

    /*CRUD MET INSTITUCION CONVENIO*/
    public MetInstitucionConvenio obtenerMetInstitucionConv(String pkInstitucion);

    /*CRUD MET TIPO MOVILIDAD*/
    public String updatePKMetTipoMovilidad(MetSolicitud metSolicitud);

    /*------------------------------------------------------------*/
    public String crearSolicitud(MetSolicitud nuevaSolicitud);

    public MetInfUsuario obtenerDtUsuario(String usuario);

    public MetSolicitud obtenerIDSolicitud(String pkUsuario);

    public String definirEstadoSolicitud(MetEstadoSolicitud metEstadoSolicitud);

    public String definirContactoEmergencia(MetContactoEmergencia metContactoEmergencia);

    public MetEstadoSolicitud obtenerIDEstadoSolicitud(String solicitud);
    /*----------------------- SQL -----------------------------------*/
    public String guardarInfPersonal(MetInfUsuario metInfoPersonal);

    public String guardarOtraInstitucion(MetOtraInstitucionExt metOtraInstitucion);

    public String eliminarPKInstitucion(String pkSolicitud);

    public String borrarOtraInstitucion(String pkSolicitud);

    public MetOtraInstitucionExt consultarInstitucionExt(String pkSolicitud);

    public String existeOtraInstitucion(String pkSolicitud);

    public String actualizarOtraInstitucion(MetOtraInstitucionExt metOtraInstitucionExt);

    public String updateRHusuario(MetInfUsuario usuario);
    
    public String guardarArchivo(MetSoporteAdjunto metSoporteAdjunto);
    
    public List<MetSoporteAdjunto> selectListArchivos(String pkSolicitud);
    
     public String deleteSoporteAdjunto(String metSoporteAdjunto);
    
    public String validaOtraInstitucion(String solicitudPK);
    
    public String validarCodigoEstudiante(String metInfUsuario);
    
    public String existeUsuario(String metInfUsuario);
    
    public String getContrasenia(MetInfUsuario metInfUsuario);
}
