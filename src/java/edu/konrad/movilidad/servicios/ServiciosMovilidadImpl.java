/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.servicios;

import edu.konrad.movilidad.constants.Constantes;
import edu.konrad.movilidad.bean.MetContactoEmergencia;
import edu.konrad.movilidad.bean.MetCursosKL;
import edu.konrad.movilidad.bean.MetDistinciones;
import edu.konrad.movilidad.bean.MetEstadoSolicitud;
import edu.konrad.movilidad.bean.MetFacultadInstOrigen;
import edu.konrad.movilidad.bean.MetGenero;
import edu.konrad.movilidad.bean.MetHomologacionSaliente;
import edu.konrad.movilidad.bean.MetInfUsuario;
import edu.konrad.movilidad.bean.MetInfoCursoKL;
import edu.konrad.movilidad.bean.MetInfoIdioma;
import edu.konrad.movilidad.bean.MetInformacionAdicional;
import edu.konrad.movilidad.bean.MetInstitucionConvenio;
import edu.konrad.movilidad.bean.MetNivelEstudioEntrante;
import edu.konrad.movilidad.bean.MetObservacionSolicitud;
import edu.konrad.movilidad.bean.MetOtraInstitucionExt;
import edu.konrad.movilidad.bean.MetPais;
import edu.konrad.movilidad.bean.MetParentesco;
import edu.konrad.movilidad.bean.MetProgramaInstOrigen;
import edu.konrad.movilidad.bean.MetProgramaKl;
import edu.konrad.movilidad.bean.MetSolicitud;
import edu.konrad.movilidad.bean.MetSoporteAdjunto;
import edu.konrad.movilidad.bean.MetTipoIdentificacion;
import edu.konrad.movilidad.bean.MetTipoMovilidad;
import edu.konrad.movilidad.bean.MetTipoSolicitud;
import edu.konrad.movilidad.bean.UsuarioSesion;
import edu.konrad.movilidad.framework.manager.ManagerMovilidad;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 *
 * @author Jose.suspes
 */
public class ServiciosMovilidadImpl implements ServiciosMovilidad {

    private final static Logger log = Logger.getLogger(ServiciosMovilidadImpl.class);

    @Override
    public String validaUsuario(UsuarioSesion usuarioSesion) {
        String usuarioValido = "0";
        try {
            usuarioValido = (String) ManagerMovilidad.getManager().obtenerRegistro("validarUsuario", usuarioSesion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return usuarioValido;
    }

    @Override
    public String validarExisteSolicitud(String metInfUsuario) {
        String solicitudEncontrada = "0";
        try {
            solicitudEncontrada = (String) ManagerMovilidad.getManager().obtenerRegistro("validarExisteSolicitud", metInfUsuario);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return solicitudEncontrada;
    }

    /*LISTAS*/
    @Override
    public List<MetTipoIdentificacion> tiposIdentificacion() {

        List<MetTipoIdentificacion> tiposIDentificacion = new ArrayList();
        try {
            tiposIDentificacion = (List<MetTipoIdentificacion>) ManagerMovilidad.getManager().obtenerListado("obtenerListadoTipoIdentificacion");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tiposIDentificacion;
    }

    @Override
    public List<MetTipoIdentificacion> tiposIdentificacionIngles() {

        List<MetTipoIdentificacion> tiposIDentificacion = new ArrayList();
        try {
            tiposIDentificacion = (List<MetTipoIdentificacion>) ManagerMovilidad.getManager().obtenerListado("listaTipoDocumentoIngles");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tiposIDentificacion;
    }

    @Override
    public List<MetGenero> tiposGenero() {

        List<MetGenero> tiposGenero = new ArrayList();
        try {
            tiposGenero = (List<MetGenero>) ManagerMovilidad.getManager().obtenerListado("obtenerListadoGenero");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tiposGenero;
    }

    @Override
    public List<MetParentesco> tiposParentesco() {

        List<MetParentesco> tiposParentesco = new ArrayList();
        try {
            tiposParentesco = (List<MetParentesco>) ManagerMovilidad.getManager().obtenerListado("obtenerListadoParentesco");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tiposParentesco;
    }

    @Override
    public List<MetTipoMovilidad> tiposMovilidad() {

        List<MetTipoMovilidad> tiposMovilidad = new ArrayList();
        try {
            tiposMovilidad = (List<MetTipoMovilidad>) ManagerMovilidad.getManager().obtenerListado("obtenerListadoTipoMovilidad");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tiposMovilidad;
    }

    @Override
    public List<MetTipoSolicitud> tiposSolicitud() {

        List<MetTipoSolicitud> tiposSolicitud = new ArrayList();
        try {
            tiposSolicitud = (List<MetTipoSolicitud>) ManagerMovilidad.getManager().obtenerListado("obtenerListadoTipSolicitud");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return tiposSolicitud;
    }

    @Override
    public List<MetPais> listaPais() {

        List<MetPais> ListarPais = new ArrayList();
        try {
            ListarPais = (List<MetPais>) ManagerMovilidad.getManager().obtenerListado("obtenerListadoPais");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return ListarPais;
    }

    @Override
    public List<MetNivelEstudioEntrante> nivelesEstudio() {

        List<MetNivelEstudioEntrante> nivelesEstudio = new ArrayList();
        try {
            nivelesEstudio = (List<MetNivelEstudioEntrante>) ManagerMovilidad.getManager().obtenerListado("obtenerListadoNivelEstudio");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return nivelesEstudio;
    }

    @Override
    public List<MetInstitucionConvenio> listInstitucionConvenio() {

        List<MetInstitucionConvenio> listInstitucionConvenio = new ArrayList();
        try {
            listInstitucionConvenio = (List<MetInstitucionConvenio>) ManagerMovilidad.getManager().obtenerListado("listaInstitucionesConvenio");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listInstitucionConvenio;
    }

    @Override
    public String obtenerUsuario(MetInfUsuario metInfUsuario) {
        String obtenerUsuario = "0";
        try {
            obtenerUsuario = (String) ManagerMovilidad.getManager().obtenerRegistro("obtenerUsuario", metInfUsuario);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return obtenerUsuario;
    }

    @Override
    public MetInfUsuario obtenerInfoUsuario(String usuario) {
        MetInfUsuario infUsuario = new MetInfUsuario();
        try {
            infUsuario = ((List<MetInfUsuario>) ManagerMovilidad.getManager().obtenerListado("consultarInfoUsuario", usuario)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return infUsuario;
    }

    @Override
    public MetInfUsuario obtenerUsuarioXEmail(MetInfUsuario usuario) {
        MetInfUsuario infUsuario = new MetInfUsuario();
        try {
            infUsuario = ((List<MetInfUsuario>) ManagerMovilidad.getManager().obtenerListado("consultarUsuarioXEmail", usuario)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return infUsuario;
    }

    @Override
    public MetInstitucionConvenio obtenerInfInstitucionConvenio(String InstitucionConvenio) {
        MetInstitucionConvenio MetInstitucionConvenio = new MetInstitucionConvenio();
        try {
            MetInstitucionConvenio = ((List<MetInstitucionConvenio>) ManagerMovilidad.getManager().obtenerListado("obtenerInfInstitucionConvenio", InstitucionConvenio)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return MetInstitucionConvenio;
    }

    @Override
    public MetInfUsuario obtenerEstSaliente(String estSaliente) {
        MetInfUsuario metInfUsuario = new MetInfUsuario();
        try {
            metInfUsuario = ((List<MetInfUsuario>) ManagerMovilidad.getManager().obtenerListado("obtenerEstSaliente", estSaliente)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return metInfUsuario;
    }

    @Override
    public String insertMetInfUsuario(MetInfUsuario metUsuario) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertMetInfUsuario", metUsuario);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertMetInfInstitucionConvenio(MetInstitucionConvenio metInstitucionConvenio) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertInfConvenio", metInstitucionConvenio);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertMetProgramaInstOrigen(MetProgramaInstOrigen metProgramaInstOrigen) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertProgramaInstOrigen", metProgramaInstOrigen);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertMetSolicitud(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertInfMetSolicitud", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String actualizarContactoEmergencia(MetContactoEmergencia metContactoEmergencia) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("actualizarContactoEmergencia", metContactoEmergencia);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public MetInfUsuario consultarSolicitudEnviada(String metInfUsuario) {
        MetInfUsuario infSolicitudEnviada = new MetInfUsuario();
        try {
            infSolicitudEnviada = ((List<MetInfUsuario>) ManagerMovilidad.getManager().obtenerListado("consultarSolicitudEnviada", metInfUsuario)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return infSolicitudEnviada;
    }

    @Override
    public String insertarMetInfoIdioma(MetInfoIdioma infoIdioma) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertMetInfIdioma", infoIdioma);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertSolicitud(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("InsertSolicitud", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertMetInstitucionOrigen(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("InsertInstitucionOrigen", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertarMetSolicitud(MetSolicitud solicitud) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertInfMetSolicitud", solicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public MetInformacionAdicional obtenerInfoAdicional(String informacionAdicional) {
        MetInformacionAdicional infAdicional = new MetInformacionAdicional();
        try {
            infAdicional = ((List<MetInformacionAdicional>) ManagerMovilidad.getManager().obtenerListado("consultarInfAdicional", informacionAdicional)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return infAdicional;
    }

    @Override
    public String actualizarInfoAdicional(MetInformacionAdicional metInfoAdicional) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("actualizarInfoAdicional", metInfoAdicional);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String actualizarInfPersonalEst(MetInfUsuario metUsuario) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("actualizarInfPersonalEst", metUsuario);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String updateInfoIdioma(MetInfoIdioma metInfoIdioma) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("updateInfoIdioma", metInfoIdioma);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }


    /* ADMINISTRADOR*/
    @Override
    public String contSolicitudEnviada() {
        String contSolicitudesEnviadas = "0";
        try {
            contSolicitudesEnviadas = (String) ManagerMovilidad.getManager().obtenerRegistro("contSolicitudSinRevisar");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return contSolicitudesEnviadas;
    }

    @Override
    public List<MetInfUsuario> listarSolicitudSinRevisar() {

        List<MetInfUsuario> listSolicitudEnviada = new ArrayList();
        try {
            listSolicitudEnviada = (List<MetInfUsuario>) ManagerMovilidad.getManager().obtenerListado("listSolicitudSinRevisar");

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listSolicitudEnviada;
    }

    /*   @Override
    public List<MetInfUsuario> listarSolicitudRechazada() {

        List<MetInfUsuario> listSolicitudRechazada = new ArrayList();
        try {
            listSolicitudRechazada = (List<MetInfUsuario>) ManagerMovilidad.getManager().obtenerListado("listSolicitudRechazada");

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listSolicitudRechazada;
    }*/
    @Override
    public List<MetInfUsuario> listarSolicitudAprobada() {

        List<MetInfUsuario> listSolicitudAprobada = new ArrayList();
        try {
            listSolicitudAprobada = (List<MetInfUsuario>) ManagerMovilidad.getManager().obtenerListado("listSolicitudAprobada");

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listSolicitudAprobada;
    }

    @Override
    public MetInfUsuario obtenerAdmPersonalEntrante(String InfPersonalEntrante) {
        MetInfUsuario infPerEntrante = new MetInfUsuario();
        try {
            infPerEntrante = ((List<MetInfUsuario>) ManagerMovilidad.getManager().obtenerListado("infAdmPersonalEstEntrante", InfPersonalEntrante)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return infPerEntrante;
    }

    /* INSERT FORMULARIO*/
    @Override
    public String insertNuevaSolicitud(MetSolicitud newSolicitud) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertNuevaSolicitud", newSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    /* CONSULTAR INFORMACION FORMULARIOS ESTUDIANTE*/
    @Override
    public MetContactoEmergencia obtenerContactoEmergenciaSol(String infContactoEmergencia) {
        MetContactoEmergencia contactoEmergencia = new MetContactoEmergencia();
        try {
            contactoEmergencia = ((List<MetContactoEmergencia>) ManagerMovilidad.getManager().obtenerListado("obtenerContactoEmergencia", infContactoEmergencia)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return contactoEmergencia;
    }

    @Override
    public MetSolicitud contSolicitudPK(String solicitudPK) {
        MetSolicitud solicitudG = new MetSolicitud();
        try {
            solicitudG = ((List<MetSolicitud>) ManagerMovilidad.getManager().obtenerListado("consultarPKSolicitud", solicitudPK)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return solicitudG;
    }

    @Override
    public String insertNewSolicitud(MetSolicitud metNewSolicitud) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertNewSolicitud", metNewSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public MetSolicitud consultarPKSolicitud(String usuarioPK) {
        MetSolicitud solicitudPK = new MetSolicitud();
        try {
            solicitudPK = ((List<MetSolicitud>) ManagerMovilidad.getManager().obtenerListado("consultarPKSolicitud", usuarioPK)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return solicitudPK;
    }

    @Override
    public String insertFechaEnvio(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("insertFechaEnvio", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertInfoIdioma(MetInfoIdioma metinfIdioma) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("insertInfoIdioma", metinfIdioma);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public MetSolicitud obtenerInfSolicitud(String metsolicitudpk) {
        MetSolicitud metSolicitud = new MetSolicitud();
        try {
            metSolicitud = ((List<MetSolicitud>) ManagerMovilidad.getManager().obtenerListado("obtenerInfSolicitud", metsolicitudpk)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return metSolicitud;
    }

    @Override
    public MetSolicitud obtenerPromedioAcumulado(String codigoEstudiante) {
        MetSolicitud metSolicitud = new MetSolicitud();
        try {
            if (((List<MetSolicitud>) ManagerMovilidad.getManager().obtenerListado("getPromedioAcumulado", codigoEstudiante)).size() > 0) {
                metSolicitud = ((List<MetSolicitud>) ManagerMovilidad.getManager().obtenerListado("getPromedioAcumulado", codigoEstudiante)).get(0);
            } else {
                metSolicitud = new MetSolicitud();
                metSolicitud.setPromedioAcumulado("0");
            }

            System.out.println("promedio acumulado desde obtener promedio: " + metSolicitud.getPromedioAcumulado());
        } catch (Exception e) {
            log.error(e.getMessage());
            metSolicitud = new MetSolicitud();
            metSolicitud.setPromedioAcumulado("0");
        }
        return metSolicitud;
    }

    @Override
    public MetSolicitud obtenerPromedioAcumulado2(String codigoEstudiante) {
        MetSolicitud metSolicitud = new MetSolicitud();
        try {
            if (((List<MetSolicitud>) ManagerMovilidad.getManager().obtenerListado("getPromedioAcumulado2", codigoEstudiante)).size() > 0) {
                metSolicitud = ((List<MetSolicitud>) ManagerMovilidad.getManager().obtenerListado("getPromedioAcumulado2", codigoEstudiante)).get(0);
                System.out.println("promedio acumulado desde obtener promedio: " + metSolicitud.getPromedioAcumulado());
            } else {
                metSolicitud = new MetSolicitud();
                metSolicitud.setPromedioAcumulado("0");
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            metSolicitud = new MetSolicitud();
            metSolicitud.setPromedioAcumulado("0");
        }
        return metSolicitud;
    }

    @Override
    public MetSolicitud obtenerPromedioUltSemestre(String codigoEstudiante) {
        MetSolicitud metSolicitud = new MetSolicitud();
        try {
            metSolicitud = ((List<MetSolicitud>) ManagerMovilidad.getManager().obtenerListado("getPromedioUltSemestre", codigoEstudiante)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return metSolicitud;
    }

    @Override
    public String insertInfIdioma(MetInfoIdioma metInfIdioma) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("insertInfoIdioma", metInfIdioma);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public MetContactoEmergencia obtenerContactoEmergenciaFent(String metContatoEmergencia) {
        MetContactoEmergencia metContacto = new MetContactoEmergencia();
        try {
            metContacto = ((List<MetContactoEmergencia>) ManagerMovilidad.getManager().obtenerListado("obtenerContactoEmergenciaFEntr", metContatoEmergencia)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return metContacto;
    }

    @Override
    public String enviarSolicitud(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("estadoSolicitudSinRevisar", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String aprobarSolicitud(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("estadoSolicitudAprovada", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String rechazarSolicitud(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("estadoSolicitudRechazada", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public List<MetSolicitud> listarSolicitudesEnviadasFent(String solEnviada) {

        List<MetSolicitud> listSolicitudEnviadaent = new ArrayList();
        try {
            listSolicitudEnviadaent = (List<MetSolicitud>) ManagerMovilidad.getManager().obtenerListado("listarSolicitudesFest", solEnviada);

        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listSolicitudEnviadaent;
    }

    /* FORMULARIO ESTUDIANTE  */
    @Override
    public String insertEstadoSolicitud(MetEstadoSolicitud metEstadoSolicitud) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertEstadoSolicitud", metEstadoSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertPKObservacion(MetObservacionSolicitud metObservacion) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertPKObservacion", metObservacion);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertPKContactoEmergencia(MetContactoEmergencia metContactoEmergencia) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertPKContactoEmergencia", metContactoEmergencia);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertDistinciones(MetDistinciones metDistinciones) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertDistinciones", metDistinciones);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertPKEstadoSolicitud(MetEstadoSolicitud metEstadoSolicitud) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertPKEstadoSolicitud", metEstadoSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertMetInfoIdiomas(MetInfoIdioma metInfoIdioma) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertMetInfoIdioma", metInfoIdioma);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    /*UPDATES*/
    @Override
    public String cambiarEstadoSolicitud(MetEstadoSolicitud metEstadoSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("cambiarEstadoSolicitud", metEstadoSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String updateMetSolicitud(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("updateMetSolicitud", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String updatePKMetInstConvenio(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("updatePKMetInstConvenio", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String updatePKMetFacultadConvenio(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("updatePKMetFacultadConvenio", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String updatePKMetProgramaConvenio(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("updatePKMetProgramaConvenio", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String updatePKMetNivelEstudio(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("updatePKMetNivelEstudio", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String updateContactoEmergencia(MetContactoEmergencia metContactoEmergencia) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("updateContactoEmergencia", metContactoEmergencia);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String updateMetObservacion(MetObservacionSolicitud metObservacionSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("updateMetObservacion", metObservacionSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    /*LISTAS*/
    @Override
    public List<MetInstitucionConvenio> listaInstitucionesConvenio() {
        List<MetInstitucionConvenio> listInstitucionesConvenio = new ArrayList();
        try {
            listInstitucionesConvenio = (List<MetInstitucionConvenio>) ManagerMovilidad.getManager().obtenerListado("listaInstitucionesConvenio");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listInstitucionesConvenio;
    }

    @Override
    public List<MetFacultadInstOrigen> listaFacultadesConvenio() {
        List<MetFacultadInstOrigen> listFacultadConvenio = new ArrayList();
        try {
            listFacultadConvenio = (List<MetFacultadInstOrigen>) ManagerMovilidad.getManager().obtenerListado("listaFacultadesConvenio");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listFacultadConvenio;
    }

    @Override
    public List<MetProgramaInstOrigen> listaProgramasConvenio() {
        List<MetProgramaInstOrigen> listProgramasConvenio = new ArrayList();
        try {
            listProgramasConvenio = (List<MetProgramaInstOrigen>) ManagerMovilidad.getManager().obtenerListado("listaProgramasConvenio");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listProgramasConvenio;
    }

    @Override
    public List<MetNivelEstudioEntrante> listaNivelEstudio() {
        List<MetNivelEstudioEntrante> listNivelEstudio = new ArrayList();
        try {
            listNivelEstudio = (List<MetNivelEstudioEntrante>) ManagerMovilidad.getManager().obtenerListado("listaNivelEstudio");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listNivelEstudio;
    }

    @Override
    public List<MetProgramaKl> listarProgramasKL() {
        List<MetProgramaKl> listProgramaKL = new ArrayList();
        try {
            listProgramaKL = (List<MetProgramaKl>) ManagerMovilidad.getManager().obtenerListado("listarProgramasKL");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listProgramaKL;
    }

    @Override
    public List<MetCursosKL> listarCursosKL() {
        List<MetCursosKL> listCursoKL = new ArrayList();
        try {
            listCursoKL = (List<MetCursosKL>) ManagerMovilidad.getManager().obtenerListado("listarCursosKL");
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listCursoKL;
    }


    /*CONSULTAS*/
    @Override
    public String validarInfoIdioma(MetInfoIdioma metInfoIdioma) {
        String infIdiomaValido = "0";
        try {
            infIdiomaValido = (String) ManagerMovilidad.getManager().obtenerRegistro("validarInfoIdioma", metInfoIdioma);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return infIdiomaValido;
    }

    @Override
    public MetInfoIdioma obtenerMetInfoIdiomaEsp(String infIdiomaEsp) {
        MetInfoIdioma metInfIdiomaEsp = new MetInfoIdioma();
        try {
            metInfIdiomaEsp = ((List<MetInfoIdioma>) ManagerMovilidad.getManager().obtenerListado("obtenerMetInfoIdiomaEsp", infIdiomaEsp)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return metInfIdiomaEsp;
    }

    @Override
    public MetInfoIdioma obtenerMetInfoIdiomaIng(String infIdiomaIng) {
        MetInfoIdioma metInfIdiomaIng = new MetInfoIdioma();
        try {
            metInfIdiomaIng = ((List<MetInfoIdioma>) ManagerMovilidad.getManager().obtenerListado("obtenerMetInfoIdiomaIng", infIdiomaIng)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return metInfIdiomaIng;
    }

    @Override
    public MetInfoIdioma obtenerMetInfoIdiomaOtr(String infIdiomaOtr) {
        MetInfoIdioma metInfIdiomaOtr = new MetInfoIdioma();
        try {
            metInfIdiomaOtr = ((List<MetInfoIdioma>) ManagerMovilidad.getManager().obtenerListado("obtenerMetInfoIdiomaOtr", infIdiomaOtr)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return metInfIdiomaOtr;
    }

    @Override
    public MetSolicitud obtenerPKSolicitud(String solicitud) {
        MetSolicitud metSolicitud = new MetSolicitud();
        try {
            metSolicitud = ((List<MetSolicitud>) ManagerMovilidad.getManager().obtenerListado("consultarPKSolicitud", solicitud)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return metSolicitud;
    }

    @Override
    public MetEstadoSolicitud obtenerEstadoPkSol(String estadoSolicitud) {
        MetEstadoSolicitud metSolicitud = new MetEstadoSolicitud();
        try {
            metSolicitud = ((List<MetEstadoSolicitud>) ManagerMovilidad.getManager().obtenerListado("consultarPKEstado", estadoSolicitud)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return metSolicitud;
    }

    @Override
    public List<MetDistinciones> obtenerDistincion(String pkSolicitud) {
        List<MetDistinciones> listDistincion = new ArrayList();
        try {
            listDistincion = (List<MetDistinciones>) ManagerMovilidad.getManager().obtenerListado("consultarDistincion", pkSolicitud);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listDistincion;
    }

    @Override
    public List<MetInfoCursoKL> obtenerInfoCursoKL(String pkSolicitud) {
        List<MetInfoCursoKL> listInfoCurso = new ArrayList();
        try {
            listInfoCurso = (List<MetInfoCursoKL>) ManagerMovilidad.getManager().obtenerListado("consultarInfoCursoKL", pkSolicitud);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listInfoCurso;
    }

    @Override
    public List<MetInfoCursoKL> obtenerInfoCursoKLAlt(String pkSolicitud) {
        List<MetInfoCursoKL> listInfoCurso = new ArrayList();
        try {
            listInfoCurso = (List<MetInfoCursoKL>) ManagerMovilidad.getManager().obtenerListado("consultarInfoCursoKLAlt", pkSolicitud);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listInfoCurso;
    }

    @Override
    public String deleteDistincion(String metdistincion) {
        try {
            ManagerMovilidad.getManager().borrarRegistro("deleteDistincion", metdistincion);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String deleteInfoCurso(String metInfoCurso) {
        try {
            ManagerMovilidad.getManager().borrarRegistro("deleteInfoCurso", metInfoCurso);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String validaDistincion(String distincion) {
        String usuarioValido = "0";
        try {
            usuarioValido = (String) ManagerMovilidad.getManager().obtenerRegistro("validarDistincion", distincion);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return usuarioValido;
    }

    @Override
    public String insertInfoCursoKL(MetInfoCursoKL metInfoCursoKL) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertCursoKL", metInfoCursoKL);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertInfoCursoKLAlt(MetInfoCursoKL metInfoCursoKL) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertCursoKLAlt", metInfoCursoKL);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertHomologacionS(MetHomologacionSaliente metHomologacionSal) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertMetHomologacion", metHomologacionSal);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String insertHomologacionSALT(MetHomologacionSaliente metHomologacionSal) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("insertMetHomologacionALT", metHomologacionSal);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public List<MetHomologacionSaliente> obtenerHomologacion(String pkSolicitud) {
        List<MetHomologacionSaliente> listHomologacion = new ArrayList();
        try {
            listHomologacion = (List<MetHomologacionSaliente>) ManagerMovilidad.getManager().obtenerListado("consultarHomologacion", pkSolicitud);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listHomologacion;
    }

    @Override
    public List<MetHomologacionSaliente> obtenerHomologacionALT(String pkSolicitud) {
        List<MetHomologacionSaliente> listHomologacion = new ArrayList();
        try {
            listHomologacion = (List<MetHomologacionSaliente>) ManagerMovilidad.getManager().obtenerListado("consultarHomologacionALT", pkSolicitud);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listHomologacion;
    }

    @Override
    public String deleteHomologacion(String pkSolicitud) {
        try {
            ManagerMovilidad.getManager().borrarRegistro("deleteHomologacionS", pkSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    /*CRUD MET INSTITUCION CONVENIO*/
    @Override
    public MetInstitucionConvenio obtenerMetInstitucionConv(String pkInstitucion) {
        MetInstitucionConvenio metInstConvenio = new MetInstitucionConvenio();
        try {
            metInstConvenio = ((List<MetInstitucionConvenio>) ManagerMovilidad.getManager().obtenerListado("obtenerMetInstitucionConv", pkInstitucion)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return metInstConvenio;
    }

    /*CRUD MET TIPOMOVILIDAD*/
    @Override
    public String updatePKMetTipoMovilidad(MetSolicitud metSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("updatePKMetTipoMovilidad", metSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }


    /*-----------------------------------------------------------------------------------------------------------------------------------*/
    @Override
    public String crearSolicitud(MetSolicitud nuevaSolicitud) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("crearSolicitud", nuevaSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public MetInfUsuario obtenerDtUsuario(String usuario) {
        MetInfUsuario infUsuario = new MetInfUsuario();
        try {
            infUsuario = ((List<MetInfUsuario>) ManagerMovilidad.getManager().obtenerListado("obtenerDtUsuario", usuario)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return infUsuario;
    }

    @Override
    public MetSolicitud obtenerIDSolicitud(String pkUsuario) {
        MetSolicitud solicitudPK = new MetSolicitud();
        try {
            solicitudPK = ((List<MetSolicitud>) ManagerMovilidad.getManager().obtenerListado("obtenerIDSolicitud", pkUsuario)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return solicitudPK;
    }

    @Override
    public String definirEstadoSolicitud(MetEstadoSolicitud metEstadoSolicitud) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("definirEstadoSolicitud", metEstadoSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String definirContactoEmergencia(MetContactoEmergencia metContactoEmergencia) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("definirContEmergencia", metContactoEmergencia);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public MetEstadoSolicitud obtenerIDEstadoSolicitud(String solicitud) {
        MetEstadoSolicitud metEstadoSol = new MetEstadoSolicitud();
        try {
            metEstadoSol = ((List<MetEstadoSolicitud>) ManagerMovilidad.getManager().obtenerListado("obtenerIDEstadoSolicitud", solicitud)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return metEstadoSol;
    }

    /*-------------------- SQL --------------------------------*/
    @Override
    public String guardarInfPersonal(MetInfUsuario metInfoPersonal) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("guardarInfPersonal", metInfoPersonal);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String guardarOtraInstitucion(MetOtraInstitucionExt metOtraInstitucion) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("guardarOtraInstitucion", metOtraInstitucion);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String eliminarPKInstitucion(String pkSolicitud) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("eliminarPKInstitucion", pkSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String borrarOtraInstitucion(String pkSolicitud) {
        try {
            ManagerMovilidad.getManager().borrarRegistro("borrarOtraInstitucion", pkSolicitud);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public MetOtraInstitucionExt consultarInstitucionExt(String pkSolicitud) {
        MetOtraInstitucionExt otraInstitucion = new MetOtraInstitucionExt();
        try {
            otraInstitucion = ((List<MetOtraInstitucionExt>) ManagerMovilidad.getManager().obtenerListado("consultarInstitucionExt", pkSolicitud)).get(0);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return otraInstitucion;
    }

    @Override
    public String existeOtraInstitucion(String pkSolicitud) {
        String existeOtraInst = "0";
        try {
            existeOtraInst = (String) ManagerMovilidad.getManager().obtenerRegistro("existeOtraInstitucion", pkSolicitud);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return existeOtraInst;
    }

    @Override
    public String actualizarOtraInstitucion(MetOtraInstitucionExt metOtraInstitucionExt) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("actualizarOtraInstitucion", metOtraInstitucionExt);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String updateRHusuario(MetInfUsuario usuario) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("updateRHusuario", usuario);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String guardarArchivo(MetSoporteAdjunto metSoporteAdjunto) {
        try {
            ManagerMovilidad.getManager().insertarRegistro("guardarArchivo", metSoporteAdjunto);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public List<MetSoporteAdjunto> selectListArchivos(String pkSolicitud) {
        List<MetSoporteAdjunto> listSoporte = new ArrayList();
        try {
            listSoporte = (List<MetSoporteAdjunto>) ManagerMovilidad.getManager().obtenerListado("selectListArchivos", pkSolicitud);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return listSoporte;
    }

    @Override
    public String deleteSoporteAdjunto(String metSoporteAdjunto) {
        try {
            ManagerMovilidad.getManager().actualizarRegistro("eliminarSoporteAdjunto", metSoporteAdjunto);
            return Constantes.OK;
        } catch (Exception e) {
            log.error(e.getMessage());
            return Constantes.FAILED;
        }
    }

    @Override
    public String validaOtraInstitucion(String solicitudPK) {
        String existeOtraInstitucion = "0";
        try {
            existeOtraInstitucion = (String) ManagerMovilidad.getManager().obtenerRegistro("validarOtraInstitucion", solicitudPK);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return existeOtraInstitucion;
    }

    @Override
    public String validarCodigoEstudiante(String metInfUsuario) {
        String codigoValido = "0";
        try {
            codigoValido = (String) ManagerMovilidad.getManager().obtenerRegistro("validarCodigoEstudiante", metInfUsuario);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return codigoValido;
    }

    @Override
    public String existeUsuario(String metInfUsuario) {
        String codigoValido = "0";
        try {
            codigoValido = (String) ManagerMovilidad.getManager().obtenerRegistro("existeUsuario", metInfUsuario);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return codigoValido;
    }

    @Override
    public String getContrasenia(MetInfUsuario metInfUsuario) {
        String contrasenia = null;
        try {
            contrasenia = (String) ManagerMovilidad.getManager().obtenerRegistro("getContrasenia", metInfUsuario);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return contrasenia;
    }
}
