/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.client;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import edu.konrad.movilidad.bean.MetInfUsuario;
import edu.konrad.movilidad.bean.MetSolicitud;
import edu.konrad.movilidad.bean.MetSoporteAdjunto;
import edu.konrad.movilidad.constants.Constantes;
import edu.konrad.movilidad.controller.solicitudEntranteMB;
import edu.konrad.movilidad.servicios.ServiciosMovilidad;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author leidy.sarmiento
 */
public class FileMongoClient {

    private static FileMongoClient fileMongoClient = new FileMongoClient();
    private static FileMongoClient fileMongoClientAdjuntar = new FileMongoClient();
    private static final Logger logger = Logger.getLogger(FileMongoClient.class);

    public String idArchivo;

    public static FileMongoClient getFileMongoClient() {
        return fileMongoClient;
    }
    
    public static FileMongoClient getFileMongoAdjuntar() {
        return fileMongoClientAdjuntar;
    }

    public void cargarArchivo(UploadedFile file) throws IOException {
        logger.info("***** Entro a cargarArchivo");
        try {

            InputStream initialStream = file.getInputstream();
            File initialFile = new File(file.getFileName());
            FileUtils.copyInputStreamToFile(initialStream, initialFile);
            logger.info("nombre archivo a subir: " + initialFile.getName());

            final javax.ws.rs.client.Client client = javax.ws.rs.client.ClientBuilder.newBuilder().register(MultiPartFeature.class).build();

            javax.ws.rs.client.WebTarget t = client.target("http://192.168.99.45:8080/mongo/rest/").path("files").path("sendFileInput");

            FileDataBodyPart filePart = new FileDataBodyPart("file",
                    initialFile);

            filePart.setContentDisposition(
                    FormDataContentDisposition.name("file")
                            .fileName(initialFile.getName()).build());

            MultiPart multipartEntity = new FormDataMultiPart()
                    .field("name", initialFile.getName(), MediaType.TEXT_PLAIN_TYPE)
                    .bodyPart(filePart);

            javax.ws.rs.core.Response response = t.request().post(
                    Entity.entity(multipartEntity, multipartEntity.getMediaType()));

            System.out.println(response.getStatus());
            idArchivo = response.readEntity(String.class);
            response.close();
            logger.info("Id de archivo subido: " + idArchivo);
        } catch (Exception e) {
            logger.info("No fue posible subir el archivo: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public File leerArchivo(String id) {
        Client client = Client.create();
        logger.info("***** ENTRÓ A leerArchivo");
        //String id = "";
        String url = "http://192.168.99.45:8080/mongo/rest/files/getFile?fileId=" + id;
        WebResource webResource = client.resource(url);
        ClientResponse response = webResource.type(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        File archivo = response.getEntity(java.io.File.class);
        File archivoNombrado = new File(this.getNameFromRest(id));

        archivo.renameTo(archivoNombrado);
        logger.info("Nombre leerArchivo: " + archivoNombrado.getName());
        logger.info("Size leerArchivo: " + archivoNombrado.length());
        //logger.info("Nombre Ori 2: " + archivo.getName());
        //logger.info("Size Ori 2: " + archivo.length());

       archivo.delete();//nuevo
       
       return archivoNombrado;
    }

    public String getNameFromRest(String id) {
        logger.info("***** Entro a getNameFromRest");
        Client client = Client.create();
        String url = "http://192.168.99.45:8080/mongo/rest/files/getFileName?fileId=" + id;
        logger.info("ID de documento de descarga: " + id);
        WebResource webResource = client.resource(url);
        ClientResponse response = webResource.type(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        String name = response.getEntity(String.class);
        return name;
    }

    public File getArchivo(String codigo) {
        logger.info("ID de documento de descarga: " + codigo);
        Client client = Client.create();
        String url = "http://192.168.99.45:8080/mongo/rest/files/getFile?fileId=" + codigo;
        WebResource webResource = client.resource(url);
        ClientResponse response = webResource.type(MediaType.TEXT_PLAIN).get(ClientResponse.class);
        File archivo = response.getEntity(java.io.File.class);
       // File archivoNombrado = new File(this.getNameFromRest(codigo));
       // archivo.renameTo(archivoNombrado);
       /* logger.info("Nombre Original del archivo: " + archivo.getName());
        logger.info("Tamanio Original del archivo: " + archivo.length());
        logger.info("Nombre del archivo: " + archivoNombrado.getName());
        logger.info("Tamanio del archivo: " + archivoNombrado.length());   */ 
      //  archivoNombrado.delete();
        return archivo;
    }

    public String getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(String idArchivo) {
        this.idArchivo = idArchivo;
    }

    public String idArchivoUpload() {
        return idArchivo;
    }
}
