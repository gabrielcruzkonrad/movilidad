/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.controller;

import edu.konrad.movilidad.bean.MetContactoEmergencia;
import edu.konrad.movilidad.bean.MetEstadoSolicitud;
import edu.konrad.movilidad.bean.MetInfUsuario;
import edu.konrad.movilidad.bean.MetSolicitud;
import edu.konrad.movilidad.constants.Constantes;
import edu.konrad.movilidad.bean.UsuarioSesion;
import edu.konrad.movilidad.servicios.ServiciosMovilidadImpl;
import edu.konrad.movilidad.servicios.ServiciosMovilidad;
import edu.konrad.movilidad.utils.SendEmail;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Jose.suspes
 */
@ManagedBean
@SessionScoped
public class InicioMB {

    private final static Logger log = Logger.getLogger(InicioMB.class);

    private String servidorCorreo, puertoCorreo, emisor, asunto, mensaje, contrasena, tls, authentication;
    List<String> receptores;
    List<String> receptoresCopia;
    List<String> adjuntos;

    private ServiciosMovilidad servicios;
    private final MetEstadoSolicitud metEstadoSolicitud = new MetEstadoSolicitud();
    private final MetContactoEmergencia metContactoEmergencia = new MetContactoEmergencia();
    private MetSolicitud pkSolicitud;
    private MetInfUsuario metInfUsuarioEscolaris;
    private String nivelEducacion;
    private final MetSolicitud metSolicitudSaliente = new MetSolicitud();
    private MetInfUsuario existeUsuario = new MetInfUsuario();
    private String confContrasenia;

    private MetInfUsuario user;
    private UsuarioSesion usuarioSesion;
    private MetInfUsuario obtenerUsuario;
    private final MetSolicitud metSolicitud = new MetSolicitud();

    public String varIdioma;

    private MetInfUsuario metInfUsuario;
    private MetInfUsuario usuarioXemail;
    private final Calendar fechaSistema = Calendar.getInstance();
    private int anio, mes;
    public String ultimoPKusuario;

    Date now = new Date(System.currentTimeMillis());
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat hour = new SimpleDateFormat("HH:mm:ss");

    private boolean renderEstSaliente;
    private boolean renderEstEntrante;
    private boolean renderBtnExisteUsuario;

    private MetEstadoSolicitud varEstadoSolicitud = new MetEstadoSolicitud();
    private MetSolicitud IDsolicitud;

    private boolean aceptoTerminos;

    private String email;
    private String documentoIdentidad;

    @PostConstruct
    public void init() {
        this.usuarioSesion = new UsuarioSesion();
        this.servicios = new ServiciosMovilidadImpl();
        this.user = new MetInfUsuario();
        this.usuarioXemail = new MetInfUsuario();
        this.varIdioma = "es";
        this.renderEstEntrante = true;
        this.renderBtnExisteUsuario = true;
        limpiarCampos();
        mensajeInicial();
    }

    public String onIngresar() {
        String usuarioValido;
        usuarioValido = servicios.validaUsuario(usuarioSesion);
        if (!usuarioValido.equalsIgnoreCase(Constantes.ZERO)) {
            this.metInfUsuario = servicios.obtenerInfoUsuario(usuarioSesion.getNamUsuario());

            log.info("ROL: " + metInfUsuario.getMetRolUsuario().getPk());

            if ("2".equals(metInfUsuario.getMetRolUsuario().getPk())) {
                return "/vistas/formularioAdministrador.xhtml?faces-redirect=true";
            }

            if ("1".equals(metInfUsuario.getMetRolUsuario().getPk())) {
                this.IDsolicitud = servicios.obtenerIDSolicitud(metInfUsuario.getUsuarioPK());
                if ("1".equals(metInfUsuario.getMetTipoSolicitud().getPk())) {
                    varEstadoSolicitud = servicios.obtenerIDEstadoSolicitud(IDsolicitud.getSolicitudPK());
                    log.info("Estado de la solicitud: " + varEstadoSolicitud.getMetTipoEstadoSolicitud().getPk());
                    if (("1".equals(varEstadoSolicitud.getMetTipoEstadoSolicitud().getPk()))
                            || ("2".equals(varEstadoSolicitud.getMetTipoEstadoSolicitud().getPk()))
                            || ("5".equals(varEstadoSolicitud.getMetTipoEstadoSolicitud().getPk())))// Aprobada o Enviada
                    {
                        return "/vistas/solicitudEntranteConsultar.xhtml?faces-redirect=true";
                    }

                    if ("3".equals(varEstadoSolicitud.getMetTipoEstadoSolicitud().getPk()))//Guardada
                    {
                        return "/vistas/solicitudEntranteV.xhtml?faces-redirect=true";
                    }
                }

                if ("2".equals(metInfUsuario.getMetTipoSolicitud().getPk())) {
                    varEstadoSolicitud = servicios.obtenerIDEstadoSolicitud(IDsolicitud.getSolicitudPK());
                    log.info("Estado de la solicitud: " + varEstadoSolicitud.getMetTipoEstadoSolicitud().getPk());
                    if (("1".equals(varEstadoSolicitud.getMetTipoEstadoSolicitud().getPk()))
                            || ("2".equals(varEstadoSolicitud.getMetTipoEstadoSolicitud().getPk()))
                            || ("5".equals(varEstadoSolicitud.getMetTipoEstadoSolicitud().getPk())))// Aprobada o Enviada
                    {
                        return "/vistas/solicitudSalienteConsultar.xhtml?faces-redirect=true";
                    }
                    if ("3".equals(varEstadoSolicitud.getMetTipoEstadoSolicitud().getPk()))//Guardada
                    {
                        return "/vistas/solicitudSalienteV.xhtml?faces-redirect=true";
                    }
                }
            }

        } else {
            if ("es".equals(varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Usuario no registrado. Para acceder a la aplicación realice el registro correspondiente."));
            }

            if ("en".equals(varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Unregistered user. To access the application, perform the corresponding registration"));
            }

            return "";
        }
        return null;
    }

    public void onValidarUsuario() {
        String usuarioValido;
        usuarioValido = servicios.obtenerUsuario(user);
        if (!usuarioValido.equalsIgnoreCase(Constantes.ZERO)) {
            if ("es".equals(varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "El nombre de usuario ya existe"));
            }

            if ("en".equals(varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Username exist"));
            }
        } else {
        }
    }

    public String onValidarUserRegistrado(String cadena) {
        Pattern pat = Pattern.compile("abc");
        Matcher mat = pat.matcher(cadena);
        if (mat.matches()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "SI"));
            return "";
        } else {
            if ("es".equals(varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Usuario no registrado. Para acceder a la aplicación realice el registro correspondiente."));
            }
            if ("en".equals(varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Usuario no registrado. Para acceder a la aplicación realice el registro correspondiente."));
            }

            return "";
        }
    }

    public void onValidarCodigoEstudiantil() {
        String codigoEstudiantilValido;
        codigoEstudiantilValido = servicios.validarCodigoEstudiante(user.getUsuario());
        log.info("******* USUARIO VALIDO : " + codigoEstudiantilValido + "USUARIO: " + user.getUsuario());
        if (!codigoEstudiantilValido.equalsIgnoreCase(Constantes.ZERO)) {
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "El código estudiantil ingresado no existe. Por favor"
                    + " verifique e ingrese su código nuevamente."));
            user.setUsuario(null);
        }

        String usuarioValido;
        usuarioValido = servicios.obtenerUsuario(user);
        if (!usuarioValido.equalsIgnoreCase(Constantes.ZERO)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "El código estudiantil ya se encuentra registrado en el "
                    + " sistema. Por favor verifique e ingrese su código nuevamente."));
            user.setUsuario(null);
        }
    }

    public void validarCodigoEstudiantil() {
        String codigoValido;
        codigoValido = servicios.validarCodigoEstudiante(user.getUsuario());
        if (!codigoValido.equalsIgnoreCase(Constantes.ZERO)) {
            log.info("VALIDÓ CODIGO Y ENTRO A COPIAR DATOS");
            this.metInfUsuarioEscolaris = servicios.obtenerEstSaliente(user.getUsuario());
            this.user.setNombres(metInfUsuarioEscolaris.getNombres());
            this.user.setApellidos(metInfUsuarioEscolaris.getApellidos());
            if ("Femenino".equals(metInfUsuarioEscolaris.getMetGenero().getNombreEspaniol())) {
                this.user.getMetGenero().setPk("1");
            }
            if ("Masculino".equals(metInfUsuarioEscolaris.getMetGenero().getNombreEspaniol())) {
                this.user.getMetGenero().setPk("2");
            }

            if ("Cédula de Ciudadanía".equals(metInfUsuarioEscolaris.getMetTipoIdentificacion().getNombre())) {
                this.user.getMetTipoIdentificacion().setPk("1");
            }
            if ("Pasaporte".equals(metInfUsuarioEscolaris.getMetTipoIdentificacion().getNombre())) {
                this.user.getMetTipoIdentificacion().setPk("3");
            }
            this.user.setNumeroIdentificacion(metInfUsuarioEscolaris.getNumeroIdentificacion());
            this.user.setOtraCiudad(metInfUsuarioEscolaris.getLugarNacimiento());
            this.user.setFechaNacimiento(metInfUsuarioEscolaris.getFechaNacimiento());
            this.user.setNacionalidad(metInfUsuarioEscolaris.getNacionalidad());
            this.user.setDireccionResidencia(metInfUsuarioEscolaris.getDireccionResidencia());
            this.user.setTelCasa(metInfUsuarioEscolaris.getTelCasa());
            this.user.setCelular(metInfUsuarioEscolaris.getCelular());
            this.user.setEmailInstitucional(metInfUsuarioEscolaris.getEmailInstitucional());
            this.user.setEmailAlternativo(metInfUsuarioEscolaris.getEmailAlternativo());
            this.user.setLugarExpedicion(metInfUsuarioEscolaris.getLugarExpedicion());
            defineNivelEducacion();
            this.metSolicitudSaliente.getMetNivelEstEntrante().setPk(nivelEducacion);
            this.metSolicitudSaliente.setOtraFacultadEntrante(metInfUsuarioEscolaris.getFacultadEstSaliente());
            this.metSolicitudSaliente.setSemestreEntrante(metInfUsuarioEscolaris.getSemestreEstSaliente());
            this.metSolicitudSaliente.setOtroProgramaInstConvenio(metInfUsuarioEscolaris.getProgramaEstSaliente());
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "El código estudiantil ingresado no existe. Por favor ingrese de nuevo su código esudiantil y verifique que se encuentra escrito correctamente."));
        }
    }

    public void defineNivelEducacion() {

        if ("P".equals(metInfUsuarioEscolaris.getTipoPrograma())) {
            nivelEducacion = "1";
        }

        if ("E".equals(metInfUsuarioEscolaris.getTipoPrograma())) {

            Boolean findEspecializacion = metInfUsuarioEscolaris.getProgramaEstSaliente().contains("Especializacion");
            if (findEspecializacion == true) {
                nivelEducacion = "5";
            }

            Boolean findMaestria = metInfUsuarioEscolaris.getProgramaEstSaliente().contains("Maestria");
            if (findMaestria == true) {
                nivelEducacion = "2";
            }

            Boolean findDoctorado = metInfUsuarioEscolaris.getProgramaEstSaliente().contains("Doctorado");
            if (findDoctorado == true) {
                nivelEducacion = "3";
            }
        }

        if ("D".equals(metInfUsuarioEscolaris.getTipoPrograma())) {

            Boolean findDiplomado = metInfUsuarioEscolaris.getProgramaEstSaliente().contains("Diplomado");
            if (findDiplomado == true) {
                nivelEducacion = "6";
            }
        }
        System.err.println("1 . NIVEL DE EDUCACIÓN: " + nivelEducacion);
    }

    public void definirTipoEstudiante() {
        if ("1".equals(user.getMetTipoSolicitud().getPk())) {
            limpiarCampos2();
            renderEstEntrante = true;
            renderEstSaliente = false;
            mensajeInicialInternacional();
        }

        if ("2".equals(user.getMetTipoSolicitud().getPk())) {
            limpiarCampos2();
            renderEstEntrante = false;
            renderEstSaliente = true;
            mensajeInicialKonradista();
        }
    }

    public void definirEstadoSolicitud() {
        pkSolicitud = servicios.obtenerIDSolicitud(obtenerUsuario.getUsuarioPK());
        metEstadoSolicitud.getMetTipoEstadoSolicitud().setPk(Constantes.ESTADOSOL_GUARDADA);
        metEstadoSolicitud.getMetSolicitud().setSolicitudPK(pkSolicitud.getSolicitudPK());
        String resEstadoSolicitud;
        resEstadoSolicitud = servicios.definirEstadoSolicitud(metEstadoSolicitud);
        if (resEstadoSolicitud.equalsIgnoreCase(Constantes.FAILED)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Se ha producido un error al crear el estado de la solicitud,"
                    + "por favor comuníquese con el admisnitrador de la plataforma"));
            log.info("CREACION DE ESTADO DE SOLICITUD INCORRECTA");
        } else {
            log.info("CREACION DE ESTADO CORRECTA");
        }
    }

    public void definirContactoEmergencia() {
        metContactoEmergencia.getMetSolicitud().setSolicitudPK(pkSolicitud.getSolicitudPK());
        String resContactoEmergencia;
        resContactoEmergencia = servicios.definirContactoEmergencia(metContactoEmergencia);
        if (resContactoEmergencia.equalsIgnoreCase(Constantes.FAILED)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Se ha producido un error al crear la solicitud,"
                    + "por favor comuníquese con el admisnitrador de la plataforma"));
            log.info("Creación de contacto emergencia incorrecta");

        } else {
            log.info("CREACION DE CONTACTO EMERGENCIA CORRECTA");
        }
    }

    /*Guardar Registro*/
    public void guardarRegistro() {

        if (("".equals(user.getMetTipoSolicitud().getPk()))
                || ("".equals(user.getNombres()))
                || ("".equals(user.getApellidos()))
                || ("".equals(user.getEmailInstitucional()))
                || ("".equals(user.getUsuario()))
                || ("".equals(user.getContrasenia()))
                || ("".equals(confContrasenia))) {
            if ("es".equals(varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Existen campos vacíos, por favor verifique he inténtelo de nuevo."));
            }
            if ("en".equals(varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Empty fields, please verify the try again."));
            }
        }

        if (aceptoTerminos == true) {
            log.info(" -- INGRESO A GUARDAR REGISTRO");
            String res;
            this.user.getMetRolUsuario().setPk(Constantes.ROL_BASE);
            this.user.setEstado(Constantes.ESTADO_BASE);
            log.info("***** Tipo de estudiante: " + user.getMetTipoSolicitud().getPk());
            if ("2".equals(user.getMetTipoSolicitud().getPk())) {
                validarCodigoEstudiantil();
            }
            res = servicios.insertMetInfUsuario(this.user);
            if (res.equalsIgnoreCase(Constantes.FAILED)) {
                if ("es".equals(varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Registro fallido, intente más tarde."));
                }
                if ("en".equals(varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Failed registration, try later."));
                }

            } else {
                crearNuevaSolicitud();
                RequestContext context = RequestContext.getCurrentInstance();
                context.execute("PF('dlgMensajeExito').show();");
                enviarCorreoConfirmacion();
            }
        }

        if (aceptoTerminos == false) {
            if ("es".equals(varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Debe aceptar las condiciones para continuar con el registro."));
            }
            if ("en".equals(varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "You must accept terms and conditions to continue the registration"));
            }
        }

    }

    public void crearNuevaSolicitud() {
        log.info("ENTRO A CREAR CONSECUTIVO");
        obtenerUsuario = servicios.obtenerDtUsuario(user.getUsuario());
        metSolicitud.getMetTipoSolicitud().setPk(obtenerUsuario.getMetTipoSolicitud().getPk());
        log.info("PK USUARIO: " + obtenerUsuario.getUsuarioPK());
        if ("2".equals(user.getMetTipoSolicitud().getPk())) {
            this.user.setUsuarioPK(obtenerUsuario.getUsuarioPK());
            String infoPersonal;
            log.info("ENTRO A GUARDAR DATOS COPIADOS: " + user.getUsuarioPK());
            infoPersonal = servicios.guardarInfPersonal(user);
            if ((infoPersonal.equalsIgnoreCase(Constantes.FAILED))) {
                log.info("ERROR al Guardar datos del estudiante saliente");
            } else {
                log.info("GUARDO datos del estudiante saliente");
            }
        }
        anio = fechaSistema.get(Calendar.YEAR);
        mes = fechaSistema.get(Calendar.MONTH) + 1;

        if (mes < 10) {
            metSolicitud.setConsecutivo("MOV_" + (Integer.toString(anio)) + "_0" + (Integer.toString(mes)) + "_" + obtenerUsuario.getUsuarioPK());
        }
        if (mes >= 10) {
            metSolicitud.setConsecutivo("MOV_" + (Integer.toString(anio)) + "_" + (Integer.toString(mes)) + "_" + obtenerUsuario.getUsuarioPK());
        }
        log.info("CONSECUTIVO: " + metSolicitud.getConsecutivo());

        metSolicitud.getMetInfUsuario().setUsuarioPK(obtenerUsuario.getUsuarioPK());
        String NuevaSolicitud;
        NuevaSolicitud = servicios.crearSolicitud(metSolicitud);
        if (NuevaSolicitud.equalsIgnoreCase(Constantes.FAILED)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Se ha producido un error al crear la solicitud"));
            log.info("CREACION DE SOLICITUD INCORRECTA");
        } else {
            log.info("CREACION DE SOLICITUD CORRECTA");
            definirEstadoSolicitud();
            definirContactoEmergencia();
            if ("2".equals(user.getMetTipoSolicitud().getPk())) {
                metSolicitudSaliente.setSolicitudPK(pkSolicitud.getSolicitudPK());
                String resMetSolicitud;
                resMetSolicitud = servicios.updateMetSolicitud(metSolicitudSaliente);
                if ((resMetSolicitud.equalsIgnoreCase(Constantes.FAILED))) {
                    log.info(" ERROR CREACION DE COPIA DE DATOS INCORRECTA");
                } else {
                    log.info("CREACION DE COPIA DE DATOS CORRECTA");
                }
                String resMetNivelEstudiopk;
                resMetNivelEstudiopk = servicios.updatePKMetNivelEstudio(metSolicitudSaliente);
                if ((resMetNivelEstudiopk.equalsIgnoreCase(Constantes.FAILED))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar NIVEL ESTUDIO"));
                } else {
                    log.info("GUARDO NIVEL DE ESTUDIO CORRECTAMENTE");
                }
            }
        }
    }

    public void limpiarCampos() {
        user.setApellidos(null);
        user.setNombres(null);
        user.setEmailAlternativo(null);
        user.setEmailInstitucional(null);
        user.setUsuario(null);
        user.setContrasenia(null);
        user.getMetTipoSolicitud().setPk(null);
        usuarioSesion.setNamUsuario(null);
        usuarioSesion.setNamContrasenia(null);
    }

    public void limpiarCampos2() {
        user.setApellidos(null);
        user.setNombres(null);
        user.setEmailAlternativo(null);
        user.setEmailInstitucional(null);
        user.setUsuario(null);
        user.setContrasenia(null);
        usuarioSesion.setNamUsuario(null);
        usuarioSesion.setNamContrasenia(null);
    }

    public String getVarIdioma() {
        return varIdioma;
    }

    public void setVarIdioma(String varIdioma) {
        this.varIdioma = varIdioma;
    }

    public static Logger getLog() {
        return log;
    }

    public ServiciosMovilidad getServicios() {
        return servicios;
    }

    public void setServicios(ServiciosMovilidad servicios) {
        this.servicios = servicios;
    }

    public UsuarioSesion getUsuarioSesion() {
        return usuarioSesion;
    }

    public void setUsuarioSesion(UsuarioSesion usuarioSesion) {
        this.usuarioSesion = usuarioSesion;
    }

    public MetInfUsuario getMetInfUsuario() {
        return metInfUsuario;
    }

    public void setMetInfUsuario(MetInfUsuario metInfUsuario) {
        this.metInfUsuario = metInfUsuario;
    }

    public MetInfUsuario getUser() {
        return user;
    }

    public void setUser(MetInfUsuario user) {
        this.user = user;
    }

    public boolean isRenderEstSaliente() {
        return renderEstSaliente;
    }

    public void setRenderEstSaliente(boolean renderEstSaliente) {
        this.renderEstSaliente = renderEstSaliente;
    }

    public boolean isRenderEstEntrante() {
        return renderEstEntrante;
    }

    public void setRenderEstEntrante(boolean renderEstEntrante) {
        this.renderEstEntrante = renderEstEntrante;
    }

    private boolean renderEstudiantExterno;
    private boolean renderEstudiantSaliente;

    public boolean isRenderEstudiantExterno() {
        return renderEstudiantExterno;
    }

    public void setRenderEstudiantExterno(boolean renderEstudiantExterno) {
        this.renderEstudiantExterno = renderEstudiantExterno;
    }

    public boolean isRenderEstudiantSaliente() {
        return renderEstudiantSaliente;
    }

    public void setRenderEstudiantSaliente(boolean renderEstudiantSaliente) {
        this.renderEstudiantSaliente = renderEstudiantSaliente;
    }

    public void mensajeInicialKonradista() {
        renderImagen = false;
        renderEstudiantSaliente = true;
        renderEstudiantExterno = false;
        msjLine1 = "Estimado Konradista, te damos la bienvenida a nuestra Plataforma de Movilidad.";
        msjLine2 = "Si no te encuentras registrado en nuestra Plataforma, te invitamos a que te registres en nuestro sistema teniendo en cuenta la siguiente información:";
        msjLine3 = "- Tu código estudiantil será tu usuario.";
        msjLine4 = "- Tu contraseña debe ser alfanumérica y tener mínimo 6 caracteres.";
        msjLine5 = "- Al registrarte recibirás un correo de confirmación a tu correo institucionl y personal indicando que tu registro ha sido exitoso.";
    }

    public void mensajeInicialInternacional() {
        renderImagen = false;
        renderEstudiantSaliente = false;
        renderEstudiantExterno = true;
        msjLine1 = "Estimado Estudiante Internacional, te damos la bienvenida a nuestra Plataforma de Movilidad.";
        msjLine2 = "Si no te encuentras registrado en nuestra Plataforma, te invitamos a que te registres en nuestro sistema teniendo en cuenta la siguiente información:";
        msjLine3 = "- Tu nombre de usuario debe tener mínimo 2 caracteres.";
        msjLine4 = "- Tu contraseña debe ser alfanumérica y tener mínimo 6 caracteres.";
        msjLine5 = "- Al registrarte recibirás un correo de confirmación a tu correo indicando que tu registro ha sido exitoso.";
    }

    private boolean renderImagen;
    private boolean renderTextoIncial;

    public boolean isRenderImagen() {
        return renderImagen;
    }

    public void setRenderImagen(boolean renderImagen) {
        this.renderImagen = renderImagen;
    }

    public boolean isRenderTextoIncial() {
        return renderTextoIncial;
    }

    public void setRenderTextoIncial(boolean renderTextoIncial) {
        this.renderTextoIncial = renderTextoIncial;
    }

    public void mensajeInicial() {
        renderImagen = true;
        renderTextoIncial = false;
    }

    public void enviarCorreoConfirmacion() {
        SendEmail enviarCorreo = new SendEmail();
        if (this.getAdjuntos() == null) {
            this.setAdjuntos(new ArrayList<String>());
        } else {
            if (this.getAdjuntos().size() > 0) {
                this.getAdjuntos().clear();
            }
        }

        if (this.getReceptores() == null) {
            this.setReceptores(new ArrayList<String>());
        } else {
            if (this.getReceptores().size() > 0) {
                this.getReceptores().clear();
            }
        }

        if (this.getReceptoresCopia() == null) {
            this.setReceptoresCopia(new ArrayList<String>());
        } else {
            if (this.getReceptoresCopia().size() > 0) {
                this.getReceptoresCopia().clear();
            }
        }

        try {
            this.servidorCorreo = "smtp.office365.com";
            this.puertoCorreo = "587";
            this.emisor = "movilidad@konradlorenz.edu.co";
            this.contrasena = "Konrad2018";

            log.info("CORREO 1: " + user.getEmailInstitucional());
            log.info("CORREO 2: " + user.getEmailAlternativo());
            if (user.getEmailInstitucional() != null) {
                this.getReceptores().add(user.getEmailInstitucional());
            }

            if (user.getEmailAlternativo() != null) {
                this.getReceptoresCopia().add(user.getEmailAlternativo());
            }

            if ("es".equals(varIdioma)) {
                this.asunto = "Confirmación de Registro - Plataforma de Movilidad Fundacion Universitaria Konrad Lorenz";
                this.mensaje = ("<p><span style=\"font-size: 12pt; font-family: calibri, sans-serif;\"><strong>Estimado&nbsp;</strong></span>" + user.getNombres() + " " + user.getApellidos() + "</p>\n"
                        + "<p><span style=\"font-size: 11pt; font-family: calibri, sans-serif;\">Agradecemos tu inter&eacute;s en la Fundaci&oacute;n Universitaria Konrad Lorenz. Tu registro en nuestra Plataforma de Movilidad ha sido exitoso.</span></p>\n"
                        + "<p><span style=\"font-size: 11pt; font-family: calibri, sans-serif;\">Te invitamos a iniciar tu proceso de postulaci&oacute;n en nuestro Programa de Movilidad a trav&eacute;s de la plataforma.</span></p>");
            }

            if ("en".equals(varIdioma)) {
                this.asunto = "Registration Confirmation - Konrad Lorenz University Foundation Mobility Platform";
                this.mensaje = ("<p><span style=\"font-size: 12pt; font-family: calibri, sans-serif;\"><strong>Dear&nbsp;</strong></span>" + user.getNombres() + " " + user.getApellidos() + "</p>\n"
                        + "<p><span style=\"font-size: 11pt; font-family: calibri, sans-serif;\">Thank you for your interest in &ldquo;Fundaci&oacute;n Universitaria Konrad Lorenz&rdquo;. Your registration in our Mobility Platform has been successful.</span></p>\n"
                        + "<p><span style=\"font-size: 11pt; font-family: calibri, sans-serif;\">We invite you to apply in our Mobility Program through the platform.</span></p>");
            }
            this.tls = "true";
            this.authentication = "true";

            enviarCorreo.envia(servidorCorreo, puertoCorreo, emisor, asunto, receptores, receptoresCopia, mensaje, adjuntos, contrasena, tls, authentication);
        } catch (Exception e) {
            e.printStackTrace();
            log.info(e.getMessage());
        }
    }

    public void restablecerContra() {
        try {
            MetInfUsuario usuario;
            this.usuarioXemail.setEmailAlternativo(usuarioXemail.getEmailInstitucional());
            usuario = servicios.obtenerUsuarioXEmail(usuarioXemail);

            System.out.println("email ALT: " + usuarioXemail.getEmailAlternativo());
            System.out.println("email INST: " + usuarioXemail.getEmailInstitucional());
            System.out.println("documento: " + usuarioXemail.getNumeroIdentificacion());

            System.out.println("email ALT registrado: " + usuario.getEmailAlternativo());
            System.out.println("email INST registrado: " + usuario.getEmailInstitucional());
            System.out.println("documento registrado: " + usuario.getNumeroIdentificacion());
            System.out.println("contraseña: " + usuario.getContrasenia());

            if ((usuario.getEmailAlternativo() == null || usuario.getEmailInstitucional() == null) && usuario.getNumeroIdentificacion() == null) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                        "El número de identificación y/o correo electrónico no se encuentran registrados en el sistema."));
            } else {
                if ((usuario.getEmailAlternativo().equals(usuario.getEmailAlternativo())
                        || usuario.getEmailInstitucional().equals(usuario.getEmailInstitucional()))
                        && usuario.getNumeroIdentificacion().equals(usuario.getNumeroIdentificacion())) {
                    try {
                        SendEmail enviarCorreo = new SendEmail();
                        if (this.getAdjuntos() == null) {
                            this.setAdjuntos(new ArrayList<String>());
                        } else {
                            if (this.getAdjuntos().size() > 0) {
                                this.getAdjuntos().clear();
                            }
                        }

                        if (this.getReceptores() == null) {
                            this.setReceptores(new ArrayList<String>());
                        } else {
                            if (this.getReceptores().size() > 0) {
                                this.getReceptores().clear();
                            }
                        }

                        if (this.getReceptoresCopia() == null) {
                            this.setReceptoresCopia(new ArrayList<String>());
                        } else {
                            if (this.getReceptoresCopia().size() > 0) {
                                this.getReceptoresCopia().clear();
                            }
                        }
                        try {
                            this.servidorCorreo = "smtp.office365.com";
                            this.puertoCorreo = "587";
                            this.emisor = "movilidad@konradlorenz.edu.co";
                            this.contrasena = "Konrad2018";
                            this.asunto = "Credenciales de acceso Plataforma Movilidad Estudiantil Fundación Universitaria Konrad Lorenz";

                            if (usuario.getEmailInstitucional() != null) {
                                this.getReceptores().add(usuario.getEmailInstitucional());
                            }
                            if (usuario.getEmailAlternativo() != null) {
                                this.getReceptores().add(usuario.getEmailAlternativo());
                            }
                            this.mensaje = ("<p>Estimado(a) " + usuario.getNombres() + " " + usuario.getApellidos() + ". </p>"
                                    + "<p>Sus credenciales de acceso son: "
                                    + "</p>"
                                    + "<p><strong>Usuario: </strong>" + usuario.getUsuario()
                                    + "<p><strong>Contraseña: </strong>" + usuario.getContrasenia()
                                    + "<p>&nbsp;</p>"
                                    + "<p>La Fundaci&oacute;n Universitaria Konrad Lorenz declara que protege los datos suministrados en virtud de lo dispuesto en la normatividad regulatoria del derecho al HABEAS DATA, e informa que sus datos ser&aacute;n tratados en forma confidencial y no ser&aacute;n utilizados para ning&uacute;n prop&oacute;sito distinto a los acad&eacute;micos.</p>"
                                    + "<p>&nbsp;</p>");
                            this.tls = "true";
                            this.authentication = "true";
                            enviarCorreo.envia(servidorCorreo, puertoCorreo, emisor, asunto, receptores, receptoresCopia, mensaje, adjuntos, contrasena, tls, authentication);
                            this.usuarioXemail.setEmailInstitucional(null);
                            this.usuarioXemail.setEmailAlternativo(null);
                            this.usuarioXemail.setNumeroIdentificacion(null);
                        } catch (Exception e) {
                            e.printStackTrace();
                            log.info(e.getMessage());
                        }
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "¡Correo Enviado!",
                                "Hemos enviado un mensaje indicando tus credenciales de acceso, por favor revisa el correo que indicaste anteriomente."));                          
                    } catch (Exception e) {
                        log.error(e.getMessage());
                        log.info("Error:No ha sido posible enviar correo de restablecimiento de contraseña.");
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error",
                                "No ha sido posible enviar el correo de restablecimiento de contraseña. Por favor inténtelo de nuevo."));
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            log.info("Error:El número de identificación y correo electrónico no se encuentran registrados en el sistema.");
        }
    }

    public void consultarContrasenia() {
        String resContrasenia;
        resContrasenia = servicios.getContrasenia(existeUsuario);
        System.out.println("*** edu.konrad.movilidad.controller.InicioMB.consultarContrasenia():  " + resContrasenia);
        if (resContrasenia.equalsIgnoreCase(Constantes.FAILED)) {
            log.info("Error al cargar la contrasenia");
        } else {
            if (resContrasenia.equals(existeUsuario.getContrasenia())) {
                log.info("Contrasenias coinciden");
            }
        }
    }

    private String msjLine1, msjLine2, msjLine3, msjLine4, msjLine5, msjLine6;

    public String getMsjLine1() {
        return msjLine1;
    }

    public void setMsjLine1(String msjLine1) {
        this.msjLine1 = msjLine1;
    }

    public String getMsjLine2() {
        return msjLine2;
    }

    public void setMsjLine2(String msjLine2) {
        this.msjLine2 = msjLine2;
    }

    public String getMsjLine3() {
        return msjLine3;
    }

    public void setMsjLine3(String msjLine3) {
        this.msjLine3 = msjLine3;
    }

    public String getMsjLine4() {
        return msjLine4;
    }

    public void setMsjLine4(String msjLine4) {
        this.msjLine4 = msjLine4;
    }

    public String getMsjLine5() {
        return msjLine5;
    }

    public void setMsjLine5(String msjLine5) {
        this.msjLine5 = msjLine5;
    }

    public String getMsjLine6() {
        return msjLine6;
    }

    public void setMsjLine6(String msjLine6) {
        this.msjLine6 = msjLine6;
    }

    public String getServidorCorreo() {
        return servidorCorreo;
    }

    public void setServidorCorreo(String servidorCorreo) {
        this.servidorCorreo = servidorCorreo;
    }

    public String getPuertoCorreo() {
        return puertoCorreo;
    }

    public void setPuertoCorreo(String puertoCorreo) {
        this.puertoCorreo = puertoCorreo;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getTls() {
        return tls;
    }

    public void setTls(String tls) {
        this.tls = tls;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public List<String> getReceptores() {
        return receptores;
    }

    public void setReceptores(List<String> receptores) {
        this.receptores = receptores;
    }

    public List<String> getReceptoresCopia() {
        return receptoresCopia;
    }

    public void setReceptoresCopia(List<String> receptoresCopia) {
        this.receptoresCopia = receptoresCopia;
    }

    public List<String> getAdjuntos() {
        return adjuntos;
    }

    public void setAdjuntos(List<String> adjuntos) {
        this.adjuntos = adjuntos;
    }

    public MetInfUsuario getExisteUsuario() {
        return existeUsuario;
    }

    public void setExisteUsuario(MetInfUsuario existeUsuario) {
        this.existeUsuario = existeUsuario;
    }

    public boolean isRenderBtnExisteUsuario() {
        return renderBtnExisteUsuario;
    }

    public void setRenderBtnExisteUsuario(boolean renderBtnExisteUsuario) {
        this.renderBtnExisteUsuario = renderBtnExisteUsuario;
    }

    private boolean renderPnlRestablecer;

    public boolean isRenderPnlRestablecer() {
        return renderPnlRestablecer;
    }

    public void setRenderPnlRestablecer(boolean renderPnlRestablecer) {
        this.renderPnlRestablecer = renderPnlRestablecer;
    }

    public boolean isAceptoTerminos() {
        return aceptoTerminos;
    }

    public void setAceptoTerminos(boolean aceptoTerminos) {
        this.aceptoTerminos = aceptoTerminos;
    }

    public String getConfContrasenia() {
        return confContrasenia;
    }

    public void setConfContrasenia(String confContrasenia) {
        this.confContrasenia = confContrasenia;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDocumentoIdentidad() {
        return documentoIdentidad;
    }

    public void setDocumentoIdentidad(String documentoIdentidad) {
        this.documentoIdentidad = documentoIdentidad;
    }

    public String onCambiarContrasenia() {
        return "/vistas/VistaRestablecerContrasenia.xhtml?faces-redirect=true";
    }

    public String onVolverInicio() {
        return "/index.xhtml?faces-redirect=true";
    }

    public MetInfUsuario getUsuarioXemail() {
        return usuarioXemail;
    }

    public void setUsuarioXemail(MetInfUsuario usuarioXemail) {
        this.usuarioXemail = usuarioXemail;
    }

}
