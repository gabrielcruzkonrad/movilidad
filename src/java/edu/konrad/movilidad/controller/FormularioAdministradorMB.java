/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.controller;

import edu.konrad.movilidad.bean.MetInfUsuario;
import edu.konrad.movilidad.bean.MetSolicitud;
import edu.konrad.movilidad.bean.MetTipoSolicitud;
import edu.konrad.movilidad.servicios.ServiciosMovilidad;
import edu.konrad.movilidad.servicios.ServiciosMovilidadImpl;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;


/**
 *
 * @author leidy.sarmiento
 */
@ManagedBean
@ViewScoped
public class FormularioAdministradorMB {

     private final static Logger log = Logger.getLogger(FormularioAdministradorMB.class);
    
    private ServiciosMovilidad servicios;
    private MetInfUsuario metInfUsuario;
    private List<MetInfUsuario> listarSolicitudSR;
    private List<MetInfUsuario> listarSolicitudAp;
    private List<MetInfUsuario> listarSolicitudRch;
    private List<MetInfUsuario> filtrarSolicitud;
    private List<MetInfUsuario> filtrarSolicitud1;
    private List<MetInfUsuario> filtrarSolicitud2;
    private String contSolicitudEnviadas;
    public MetInfUsuario usuarioEstudiante;
    private MetInfUsuario selectedInfEstudiante;
    private MetSolicitud metSolicitudPK = new MetSolicitud();
    List<MetTipoSolicitud> tiposSolicitud;
    private String varFechaEnvioSolicitud;

    @ManagedProperty(value = "#{inicioMB}")
    private InicioMB inicioMB;

    @PostConstruct
    public void init() {
        this.servicios = new ServiciosMovilidadImpl();
        this.listarSolicitudSR = servicios.listarSolicitudSinRevisar();
        this.listarSolicitudAp = servicios.listarSolicitudAprobada();
       // this.listarSolicitudRch = servicios.listarSolicitudRechazada();
        this.metInfUsuario = servicios.obtenerInfoUsuario(inicioMB.getUsuarioSesion().getNamUsuario());
        this.metInfUsuario.setUsuario(inicioMB.getUsuarioSesion().getNamUsuario());
        this.contSolicitudEnviadas = servicios.contSolicitudEnviada();
        this.tiposSolicitud = servicios.tiposSolicitud();
    }

    public String getVarFechaEnvioSolicitud() {
        return varFechaEnvioSolicitud;
    }

    public void setVarFechaEnvioSolicitud(String varFechaEnvioSolicitud) {
        this.varFechaEnvioSolicitud = varFechaEnvioSolicitud;
    }

    public List<MetInfUsuario> getFiltrarSolicitud1() {
        return filtrarSolicitud1;
    }

    public void setFiltrarSolicitud1(List<MetInfUsuario> filtrarSolicitud1) {
        this.filtrarSolicitud1 = filtrarSolicitud1;
    }

    public List<MetInfUsuario> getFiltrarSolicitud2() {
        return filtrarSolicitud2;
    }

    public void setFiltrarSolicitud2(List<MetInfUsuario> filtrarSolicitud2) {
        this.filtrarSolicitud2 = filtrarSolicitud2;
    }

    public MetSolicitud getMetSolicitudPK() {
        return metSolicitudPK;
    }

    public void setMetSolicitudPK(MetSolicitud metSolicitudPK) {
        this.metSolicitudPK = metSolicitudPK;
    }

    public MetInfUsuario getSelectedInfEstudiante() {
        return selectedInfEstudiante;
    }

    public void setSelectedInfEstudiante(MetInfUsuario selectedInfEstudiante) {
        this.selectedInfEstudiante = selectedInfEstudiante;
    }

    public MetInfUsuario getUsuarioEstudiante() {
        return usuarioEstudiante;
    }

    public void setUsuarioEstudiante(MetInfUsuario usuarioEstudiante) {
        this.usuarioEstudiante = usuarioEstudiante;
    }

    public List<MetInfUsuario> getListarSolicitudAp() {
        return listarSolicitudAp;
    }

    public void setListarSolicitudAp(List<MetInfUsuario> listarSolicitudAp) {
        this.listarSolicitudAp = listarSolicitudAp;
    }

    public List<MetInfUsuario> getListarSolicitudRch() {
        return listarSolicitudRch;
    }

    public void setListarSolicitudRch(List<MetInfUsuario> listarSolicitudRch) {
        this.listarSolicitudRch = listarSolicitudRch;
    }

    public List<MetTipoSolicitud> getTiposSolicitud() {
        return tiposSolicitud;
    }

    public void setTiposSolicitud(List<MetTipoSolicitud> tiposSolicitud) {
        this.tiposSolicitud = tiposSolicitud;
    }

    public String getContSolicitudEnviadas() {
        return contSolicitudEnviadas;
    }

    public void setContSolicitudEnviadas(String contSolicitudEnviadas) {
        this.contSolicitudEnviadas = contSolicitudEnviadas;
    }

    public List<MetInfUsuario> getFiltrarSolicitud() {
        return filtrarSolicitud;
    }

    public void setFiltrarSolicitud(List<MetInfUsuario> filtrarSolicitud) {
        this.filtrarSolicitud = filtrarSolicitud;
    }

    public List<MetInfUsuario> getListarSolicitudSR() {
        return listarSolicitudSR;
    }

    public void setListarSolicitudSR(List<MetInfUsuario> listarSolicitudSR) {
        this.listarSolicitudSR = listarSolicitudSR;
    }

    public ServiciosMovilidad getServicios() {
        return servicios;
    }

    public void setServicios(ServiciosMovilidad servicios) {
        this.servicios = servicios;
    }

    public MetInfUsuario getMetInfUsuario() {
        return metInfUsuario;
    }

    public void setMetInfUsuario(MetInfUsuario metInfUsuario) {
        this.metInfUsuario = metInfUsuario;
    }

    public InicioMB getInicioMB() {
        return inicioMB;
    }

    public void setInicioMB(InicioMB inicioMB) {
        this.inicioMB = inicioMB;
    }

    public String onCerrarSesion() {
        return "/index.xhtml?faces-redirect=true";
    }

    public String pruebaq() {

        if ("Entrante".equals(selectedInfEstudiante.getMetTipoSolicitud().getNombreEspaniol())) {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("estudiante", selectedInfEstudiante);
            return "/vistas/consultarAdmSolicitudEntrante.xhtml?faces-redirect=true";
        }

        if ("Saliente".equals(selectedInfEstudiante.getMetTipoSolicitud().getNombreEspaniol())) {
            FacesContext.getCurrentInstance().getExternalContext().getFlash().put("estudiante", selectedInfEstudiante);
            return "/vistas/consultarAdmSolicitudSaliente.xhtml?faces-redirect=true";
        }
        return null;
    }
    
    public void mostrarFecha(Date fecha){
         DateFormat fechaHora = new SimpleDateFormat("yyyy/MM/dd");
         varFechaEnvioSolicitud = fechaHora.format(fecha);
    }

    public void logout() {
        log.info("Logout de la sesion: " + ((inicioMB.getUsuarioSesion().getNamUsuario() == null) ? inicioMB.getUsuarioSesion().getNamUsuario() : ""));
        ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
        String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
        try {
            ((HttpSession) ctx.getSession(false)).invalidate();
            ctx.redirect(ctxPath + "/faces/index.xhtml?faces-redirect=true");
        } catch (IOException ex) {
            log.info("Error" + ex.getMessage());
        }
    }

}
