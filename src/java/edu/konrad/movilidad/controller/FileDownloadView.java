/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.controller;

/**
 *
 * @author leidy.sarmiento
 */
import java.io.InputStream;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean
public class FileDownloadView {

    private StreamedContent descargarArchivo;
    //private solicitudSalienteMB archivo;

    public StreamedContent getDescargarArchivo() {
        return descargarArchivo;
    }

    public void selectArchivoDownload(String caso) {
       // System.out.println(" --- ENTRO A DESCRAGAR ARCHIVO " + archivo.getDescarga());
        System.out.println("*** entro a selectArchivoDownload " + caso);
        InputStream stream = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/resources/documents/" + caso);
        descargarArchivo = new DefaultStreamedContent(stream, "application/docx", caso);
    }
}
