/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.controller;

import edu.konrad.movilidad.bean.IdiomaBean;
import edu.konrad.movilidad.bean.MetCiudad;
import edu.konrad.movilidad.bean.MetContactoEmergencia;
import edu.konrad.movilidad.bean.MetCursosKL;
import edu.konrad.movilidad.bean.MetDistinciones;
import edu.konrad.movilidad.bean.MetEstadoSolicitud;
import edu.konrad.movilidad.bean.MetFacultadInstOrigen;
import edu.konrad.movilidad.bean.MetGenero;
import edu.konrad.movilidad.bean.MetHomologacionSaliente;
import edu.konrad.movilidad.bean.MetIdiomas;
import edu.konrad.movilidad.bean.MetInfPerfilMedico;
import edu.konrad.movilidad.bean.MetInfUsuario;
import edu.konrad.movilidad.bean.MetInfoCursoKL;
import edu.konrad.movilidad.bean.MetInfoIdioma;
import edu.konrad.movilidad.bean.MetInformacionAdicional;
import edu.konrad.movilidad.bean.MetInstitucionConvenio;
import edu.konrad.movilidad.bean.MetNivelEstudioEntrante;
import edu.konrad.movilidad.bean.MetObservacionSolicitud;
import edu.konrad.movilidad.bean.MetOtraInstitucionExt;
import edu.konrad.movilidad.bean.MetPais;
import edu.konrad.movilidad.bean.MetParentesco;
import edu.konrad.movilidad.bean.MetPerfilMedico;
import edu.konrad.movilidad.bean.MetProgramaInstOrigen;
import edu.konrad.movilidad.bean.MetProgramaKl;
import edu.konrad.movilidad.bean.MetSolicitud;
import edu.konrad.movilidad.bean.MetSoporteAdjunto;
import edu.konrad.movilidad.bean.MetTipoIdentificacion;
import edu.konrad.movilidad.bean.MetTipoMovilidad;
import edu.konrad.movilidad.bean.SMTPAuthenticator;
import edu.konrad.movilidad.client.FileMongoClient;
import edu.konrad.movilidad.constants.Constantes;
import edu.konrad.movilidad.servicios.ServiciosMovilidad;
import edu.konrad.movilidad.servicios.ServiciosMovilidadImpl;
import edu.konrad.movilidad.utils.GenerarFile;
import edu.konrad.movilidad.utils.SendEmail;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import static org.primefaces.component.focus.Focus.PropertyKeys.context;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author leidy.sarmiento
 */
@ManagedBean
@ViewScoped
public class consultarAdmSolicitudEntrante {

    private final static Logger log = Logger.getLogger(solicitudEntranteMB.class);

    private String servidorCorreo, puertoCorreo, emisor, asunto, mensaje, contrasena, tls, authentication;
    List<String> receptores;
    List<String> receptoresCopia;
    List<String> adjuntos;

    private boolean renderBtnEnviar;
    private MetSoporteAdjunto selectArchivo;

    private ServiciosMovilidad servicios;
    private MetSolicitud pkSolicitud;
    private MetInfUsuario obtenerIDUsuario;
    private MetInfUsuario usuarioAdministrador;

    private MetInfUsuario metInfUsuario;
    private MetSolicitud usuarioSolPK = new MetSolicitud();
    private MetSolicitud metSolicitud;
    private MetSolicitud selectedSolicitud;
    private MetDistinciones selectedDistincion;
    private MetInfoCursoKL selectedInfoCursoKL;

    private MetInstitucionConvenio metInstitucionConvenio;
    private MetEstadoSolicitud metEstadoSolicitud = new MetEstadoSolicitud();

    private MetInfoIdioma idiomaEspanol = new MetInfoIdioma();
    private MetInfoIdioma idiomaIngles = new MetInfoIdioma();
    private MetInfoIdioma idiomaOtro = new MetInfoIdioma();

    private MetObservacionSolicitud metObservacion = new MetObservacionSolicitud();
    private MetHomologacionSaliente metHomologacionSaliente = new MetHomologacionSaliente();
    private MetDistinciones metDistinciones = new MetDistinciones();
    private MetContactoEmergencia metContactoEmergencia = new MetContactoEmergencia();
    private MetSoporteAdjunto metSoporteAdjunto = new MetSoporteAdjunto();
    private MetPerfilMedico metPerfilMedico = new MetPerfilMedico();
    private MetInformacionAdicional metInfoAdicional = new MetInformacionAdicional();
    private MetInfoCursoKL metInfoCursoKL = new MetInfoCursoKL();
    private MetInfoCursoKL metInfoCursoKLAlt = new MetInfoCursoKL();
    private MetInfPerfilMedico metInfPerfilMedico = new MetInfPerfilMedico();

    private boolean mostrarFormLista;
    private Date fecha = new Date();

    private List<MetSolicitud> listaSolicitudes;
    private List<MetSolicitud> filtrarSolicitud;
    private List<MetDistinciones> listaDistinciones;
    private List<MetInfoCursoKL> listaInfoCursoKL;
    private List<MetInfoCursoKL> listaInfoCursoKLAlt;

    @ManagedProperty(value = "#{inicioMB}")
    private InicioMB inicioMB;

    List<MetParentesco> tiposParentesco;
    List<MetTipoMovilidad> tiposMovilidad;
    List<MetNivelEstudioEntrante> NivelEstudio;
    List<MetCiudad> listaCiudad;
    List<MetPais> listaPais;
    List<MetCiudad> listaCiudadPais;
    List<MetInstitucionConvenio> listInstitucionesConvenio;
    List<MetTipoIdentificacion> tiposIdentificacion;
    List<MetGenero> tiposGenero;

    List<MetInstitucionConvenio> listaInstitucionesConvenio;
    List<MetFacultadInstOrigen> listaFacultadConvenio;
    List<MetProgramaInstOrigen> listaProgramaConvenio;
    List<MetNivelEstudioEntrante> listaNivelEstudioEntrante;
    List<MetProgramaKl> listaProgramasKL;
    List<MetCursosKL> listaCursosKL;

    private final List<String> itemsDocAdjunto = new ArrayList<String>();
    private final List<String> itemsSemestre = new ArrayList<String>();
    private final List<String> itemsTipoSangre = new ArrayList<String>();
    private final List<String> itemsSiNo = new ArrayList<String>();

    private boolean renderPCrearSolicitud;
    private boolean renderPTablaSolicitud;
    private boolean disabledIdiomaEsp, disabledIdiomaIng, disabledIdiomaOtr, idiomaNativoEsp, idiomaNativoIng, idiomaNativoOtr;
    private boolean renderedDistinciones;
    private boolean renderedCursosKL, renderedCursosKLAlt;
    private boolean renderedOtroParentesco;
    private boolean renderedPOtraInst;

    private int diasMovilidad, mDuracionMovilidad, dDuracionMovilidad;
    private String duracionMov;

    public ArrayList<String> arrayAspectosMedicos = new ArrayList<String>();

    /*Inf Inst Convenio*/
    private boolean selectOtraInstitucion, disableInstitucion, disableOtrInstitucion, varIptOtraCiudad, disabledCamposContacto, disableCheckOtraInst, disablebtnInst;
    private String convenioInstitucion, existeConvenio;

    /*INF ACADEMICA*/
    private boolean disTipMovilidad, disFecInicioMov, disFecFinMov, disDuracionMov, disMotivoEst, disbtnEspaniol, disbtnIngles, disbtnOtroidioma;

    /*INF ADICIONAL*/
    private boolean disVigenciaDesde, disVigenciaHasta, disDirAlojamiento, disTelAlojamiento;

    private boolean botonCrearSolicitud, tabFormEntrante;
    //  private String existeSolicitud;
    private boolean renderedDuracionMovilidad;

    private boolean renderedPersonal;
    private boolean renderedInstOrigen;
    private boolean renderedAcademica;
    private boolean renderedMovilidad;
    private boolean renderedPrfilMedico;
    private boolean renderedDocAdjuntos;
    private int variablePanel;
    private boolean renderbtnSiguiente;
    private boolean renderbtnEnviar;
    private boolean renderAtras;

    private boolean renderSolicitud;

    private boolean disOtroNivelEducacion, disNivelEducacion, selecOtroNivelEdu;

    private IdiomaBean idiomaBean = new IdiomaBean();

    private StreamedContent fileTest;

    /*---------- SQL ------------*/
    private MetOtraInstitucionExt metOtraInstitucion = new MetOtraInstitucionExt();

    private boolean campoIngles, campoEspaniol;

    private List<MetSoporteAdjunto> listSoporteAdjunto;

    private boolean renderedInsitucionOrigen;

    private String existeOtraInsti;
    private MetInfUsuario metInfUsuariosol;

    @PostConstruct
    public void init() {

        this.servicios = new ServiciosMovilidadImpl();
        this.usuarioAdministrador = servicios.obtenerInfoUsuario(inicioMB.getUsuarioSesion().getNamUsuario());
        this.metInfUsuariosol = (MetInfUsuario) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("estudiante");

        //----------------------------------------  CONSULTAS -----------------------------------
        this.idiomaBean.doCambioIdiomaLink(inicioMB.varIdioma);
        this.metInfUsuario = servicios.obtenerInfoUsuario(metInfUsuariosol.getUsuario());
        this.obtenerIDUsuario = servicios.obtenerDtUsuario(metInfUsuario.getUsuario());
        this.pkSolicitud = servicios.obtenerIDSolicitud(obtenerIDUsuario.getUsuarioPK());
        this.metSolicitud = servicios.obtenerInfSolicitud(pkSolicitud.getSolicitudPK());
        this.metContactoEmergencia = servicios.obtenerContactoEmergenciaFent(pkSolicitud.getSolicitudPK());
        this.metInstitucionConvenio = servicios.obtenerInfInstitucionConvenio(metSolicitud.getMetInstitucionConvenio().getPk());
        this.listaSolicitudes = servicios.listarSolicitudesEnviadasFent(metInfUsuario.getUsuarioPK());

        //-------------------------------------- LISTAS ---------------------------------------
        this.tiposIdentificacion = servicios.tiposIdentificacion();
        this.listaPais = servicios.listaPais();
        this.tiposGenero = servicios.tiposGenero();
        this.tiposParentesco = servicios.tiposParentesco();
        this.tiposMovilidad = servicios.tiposMovilidad();
        this.NivelEstudio = servicios.nivelesEstudio();
        this.listSoporteAdjunto = servicios.selectListArchivos(pkSolicitud.getSolicitudPK());

        //------------------------------------------ VARIABLES DEL FORMULARIO
        this.renderedPersonal = true;
        this.renderbtnSiguiente = true;
        this.renderAtras = true;

        this.usuarioSolPK = servicios.consultarPKSolicitud(metInfUsuario.getUsuarioPK());

        this.listaInstitucionesConvenio = servicios.listaInstitucionesConvenio();
        this.listaFacultadConvenio = servicios.listaFacultadesConvenio();
        this.listaProgramaConvenio = servicios.listaProgramasConvenio();
        this.listaNivelEstudioEntrante = servicios.listaNivelEstudio();
        this.listaProgramasKL = servicios.listarProgramasKL();
        this.listaCursosKL = servicios.listarCursosKL();

        this.idiomaEspanol = servicios.obtenerMetInfoIdiomaEsp(pkSolicitud.getSolicitudPK());
        this.idiomaIngles = servicios.obtenerMetInfoIdiomaIng(pkSolicitud.getSolicitudPK());
        this.idiomaOtro = servicios.obtenerMetInfoIdiomaOtr(pkSolicitud.getSolicitudPK());
        this.listaDistinciones = servicios.obtenerDistincion(pkSolicitud.getSolicitudPK());
        this.listaInfoCursoKL = servicios.obtenerInfoCursoKL(pkSolicitud.getSolicitudPK());
        this.listaInfoCursoKLAlt = servicios.obtenerInfoCursoKLAlt(pkSolicitud.getSolicitudPK());

        this.renderedDistinciones = true;
        this.renderedCursosKL = true;
        this.renderedCursosKLAlt = true;
        this.mostrarFormLista = false;

        this.disOtroNivelEducacion = true;

        seccionInfMovilidad();
        contDistincion();
        otroParentesco();
        definirIdioma();

        itemsDocAdjunto.add("Pasaporte");
        itemsDocAdjunto.add("Carta de motivación");
        itemsDocAdjunto.add("Certificado original de notas");
        itemsDocAdjunto.add("Solvencia económica");
        itemsDocAdjunto.add("Carta de compromiso");
        itemsDocAdjunto.add("Carta de aplicación");

        itemsSemestre.add("Primero (First)");
        itemsSemestre.add("Segundo (Second)");
        itemsSemestre.add("Tercero (Third)");
        itemsSemestre.add("Cuarto (Fourth)");
        itemsSemestre.add("Quinto (Fifth)");
        itemsSemestre.add("Sexto (Sixth)");
        itemsSemestre.add("Séptimo (Seventh)");
        itemsSemestre.add("Octavo (Eighth)");
        itemsSemestre.add("Noveno (Nineth)");
        itemsSemestre.add("Décimo (Tenth)");

        itemsTipoSangre.add("A-");
        itemsTipoSangre.add("A+");
        itemsTipoSangre.add("B-");
        itemsTipoSangre.add("B+");
        itemsTipoSangre.add("AB-");
        itemsTipoSangre.add("AB+");
        itemsTipoSangre.add("O-");
        itemsTipoSangre.add("O+");

        /*-------------------------------------------------------------------*/
        this.metOtraInstitucion = servicios.consultarInstitucionExt(pkSolicitud.getSolicitudPK());

        definirEstadoSolicitud();
        validaInstitucionOrigen();

    }

    public StreamedContent getFileTest() {
        return fileTest;
    }

    public void setFileTest(StreamedContent fileTest) {
        this.fileTest = fileTest;
    }

    public IdiomaBean getIdiomaBean() {
        return idiomaBean;
    }

    public void setIdiomaBean(IdiomaBean idiomaBean) {
        this.idiomaBean = idiomaBean;
    }

    public boolean isRenderbtnEnviar() {
        return renderbtnEnviar;
    }

    public void setRenderbtnEnviar(boolean renderbtnEnviar) {
        this.renderbtnEnviar = renderbtnEnviar;
    }

    public boolean isRenderedDocAdjuntos() {
        return renderedDocAdjuntos;
    }

    public void setRenderedDocAdjuntos(boolean renderedDocAdjuntos) {
        this.renderedDocAdjuntos = renderedDocAdjuntos;
    }

    public boolean isRenderAtras() {
        return renderAtras;
    }

    public void setRenderAtras(boolean renderAtras) {
        this.renderAtras = renderAtras;
    }

    public boolean isRenderbtnSiguiente() {
        return renderbtnSiguiente;
    }

    public void setRenderbtnSiguiente(boolean renderbtnSiguiente) {
        this.renderbtnSiguiente = renderbtnSiguiente;
    }

    public boolean isRenderedInstOrigen() {
        return renderedInstOrigen;
    }

    public void setRenderedInstOrigen(boolean renderedInstOrigen) {
        this.renderedInstOrigen = renderedInstOrigen;
    }

    public boolean isRenderedAcademica() {
        return renderedAcademica;
    }

    public void setRenderedAcademica(boolean renderedAcademica) {
        this.renderedAcademica = renderedAcademica;
    }

    public boolean isRenderedMovilidad() {
        return renderedMovilidad;
    }

    public void setRenderedMovilidad(boolean renderedMovilidad) {
        this.renderedMovilidad = renderedMovilidad;
    }

    public boolean isRenderedPrfilMedico() {
        return renderedPrfilMedico;
    }

    public void setRenderedPrfilMedico(boolean renderedPrfilMedico) {
        this.renderedPrfilMedico = renderedPrfilMedico;
    }

    public int getVariablePanel() {
        return variablePanel;
    }

    public void setVariablePanel(int variablePanel) {
        this.variablePanel = variablePanel;
    }

    public boolean isRenderedPersonal() {
        return renderedPersonal;
    }

    public void setRenderedPersonal(boolean renderedPersonal) {
        this.renderedPersonal = renderedPersonal;
    }

    public boolean isRenderedDuracionMovilidad() {
        return renderedDuracionMovilidad;
    }

    public void setRenderedDuracionMovilidad(boolean renderedDuracionMovilidad) {
        this.renderedDuracionMovilidad = renderedDuracionMovilidad;
    }

    public boolean isRenderedCursosKLAlt() {
        return renderedCursosKLAlt;
    }

    public void setRenderedCursosKLAlt(boolean renderedCursosKLAlt) {
        this.renderedCursosKLAlt = renderedCursosKLAlt;
    }

    public MetInfoCursoKL getSelectedInfoCursoKL() {
        return selectedInfoCursoKL;
    }

    public void setSelectedInfoCursoKL(MetInfoCursoKL selectedInfoCursoKL) {
        this.selectedInfoCursoKL = selectedInfoCursoKL;
    }

    public List<MetInfoCursoKL> getListaInfoCursoKLAlt() {
        return listaInfoCursoKLAlt;
    }

    public void setListaInfoCursoKLAlt(List<MetInfoCursoKL> listaInfoCursoKLAlt) {
        this.listaInfoCursoKLAlt = listaInfoCursoKLAlt;
    }

    public List<MetInfoCursoKL> getListaInfoCursoKL() {
        return listaInfoCursoKL;
    }

    public void setListaInfoCursoKL(List<MetInfoCursoKL> listaInfoCursoKL) {
        this.listaInfoCursoKL = listaInfoCursoKL;
    }

    public List<MetCursosKL> getListaCursosKL() {
        return listaCursosKL;
    }

    public void setListaCursosKL(List<MetCursosKL> listaCursosKL) {
        this.listaCursosKL = listaCursosKL;
    }

    public int getmDuracionMovilidad() {
        return mDuracionMovilidad;
    }

    public void setmDuracionMovilidad(int mDuracionMovilidad) {
        this.mDuracionMovilidad = mDuracionMovilidad;
    }

    public int getdDuracionMovilidad() {
        return dDuracionMovilidad;
    }

    public void setdDuracionMovilidad(int dDuracionMovilidad) {
        this.dDuracionMovilidad = dDuracionMovilidad;
    }

    public String getDuracionMov() {
        return duracionMov;
    }

    public void setDuracionMov(String duracionMov) {
        this.duracionMov = duracionMov;
    }

    public int getDiasMovilidad() {
        return diasMovilidad;
    }

    public void setDiasMovilidad(int diasMovilidad) {
        this.diasMovilidad = diasMovilidad;
    }

    public List<MetProgramaKl> getListaProgramasKL() {
        return listaProgramasKL;
    }

    public void setListaProgramasKL(List<MetProgramaKl> listaProgramasKL) {
        this.listaProgramasKL = listaProgramasKL;
    }

    public boolean isRenderedPOtraInst() {
        return renderedPOtraInst;
    }

    public void setRenderedPOtraInst(boolean renderedPOtraInst) {
        this.renderedPOtraInst = renderedPOtraInst;
    }

    public boolean isRenderedOtroParentesco() {
        return renderedOtroParentesco;
    }

    public void setRenderedOtroParentesco(boolean renderedOtroParentesco) {
        this.renderedOtroParentesco = renderedOtroParentesco;
    }

    public MetDistinciones getSelectedDistincion() {
        return selectedDistincion;
    }

    public void setSelectedDistincion(MetDistinciones selectedDistincion) {
        this.selectedDistincion = selectedDistincion;
    }

    public List<MetDistinciones> getListaDistinciones() {
        return listaDistinciones;
    }

    public void setListaDistinciones(List<MetDistinciones> listaDistinciones) {
        this.listaDistinciones = listaDistinciones;
    }

    public boolean isRenderedCursosKL() {
        return renderedCursosKL;
    }

    public void setRenderedCursosKL(boolean renderedCursosKL) {
        this.renderedCursosKL = renderedCursosKL;
    }

    public boolean isRenderedDistinciones() {
        return renderedDistinciones;
    }

    public void setRenderedDistinciones(boolean renderedDistinciones) {
        this.renderedDistinciones = renderedDistinciones;
    }

    public boolean isIdiomaNativoIng() {
        return idiomaNativoIng;
    }

    public void setIdiomaNativoIng(boolean idiomaNativoIng) {
        this.idiomaNativoIng = idiomaNativoIng;
    }

    public boolean isIdiomaNativoOtr() {
        return idiomaNativoOtr;
    }

    public void setIdiomaNativoOtr(boolean idiomaNativoOtr) {
        this.idiomaNativoOtr = idiomaNativoOtr;
    }

    public boolean isIdiomaNativoEsp() {
        return idiomaNativoEsp;
    }

    public void setIdiomaNativoEsp(boolean idiomaNativoEsp) {
        this.idiomaNativoEsp = idiomaNativoEsp;
    }

    public boolean isDisabledIdiomaIng() {
        return disabledIdiomaIng;
    }

    public void setDisabledIdiomaIng(boolean disabledIdiomaIng) {
        this.disabledIdiomaIng = disabledIdiomaIng;
    }

    public boolean isDisabledIdiomaOtr() {
        return disabledIdiomaOtr;
    }

    public void setDisabledIdiomaOtr(boolean disabledIdiomaOtr) {
        this.disabledIdiomaOtr = disabledIdiomaOtr;
    }

    public boolean isDisabledIdiomaEsp() {
        return disabledIdiomaEsp;
    }

    public void setDisabledIdiomaEsp(boolean disabledIdiomaEsp) {
        this.disabledIdiomaEsp = disabledIdiomaEsp;
    }

    public boolean isRenderPCrearSolicitud() {
        return renderPCrearSolicitud;
    }

    public void setRenderPCrearSolicitud(boolean renderPCrearSolicitud) {
        this.renderPCrearSolicitud = renderPCrearSolicitud;
    }

    public boolean isRenderPTablaSolicitud() {
        return renderPTablaSolicitud;
    }

    public void setRenderPTablaSolicitud(boolean renderPTablaSolicitud) {
        this.renderPTablaSolicitud = renderPTablaSolicitud;
    }

    public List<MetInstitucionConvenio> getListaInstitucionesConvenio() {
        return listaInstitucionesConvenio;
    }

    public void setListaInstitucionesConvenio(List<MetInstitucionConvenio> listaInstitucionesConvenio) {
        this.listaInstitucionesConvenio = listaInstitucionesConvenio;
    }

    public List<MetFacultadInstOrigen> getListaFacultadConvenio() {
        return listaFacultadConvenio;
    }

    public void setListaFacultadConvenio(List<MetFacultadInstOrigen> listaFacultadConvenio) {
        this.listaFacultadConvenio = listaFacultadConvenio;
    }

    public List<MetProgramaInstOrigen> getListaProgramaConvenio() {
        return listaProgramaConvenio;
    }

    public void setListaProgramaConvenio(List<MetProgramaInstOrigen> listaProgramaConvenio) {
        this.listaProgramaConvenio = listaProgramaConvenio;
    }

    public List<MetNivelEstudioEntrante> getListaNivelEstudioEntrante() {
        return listaNivelEstudioEntrante;
    }

    public void setListaNivelEstudioEntrante(List<MetNivelEstudioEntrante> listaNivelEstudioEntrante) {
        this.listaNivelEstudioEntrante = listaNivelEstudioEntrante;
    }

    public MetInfPerfilMedico getMetInfPerfilMedico() {
        return metInfPerfilMedico;
    }

    public void setMetInfPerfilMedico(MetInfPerfilMedico metInfPerfilMedico) {
        this.metInfPerfilMedico = metInfPerfilMedico;
    }

    public MetObservacionSolicitud getMetObservacion() {
        return metObservacion;
    }

    public void setMetObservacion(MetObservacionSolicitud metObservacion) {
        this.metObservacion = metObservacion;
    }

    public MetHomologacionSaliente getMetHomologacionSaliente() {
        return metHomologacionSaliente;
    }

    public void setMetHomologacionSaliente(MetHomologacionSaliente metHomologacionSaliente) {
        this.metHomologacionSaliente = metHomologacionSaliente;
    }

    public MetDistinciones getMetDistinciones() {
        return metDistinciones;
    }

    public void setMetDistinciones(MetDistinciones metDistinciones) {
        this.metDistinciones = metDistinciones;
    }

    public MetSoporteAdjunto getMetSoporteAdjunto() {
        return metSoporteAdjunto;
    }

    public void setMetSoporteAdjunto(MetSoporteAdjunto metSoporteAdjunto) {
        this.metSoporteAdjunto = metSoporteAdjunto;
    }

    public MetInfoCursoKL getMetInfoCursoKL() {
        return metInfoCursoKL;
    }

    public void setMetInfoCursoKL(MetInfoCursoKL metInfoCursoKL) {
        this.metInfoCursoKL = metInfoCursoKL;
    }

    public MetInfoCursoKL getMetInfoCursoKLAlt() {
        return metInfoCursoKLAlt;
    }

    public void setMetInfoCursoKLAlt(MetInfoCursoKL metInfoCursoKLAlt) {
        this.metInfoCursoKLAlt = metInfoCursoKLAlt;
    }

    public boolean isDisbtnEspaniol() {
        return disbtnEspaniol;
    }

    public void setDisbtnEspaniol(boolean disbtnEspaniol) {
        this.disbtnEspaniol = disbtnEspaniol;
    }

    public boolean isDisbtnIngles() {
        return disbtnIngles;
    }

    public void setDisbtnIngles(boolean disbtnIngles) {
        this.disbtnIngles = disbtnIngles;
    }

    public boolean isDisbtnOtroidioma() {
        return disbtnOtroidioma;
    }

    public void setDisbtnOtroidioma(boolean disbtnOtroidioma) {
        this.disbtnOtroidioma = disbtnOtroidioma;
    }

    public MetSolicitud getSelectedSolicitud() {
        return selectedSolicitud;
    }

    public void setSelectedSolicitud(MetSolicitud selectedSolicitud) {
        this.selectedSolicitud = selectedSolicitud;
    }

    public List<MetSolicitud> getFiltrarSolicitud() {
        return filtrarSolicitud;
    }

    public void setFiltrarSolicitud(List<MetSolicitud> filtrarSolicitud) {
        this.filtrarSolicitud = filtrarSolicitud;
    }

    public List<MetSolicitud> getListaSolicitudes() {
        return listaSolicitudes;
    }

    public void setListaSolicitudes(List<MetSolicitud> listaSolicitudes) {
        this.listaSolicitudes = listaSolicitudes;
    }

    public MetInformacionAdicional getMetInfoAdicional() {
        return metInfoAdicional;
    }

    public void setMetInfoAdicional(MetInformacionAdicional metInfoAdicional) {
        this.metInfoAdicional = metInfoAdicional;
    }

    public MetPerfilMedico getMetPerfilMedico() {
        return metPerfilMedico;
    }

    public void setMetPerfilMedico(MetPerfilMedico metPerfilMedico) {
        this.metPerfilMedico = metPerfilMedico;
    }

    public MetContactoEmergencia getMetContactoEmergencia() {
        return metContactoEmergencia;
    }

    public void setMetContactoEmergencia(MetContactoEmergencia metContactoEmergencia) {
        this.metContactoEmergencia = metContactoEmergencia;
    }

    public MetInstitucionConvenio getMetInstitucionConvenio() {
        return metInstitucionConvenio;
    }

    public void setMetInstitucionConvenio(MetInstitucionConvenio metInstitucionConvenio) {
        this.metInstitucionConvenio = metInstitucionConvenio;
    }

    public MetSolicitud getMetSolicitud() {
        return metSolicitud;
    }

    public void setMetSolicitud(MetSolicitud metSolicitud) {
        this.metSolicitud = metSolicitud;
    }

    public MetEstadoSolicitud getMetEstadoSolicitud() {
        return metEstadoSolicitud;
    }

    public void setMetEstadoSolicitud(MetEstadoSolicitud metEstadoSolicitud) {
        this.metEstadoSolicitud = metEstadoSolicitud;
    }

    public boolean isBotonCrearSolicitud() {
        return botonCrearSolicitud;
    }

    public void setBotonCrearSolicitud(boolean botonCrearSolicitud) {
        this.botonCrearSolicitud = botonCrearSolicitud;
    }

    public boolean isTabFormEntrante() {
        return tabFormEntrante;
    }

    public void setTabFormEntrante(boolean tabFormEntrante) {
        this.tabFormEntrante = tabFormEntrante;
    }

    public MetSolicitud getUsuarioSolPK() {
        return usuarioSolPK;
    }

    public void setUsuarioSolPK(MetSolicitud usuarioSolPK) {
        this.usuarioSolPK = usuarioSolPK;
    }

    public List<String> getItemsDocAdjunto() {
        return itemsDocAdjunto;
    }

    public List<String> getItemsSemestre() {
        return itemsSemestre;
    }

    public List<String> getItemsTipoSangre() {
        return itemsTipoSangre;
    }

    public List<String> getItemsSiNo() {
        return itemsSiNo;
    }

    public boolean isSelectOtraInstitucion() {
        return selectOtraInstitucion;
    }

    public void setSelectOtraInstitucion(boolean selectOtraInstitucion) {
        this.selectOtraInstitucion = selectOtraInstitucion;
    }

    public boolean isDisableInstitucion() {
        return disableInstitucion;
    }

    public void setDisableInstitucion(boolean disableInstitucion) {
        this.disableInstitucion = disableInstitucion;
    }

    public boolean isDisableOtrInstitucion() {
        return disableOtrInstitucion;
    }

    public void setDisableOtrInstitucion(boolean disableOtrInstitucion) {
        this.disableOtrInstitucion = disableOtrInstitucion;
    }

    public boolean isVarIptOtraCiudad() {
        return varIptOtraCiudad;
    }

    public void setVarIptOtraCiudad(boolean varIptOtraCiudad) {
        this.varIptOtraCiudad = varIptOtraCiudad;
    }

    public boolean isDisabledCamposContacto() {
        return disabledCamposContacto;
    }

    public void setDisabledCamposContacto(boolean disabledCamposContacto) {
        this.disabledCamposContacto = disabledCamposContacto;
    }

    public boolean isDisableCheckOtraInst() {
        return disableCheckOtraInst;
    }

    public void setDisableCheckOtraInst(boolean disableCheckOtraInst) {
        this.disableCheckOtraInst = disableCheckOtraInst;
    }

    public boolean isDisablebtnInst() {
        return disablebtnInst;
    }

    public void setDisablebtnInst(boolean disablebtnInst) {
        this.disablebtnInst = disablebtnInst;
    }

    public String getConvenioInstitucion() {
        return convenioInstitucion;
    }

    public void setConvenioInstitucion(String convenioInstitucion) {
        this.convenioInstitucion = convenioInstitucion;
    }

    public String getExisteConvenio() {
        return existeConvenio;
    }

    public void setExisteConvenio(String existeConvenio) {
        this.existeConvenio = existeConvenio;
    }

    public boolean isDisTipMovilidad() {
        return disTipMovilidad;
    }

    public void setDisTipMovilidad(boolean disTipMovilidad) {
        this.disTipMovilidad = disTipMovilidad;
    }

    public boolean isDisFecInicioMov() {
        return disFecInicioMov;
    }

    public void setDisFecInicioMov(boolean disFecInicioMov) {
        this.disFecInicioMov = disFecInicioMov;
    }

    public boolean isDisFecFinMov() {
        return disFecFinMov;
    }

    public void setDisFecFinMov(boolean disFecFinMov) {
        this.disFecFinMov = disFecFinMov;
    }

    public boolean isDisDuracionMov() {
        return disDuracionMov;
    }

    public void setDisDuracionMov(boolean disDuracionMov) {
        this.disDuracionMov = disDuracionMov;
    }

    public boolean isDisMotivoEst() {
        return disMotivoEst;
    }

    public void setDisMotivoEst(boolean disMotivoEst) {
        this.disMotivoEst = disMotivoEst;
    }

    public boolean isDisVigenciaDesde() {
        return disVigenciaDesde;
    }

    public void setDisVigenciaDesde(boolean disVigenciaDesde) {
        this.disVigenciaDesde = disVigenciaDesde;
    }

    public boolean isDisVigenciaHasta() {
        return disVigenciaHasta;
    }

    public void setDisVigenciaHasta(boolean disVigenciaHasta) {
        this.disVigenciaHasta = disVigenciaHasta;
    }

    public boolean isDisDirAlojamiento() {
        return disDirAlojamiento;
    }

    public void setDisDirAlojamiento(boolean disDirAlojamiento) {
        this.disDirAlojamiento = disDirAlojamiento;
    }

    public boolean isDisTelAlojamiento() {
        return disTelAlojamiento;
    }

    public void setDisTelAlojamiento(boolean disTelAlojamiento) {
        this.disTelAlojamiento = disTelAlojamiento;
    }

    public MetInfUsuario getMetInfUsuario() {
        return metInfUsuario;
    }

    public void setMetInfUsuario(MetInfUsuario metInfUsuario) {
        this.metInfUsuario = metInfUsuario;
    }

    public ServiciosMovilidad getServicios() {
        return servicios;
    }

    public void setServicios(ServiciosMovilidad servicios) {
        this.servicios = servicios;
    }

    public InicioMB getInicioMB() {
        return inicioMB;
    }

    public void setInicioMB(InicioMB inicioMB) {
        this.inicioMB = inicioMB;
    }

    public List<MetParentesco> getTiposParentesco() {
        return tiposParentesco;
    }

    public void setTiposParentesco(List<MetParentesco> tiposParentesco) {
        this.tiposParentesco = tiposParentesco;
    }

    public List<MetTipoMovilidad> getTiposMovilidad() {
        return tiposMovilidad;
    }

    public void setTiposMovilidad(List<MetTipoMovilidad> tiposMovilidad) {
        this.tiposMovilidad = tiposMovilidad;
    }

    public List<MetNivelEstudioEntrante> getNivelEstudio() {
        return NivelEstudio;
    }

    public void setNivelEstudio(List<MetNivelEstudioEntrante> NivelEstudio) {
        this.NivelEstudio = NivelEstudio;
    }

    public List<MetCiudad> getListaCiudad() {
        return listaCiudad;
    }

    public void setListaCiudad(List<MetCiudad> listaCiudad) {
        this.listaCiudad = listaCiudad;
    }

    public List<MetPais> getListaPais() {
        return listaPais;
    }

    public void setListaPais(List<MetPais> listaPais) {
        this.listaPais = listaPais;
    }

    public List<MetCiudad> getListaCiudadPais() {
        return listaCiudadPais;
    }

    public void setListaCiudadPais(List<MetCiudad> listaCiudadPais) {
        this.listaCiudadPais = listaCiudadPais;
    }

    public List<MetInstitucionConvenio> getListInstitucionesConvenio() {
        return listInstitucionesConvenio;
    }

    public void setListInstitucionesConvenio(List<MetInstitucionConvenio> listInstitucionesConvenio) {
        this.listInstitucionesConvenio = listInstitucionesConvenio;
    }

    public List<MetTipoIdentificacion> getTiposIdentificacion() {
        return tiposIdentificacion;
    }

    public void setTiposIdentificacion(List<MetTipoIdentificacion> tiposIdentificacion) {
        this.tiposIdentificacion = tiposIdentificacion;
    }

    public List<MetGenero> getTiposGenero() {
        return tiposGenero;
    }

    public void setTiposGenero(List<MetGenero> tiposGenero) {
        this.tiposGenero = tiposGenero;
    }

    public ArrayList<String> getArrayAspectosMedicos() {
        return arrayAspectosMedicos;
    }

    public void setArrayAspectosMedicos(ArrayList<String> arrayAspectosMedicos) {
        this.arrayAspectosMedicos = arrayAspectosMedicos;
    }

    public MetInfoIdioma getIdiomaEspanol() {
        return idiomaEspanol;
    }

    public void setIdiomaEspanol(MetInfoIdioma idiomaEspanol) {
        this.idiomaEspanol = idiomaEspanol;
    }

    public MetInfoIdioma getIdiomaIngles() {
        return idiomaIngles;
    }

    public void setIdiomaIngles(MetInfoIdioma idiomaIngles) {
        this.idiomaIngles = idiomaIngles;
    }

    public MetInfoIdioma getIdiomaOtro() {
        return idiomaOtro;
    }

    public void setIdiomaOtro(MetInfoIdioma idiomaOtro) {
        this.idiomaOtro = idiomaOtro;
    }

    public boolean isMostrarFormLista() {
        return mostrarFormLista;
    }

    public void setMostrarFormLista(boolean mostrarFormLista) {
        this.mostrarFormLista = mostrarFormLista;
    }

    public MetSolicitud getPkSolicitud() {
        return pkSolicitud;
    }

    public void setPkSolicitud(MetSolicitud pkSolicitud) {
        this.pkSolicitud = pkSolicitud;
    }

    public MetInfUsuario getObtenerIDUsuario() {
        return obtenerIDUsuario;
    }

    public void setObtenerIDUsuario(MetInfUsuario obtenerIDUsuario) {
        this.obtenerIDUsuario = obtenerIDUsuario;
    }

    /*EDITAR INFORMACIÓN*/
    public boolean isRenderSolicitud() {
        return renderSolicitud;
    }

    public void setRenderSolicitud(boolean renderSolicitud) {
        this.renderSolicitud = renderSolicitud;
    }

    public String onActualizarPagina() {
        return "/vistas/formularioEntrante.xhtml?faces-redirect=true";
    }

    public String onCerrarSesion() {
        return "/index.xhtml?faces-redirect=true";
    }

    public void mostrarInstConvenio() {
        selectOtraInstitucion = false;
        disableOtrInstitucion = true;
        this.metInstitucionConvenio = servicios.obtenerInfInstitucionConvenio(metSolicitud.getMetInstitucionConvenio().getPk());
    }

    public void procOtraInstitucion() {
        if (selectOtraInstitucion == true) {
            metSolicitud.getMetInstitucionConvenio().setPk(null);
            disableInstitucion = true;
            disableOtrInstitucion = false;
            renderedPOtraInst = true;
            renderedInsitucionOrigen = false;
        }

        if (selectOtraInstitucion == false) {
            this.metInstitucionConvenio = servicios.obtenerInfInstitucionConvenio(metSolicitud.getMetInstitucionConvenio().getPk());
            disableInstitucion = false;
            disableOtrInstitucion = true;
            renderedPOtraInst = false;
            renderedInsitucionOrigen = true;
        }
    }

    public void seccionInfMovilidad() {

        if (metSolicitud.getMetTipoMovilidad().getPk() == null) {
            disTipMovilidad = false;
        }
        if (metSolicitud.getMetTipoMovilidad().getPk() != null) {
            disTipMovilidad = true;
        }

        if (metSolicitud.getFechaInicioMovilidad() == null) {
            disFecInicioMov = false;
        }
        if (metSolicitud.getFechaInicioMovilidad() != null) {
            disFecInicioMov = true;
        }

        if (metSolicitud.getFechaFinMovilidad() == null) {
            disFecFinMov = false;
        }
        if (metSolicitud.getFechaFinMovilidad() != null) {
            disFecFinMov = true;
        }

        if (metSolicitud.getDuracionMovilidad() == null) {
            disDuracionMov = false;
        }
        if (metSolicitud.getDuracionMovilidad() != null) {
            disDuracionMov = true;
        }

        if (metSolicitud.getMotivacionEstudiante() == null) {
            disMotivoEst = false;
        }
        if (metSolicitud.getMotivacionEstudiante() != null) {
            disMotivoEst = true;
        }
    }

    public void seccionInfAdicional() {

        if (metSolicitud.getVigenciaDesde() == null) {
            disVigenciaDesde = false;
        }
        if (metSolicitud.getVigenciaDesde() != null) {
            disVigenciaDesde = true;
        }

        if (metSolicitud.getVigenciaHasta() == null) {
            disVigenciaHasta = false;
        }
        if (metSolicitud.getVigenciaHasta() != null) {
            disVigenciaHasta = true;
        }

        if (metSolicitud.getDirAlojamiento() == null) {
            disDirAlojamiento = false;
        }
        if (metSolicitud.getDirAlojamiento() != null) {
            disDirAlojamiento = true;
        }

        if (metSolicitud.getTelAlojamiento() == null) {
            disTelAlojamiento = false;
        }
        if (metSolicitud.getTelAlojamiento() != null) {
            disTelAlojamiento = true;
        }
    }

    /*ADJUNTAR ARCHIVO*/
    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;

    }

    public void upload() throws IOException {

        System.out.println("Inicio a cargar archivo");

        if (file != null) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);

        } else {
            System.out.println("Archivo nulo");
        }
    }

    public void cargarDocumemto(FileUploadEvent event) {
        System.out.println("Cargar Documento");
        try {
            if (event.getFile() != null) {
                FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
                FacesContext.getCurrentInstance().addMessage(null, message);
                FileMongoClient.getFileMongoClient().cargarArchivo(event.getFile());
                String nombreArchivo = event.getFile().getFileName();
                String idArchivoBd = FileMongoClient.getFileMongoClient().idArchivoUpload();
                System.out.println("edu.konrad.movilidad.controller.solicitudEntranteMB.cargarDocumemto() " + idArchivoBd);

                metDistinciones.setCodigoArchivo(idArchivoBd);
                metDistinciones.setNombreArchivo(nombreArchivo);

                metSoporteAdjunto.setId(idArchivoBd);
                metSoporteAdjunto.setNombre(nombreArchivo);
                metSoporteAdjunto.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
                metSoporteAdjunto.setFechaCreado(fecha);
                metSoporteAdjunto.setFechaModifido(fecha);
                String res;
                res = servicios.guardarArchivo(metSoporteAdjunto);
                if ((res.equalsIgnoreCase(Constantes.FAILED))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar los datos del archivo."));
                    System.out.println("ERROR¡  NOO GUARDÓ ARCHIVO EN BD");
                } else {
                    System.out.println(" GUARDÓ ARCHIVO EN BD");
                }
            } else {
                System.out.println("Archivo nulo");
            }

        } catch (IOException e) {
            e.printStackTrace();
            FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Error al subir el archivo"));
        }
    }

    /*para adjuntar archivos*/
    private String codigoArc;

    public String getCodigoArc() {
        return codigoArc;
    }

    public void setCodigoArc(String codigoArc) {
        this.codigoArc = codigoArc;
    }

    public void definirCodigoArchivo() {
        System.out.println("Codigo Archivo Entrante: " + codigoArc);
        metSoporteAdjunto.setCodigo(codigoArc);

        if ("CI".equals(codigoArc)) {
            metSoporteAdjunto.setDescripcion(Constantes.DES_CARTA_INCENTIVO);
        }

        if ("CM".equals(codigoArc)) {
            metSoporteAdjunto.setDescripcion(Constantes.DES_CERTIFICADO_MOT);
        }

        if ("CN".equals(codigoArc)) {
            metSoporteAdjunto.setDescripcion(Constantes.DES_CERTIFICADO_NOTAS);
        }

        if ("FC".equals(codigoArc)) {
            metSoporteAdjunto.setDescripcion(Constantes.DES_FOTOCOPIA_CEDULA);
        }

        if ("FP".equals(codigoArc)) {
            metSoporteAdjunto.setDescripcion(Constantes.DES_FOTOCOPIA_PASAPORTE);
        }
    }

    public void cargarDocumento(FileUploadEvent event) {
        System.out.println(" ---- Cargar Documento Entrante");

        if (codigoArc == null) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Por favor seleccione el tipo de archivo"));
        }

        if (codigoArc != null) {
            try {
                if (event.getFile() != null) {
                    FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    FileMongoClient.getFileMongoClient().cargarArchivo(event.getFile());
                    String nombreArchivo = event.getFile().getFileName();
                    String idArchivoBd = FileMongoClient.getFileMongoClient().idArchivoUpload();
                    System.out.println("** CARGO DOCUMENTO **  " + idArchivoBd);

                    metSoporteAdjunto.setId(idArchivoBd);
                    metSoporteAdjunto.setNombre(nombreArchivo);
                    metSoporteAdjunto.getMetSolicitud().setSolicitudPK(pkSolicitud.getSolicitudPK());
                    metSoporteAdjunto.setFechaCreado(fecha);
                    metSoporteAdjunto.setFechaModifido(fecha);

                    System.out.println("PK SOLICITUD ARCHIVO: " + metSoporteAdjunto.getMetSolicitud().getSolicitudPK());
                    System.out.println("CODIGO ARCHIVO: " + metSoporteAdjunto.getCodigo());

                    String res;
                    res = servicios.guardarArchivo(metSoporteAdjunto);
                    if ((res.equalsIgnoreCase(Constantes.FAILED))) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar los datos del archivo."));
                        System.out.println("ERROR¡  NOO GUARDÓ ARCHIVO EN BD");
                    } else {
                        System.out.println(" GUARDÓ ARCHIVO EN BD");
                        this.listSoporteAdjunto = servicios.selectListArchivos(pkSolicitud.getSolicitudPK());

                    }
                } else {
                    System.out.println("Archivo nulo");
                }

            } catch (IOException e) {
                e.printStackTrace();
                FacesContext.getCurrentInstance().addMessage("", new FacesMessage(FacesMessage.SEVERITY_ERROR, "", "Error al subir el archivo"));
            }
        }
    }

    public void crearSolicitud() {
        this.tabFormEntrante = true;
        this.botonCrearSolicitud = false;
        String res;
        usuarioSolPK.getMetInfUsuario().setUsuarioPK(metInfUsuario.getUsuarioPK());
        usuarioSolPK.getMetTipoSolicitud().setPk(metInfUsuario.getMetTipoSolicitud().getPk());

        res = servicios.insertNewSolicitud(usuarioSolPK);
        if (res.equalsIgnoreCase(Constantes.FAILED)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al crear solicitud.", "Error"));
        } else {

            this.usuarioSolPK = servicios.consultarPKSolicitud(metInfUsuario.getUsuarioPK());
            metEstadoSolicitud.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
            metEstadoSolicitud.getMetTipoEstadoSolicitud().setPk(Constantes.ESTADOSOL_GUARDADA);
            this.metSolicitud.setConsecutivo("EKL_2018_" + "1");

            String resEstadoSolicitud;
            resEstadoSolicitud = servicios.insertEstadoSolicitud(metEstadoSolicitud);
            if (resEstadoSolicitud.equalsIgnoreCase(Constantes.FAILED)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al crear la solicitud", "Error"));
            }

            metObservacion.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
            String resObservacion;
            resObservacion = servicios.insertPKObservacion(metObservacion);
            if (resObservacion.equalsIgnoreCase(Constantes.FAILED)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al crear la solicitud", "Error"));
            }

            metContactoEmergencia.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
            String resContEmergencia;
            resContEmergencia = servicios.insertPKContactoEmergencia(metContactoEmergencia);
            if (resContEmergencia.equalsIgnoreCase(Constantes.FAILED)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error al crear la solicitud", "Error"));
            }
        }
    }

    public void enviarSolicitud() {
      /*  String res;
        this.usuarioSolPK = servicios.consultarPKSolicitud(metInfUsuario.getUsuarioPK());
        metSolicitud.setSolicitudPK(usuarioSolPK.getSolicitudPK());
        metEstadoSolicitud.getMetTipoEstadoSolicitud().setPk(Constantes.ESTADOSOL_SINREVISAR);
        metEstadoSolicitud.getMetSolicitud().setSolicitudPK(metSolicitud.getSolicitudPK());
        res = servicios.cambiarEstadoSolicitud(metEstadoSolicitud);
        if (res.equalsIgnoreCase(Constantes.FAILED)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al enviar la solicitud, intente de nuevo."));
        } else {
            String res1;
            res1 = servicios.insertFechaEnvio(metSolicitud);
            if (res1.equalsIgnoreCase(Constantes.FAILED)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al enviar la solicitud, intente de nuevo."));
            } else {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Solicitud enviada", "Solicitud enviada con éxito"));
                HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
                Map parameters = new HashMap();
                parameters.put("USUARIO_ESTUDIANTE", inicioMB.getUsuarioSesion().getNamUsuario());
                session.setAttribute("reporte", "ReporteEntrante");
                session.setAttribute("parametros", parameters);
                RequestContext.getCurrentInstance().execute("PF('pdReporteEntrante').show()");
                enviarReporteMovilidad();
                renderBtnEnviar = true;
            }
        }
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Solicitud enviada", "Solicitud enviada con éxito"));*/
    }

    public void guardarSolicitud() {
        this.usuarioSolPK = servicios.consultarPKSolicitud(metInfUsuario.getUsuarioPK());
        metSolicitud.setSolicitudPK(usuarioSolPK.getSolicitudPK());
        guardarTipoMovilidad();
        guardardDatosSolicitud();
        renderedPersonal = false;
        renderedInstOrigen = false;
        renderedAcademica = false;
        renderedMovilidad = false;
        renderedPrfilMedico = true;
        renderedDocAdjuntos = false;
    }

    public void guadarInfAcademica2() {

        String resMetNivelEstudiopk;
        if (metSolicitud.getMetNivelEstEntrante().getPk() != null) {
            resMetNivelEstudiopk = servicios.updatePKMetNivelEstudio(metSolicitud);
            if ((resMetNivelEstudiopk.equalsIgnoreCase(Constantes.FAILED))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "DEBE SELECCINAR IN NIVEL DE ESTUDIO"));
            } else {
                System.out.println("edu.konrad.movilidad.controller.solicitudEntranteMB.guadarInfAcademica2() --- " + metSolicitud.getMetNivelEstEntrante().getPk());
                System.out.println("edu.konrad.movilidad.controller.solicitudEntranteMB.guadarInfAcademica2() solicitud --- " + metSolicitud.getSolicitudPK());

            }
        }

        guardarInfoIdioma();
        guardardDatosSolicitud();
        renderedPersonal = false;
        renderedInstOrigen = false;
        renderedAcademica = false;
        renderedMovilidad = true;
        renderedPrfilMedico = false;
        renderedDocAdjuntos = false;
    }

    public void guardardDatosSolicitud() {
        String resMetSolicitud;
        resMetSolicitud = servicios.updateMetSolicitud(metSolicitud);
        if ((resMetSolicitud.equalsIgnoreCase(Constantes.FAILED))) {
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Datos Guardados", "DATOS GUARDADOS CON ÉXITO"));
        }
    }

    public void guardarInfoIdioma() {

        String metInfIdiomaEsp;
        String metInfIdiomaIng;
        String metInfIdiomaOtr;

        idiomaEspanol.setCodigo(Constantes.COD_IDIOMA_ESPANIOL);
        idiomaEspanol.setNamDescripcion(Constantes.DES_IDIOMA_ESPANIOL);
        idiomaEspanol.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
        metInfIdiomaEsp = servicios.validarInfoIdioma(idiomaEspanol);
        if (metInfIdiomaEsp.equalsIgnoreCase(Constantes.ZERO)) {
            String resInfIdioma;
            resInfIdioma = servicios.insertMetInfoIdiomas(idiomaEspanol);
            if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error IDIOMA ESPAÑOL"));
            }
        } else {
            String resInfIdioma;
            resInfIdioma = servicios.updateInfoIdioma(idiomaEspanol);
            if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error IDIOMA ESPAÑOL"));
            }
        }

        idiomaIngles.setCodigo(Constantes.COD_IDIOMA_INGLES);
        idiomaIngles.setNamDescripcion(Constantes.DES_IDIOMA_INGLES);
        idiomaIngles.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
        metInfIdiomaIng = servicios.validarInfoIdioma(idiomaIngles);
        if (metInfIdiomaIng.equalsIgnoreCase(Constantes.ZERO)) {
            String resInfIdioma;
            resInfIdioma = servicios.insertMetInfoIdiomas(idiomaIngles);
            if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error IDIOMA INGLES"));
            }
        } else {
            String resInfIdioma;
            resInfIdioma = servicios.updateInfoIdioma(idiomaIngles);
            if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error IDIOMA INGLES"));
            }
        }

        idiomaOtro.setCodigo(Constantes.COD_IDIOMA_OTRO);
        idiomaOtro.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
        metInfIdiomaOtr = servicios.validarInfoIdioma(idiomaOtro);
        if (metInfIdiomaOtr.equalsIgnoreCase(Constantes.ZERO)) {
            String resInfIdioma;
            resInfIdioma = servicios.insertMetInfoIdiomas(idiomaOtro);
            if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error IDIOMA OTRO"));
            }
        } else {
            String resInfIdioma;
            resInfIdioma = servicios.updateInfoIdioma(idiomaOtro);
            if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error IDIOMA OTRO"));
            }
        }
    }

    public void guardarTipoMovilidad() {
        String resTipoMovilidad;
        resTipoMovilidad = servicios.updatePKMetTipoMovilidad(metSolicitud);
        if ((resTipoMovilidad.equalsIgnoreCase(Constantes.FAILED))) {
            //    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error SOLICITUD"));
        } else {
        }
    }

    public void guardarContactoEmergencia() {
        this.usuarioSolPK = servicios.consultarPKSolicitud(metInfUsuario.getUsuarioPK());
        metSolicitud.setSolicitudPK(usuarioSolPK.getSolicitudPK());
        String resContactoEmergencia;
        metContactoEmergencia.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
        resContactoEmergencia = servicios.updateContactoEmergencia(metContactoEmergencia);
        System.out.println("CONTACTO EMERGENCIA " + metContactoEmergencia.getOtroParentesco());
        if ((resContactoEmergencia.equalsIgnoreCase(Constantes.FAILED))) {
            //  FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error SOLICITUD"));
        } else {
            String guardarRH;
            guardarRH = servicios.updateRHusuario(metInfUsuario);
            if ((guardarRH.equalsIgnoreCase(Constantes.FAILED))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "ERROR AL GUARDAR DATOS DE CONTACTO"));
                System.out.println("Error al guardar RH");
            } else {
                //  FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Datos Guardados", "DATOS GUARDADOS CON ÉXITO"));
                System.out.println("Guardó RH");
                String resMetSolicitud;
                resMetSolicitud = servicios.updateMetSolicitud(metSolicitud);
                if ((resMetSolicitud.equalsIgnoreCase(Constantes.FAILED))) {
                    //   FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error SOLICITUD"));
                    System.out.println("ERROR no guardó datos medicos");
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Datos Guardados", "DATOS GUARDADOS CON ÉXITO"));
                    System.out.println("Guardó datos medicos");
                    renderedPersonal = false;
                    renderedInstOrigen = false;
                    renderedAcademica = false;
                    renderedMovilidad = false;
                    renderedPrfilMedico = false;
                    renderedDocAdjuntos = true;
                }
                //   FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Datos Guardados", "DATOS GUARDADOS CON ÉXITO 3"));
            }
        }
    }

    public String irSolicitud() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("seleccionEstudiante", selectedSolicitud);
        return "/vistas/consultaSolicitudEntrante.xhtml?faces-redirect=true";
    }

    public void deshabilitarIdiomaEsp() {
        if ("false".equals(idiomaEspanol.getIdiomaNativo())) {
            disabledIdiomaEsp = false;
            idiomaNativoIng = false;
            idiomaNativoOtr = false;
            idiomaEspanol.setPorcentajeEscribe(null);
            idiomaEspanol.setPorcentajeHabla(null);
            idiomaEspanol.setPorcentajeLee(null);
        }
        if ("true".equals(idiomaEspanol.getIdiomaNativo())) {
            disabledIdiomaEsp = true;
            idiomaNativoIng = true;
            idiomaNativoOtr = true;
            idiomaEspanol.setPorcentajeEscribe("100%");
            idiomaEspanol.setPorcentajeHabla("100%");
            idiomaEspanol.setPorcentajeLee("100%");
            idiomaIngles.setIdiomaNativo("N");
            idiomaOtro.setIdiomaNativo("N");
        }
    }

    public void deshabilitarIdiomaIng() {
        if ("false".equals(idiomaIngles.getIdiomaNativo())) {
            disabledIdiomaIng = false;
            idiomaNativoEsp = false;
            idiomaNativoOtr = false;
            idiomaIngles.setPorcentajeEscribe(null);
            idiomaIngles.setPorcentajeHabla(null);
            idiomaIngles.setPorcentajeLee(null);
        }
        if ("true".equals(idiomaIngles.getIdiomaNativo())) {
            disabledIdiomaIng = true;
            idiomaNativoEsp = true;
            idiomaNativoOtr = true;
            idiomaIngles.setPorcentajeEscribe("100%");
            idiomaIngles.setPorcentajeHabla("100%");
            idiomaIngles.setPorcentajeLee("100%");
            idiomaEspanol.setIdiomaNativo("N");
            idiomaOtro.setIdiomaNativo("N");
        }
    }

    public void deshabilitarIdiomaOtro() {
        if ("false".equals(idiomaOtro.getIdiomaNativo())) {
            disabledIdiomaOtr = false;
            idiomaNativoIng = false;
            idiomaNativoEsp = false;
            idiomaOtro.setPorcentajeEscribe(null);
            idiomaOtro.setPorcentajeHabla(null);
            idiomaOtro.setPorcentajeLee(null);
        }
        if ("true".equals(idiomaOtro.getIdiomaNativo())) {
            disabledIdiomaOtr = true;
            idiomaNativoIng = true;
            idiomaNativoEsp = true;
            idiomaOtro.setPorcentajeEscribe("100%");
            idiomaOtro.setPorcentajeHabla("100%");
            idiomaOtro.setPorcentajeLee("100%");
            idiomaIngles.setIdiomaNativo("N");
            idiomaEspanol.setIdiomaNativo("N");
        }
    }

    public void guardarDistincion() {
        metDistinciones.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
        System.out.println("DISTINCIONES" + metDistinciones.getMetSolicitud().getSolicitudPK());
        System.out.println("NOMBRE ARCHIVO: " + metDistinciones.getNombreArchivo());
        System.out.println("CODIGO ARCHIVO: " + metDistinciones.getCodigoArchivo());

        String resMetDistinciones;
        resMetDistinciones = servicios.insertDistinciones(metDistinciones);
        if ((resMetDistinciones.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar campo distinción"));
        } else {
            this.metDistinciones.setAnio(null);
            this.metDistinciones.setDescripcion(null);
            this.metDistinciones.setCodigoArchivo(null);
            this.metDistinciones.setNombreArchivo(null);
            renderedDistinciones = true;
            this.listaDistinciones = servicios.obtenerDistincion(pkSolicitud.getSolicitudPK());
        }
    }

    public void borrarDistincion() {
        String resMetDistincion;
        resMetDistincion = servicios.deleteDistincion(selectedDistincion.getPk());
        if ((resMetDistincion.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al eliminar campo distinción"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Dato eliminado", "DATO ELIMINADO CON ÉXITO"));
            renderedDistinciones = true;
            this.listaDistinciones = servicios.obtenerDistincion(pkSolicitud.getSolicitudPK());
        }
    }

    public void contDistincion() {
        String contDistincion;
        contDistincion = servicios.validaDistincion(pkSolicitud.getSolicitudPK());
        if (!contDistincion.equalsIgnoreCase(Constantes.ZERO)) {
            renderedDistinciones = true;
        } else {
            renderedDistinciones = false;
        }
    }

    public void otroParentesco() {
        System.out.println("edu.konrad.movilidad.controller.solicitudEntranteMB.otroParentesco() 5648974 ");
        if ("11".equals(metContactoEmergencia.getMetParentesco().getPk())) {
            renderedOtroParentesco = true;
        } else {
            renderedOtroParentesco = false;
        }
    }

    public void calcularDuracionMovilidad() {

        if ((metSolicitud.getFechaInicioMovilidad() != null) && (metSolicitud.getFechaFinMovilidad() != null)) {
            Date fechaInicial = metSolicitud.getFechaInicioMovilidad();
            Date fechaFinal = metSolicitud.getFechaFinMovilidad();
            diasMovilidad = (int) ((fechaFinal.getTime() - fechaInicial.getTime()) / 86400000);
            mDuracionMovilidad = (int) (diasMovilidad / 30.44);
            dDuracionMovilidad = (int) (diasMovilidad - mDuracionMovilidad * 30);
            metSolicitud.setDuracionMovilidad(mDuracionMovilidad + " meses, " + dDuracionMovilidad + " días");
            renderedDuracionMovilidad = true;
        }
    }

    public void guardarInfCursoKL() {
        metInfoCursoKL.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
        metInfoCursoKL.setEstado(Constantes.NOALTERNATIVO);
        String resMetCurssosKL;
        resMetCurssosKL = servicios.insertInfoCursoKL(metInfoCursoKL);
        if ((resMetCurssosKL.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar curso"));
        } else {
            this.metInfoCursoKL.getMetCursoKL().setPk(null);
            renderedCursosKL = true;
            this.listaInfoCursoKL = servicios.obtenerInfoCursoKL(pkSolicitud.getSolicitudPK());
        }
    }

    public void guardarInfCursoKLAlt() {
        metInfoCursoKLAlt.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
        metInfoCursoKLAlt.setEstado(Constantes.ALTERNATIVO);
        String resMetCurssosKL;
        resMetCurssosKL = servicios.insertInfoCursoKLAlt(metInfoCursoKLAlt);
        if ((resMetCurssosKL.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar curso alternativo"));
        } else {
            this.metInfoCursoKLAlt.getMetCursoKL().setPk(null);
            this.listaInfoCursoKLAlt = servicios.obtenerInfoCursoKLAlt(pkSolicitud.getSolicitudPK());
        }
    }

    public void borrarInfCursoKL() {
        String resMetInfoCurso;
        resMetInfoCurso = servicios.deleteInfoCurso(selectedInfoCursoKL.getPk());
        if ((resMetInfoCurso.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al eliminar curso"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Dato eliminado", "DATO ELIMINADO CON ÉXITO"));
            renderedCursosKL = true;
            renderedCursosKLAlt = true;
            this.listaInfoCursoKL = servicios.obtenerInfoCursoKL(pkSolicitud.getSolicitudPK());
        }
    }

    //---------------------------------------------------------------------------------------------------------//
    public void definirEstadoSolicitud() {

        metEstadoSolicitud = servicios.obtenerIDEstadoSolicitud(pkSolicitud.getSolicitudPK());
        if ("3".equals(metEstadoSolicitud.getMetTipoEstadoSolicitud().getPk())) {
            renderPTablaSolicitud = false;
            renderSolicitud = true;
            System.out.println("OCULTA TABLA VISTA");
        } else {
            renderPTablaSolicitud = true;
            renderSolicitud = false;
            System.out.println("OCULTA SOLICITUD");
        }
    }

    public void otroNivelEducacion() {
        if (selecOtroNivelEdu == true) {
            disNivelEducacion = true;
            disOtroNivelEducacion = false;
            metSolicitud.getMetNivelEstEntrante().setPk(null);
        }

        if (selecOtroNivelEdu == false) {
            disNivelEducacion = false;
            disOtroNivelEducacion = true;
            metSolicitud.setOtroNivelEstudio(null);
        }

    }

    /*  -------------------------   CORREO ----------------------- */
    //correo
    private String from = "";//correo
    private String password = "";//password
    // destinatario1@hotmail.com,destinatario2@hotmail.com, destinatario_n@hotmail.com
    private InternetAddress[] addressTo;
    private String Subject = "";//titulo del mensaje
    private String MessageMail = "";//contenido del mensaje

    //remitente
    public void setFrom(String mail) {
        this.from = mail;
    }

    public String getFrom() {
        return this.from;
    }

    //Contraseña
    public void setPassword(char[] value) {
        this.password = new String(value);
    }

    public String getPassword() {
        return this.password;
    }

    //destinatario
    public void setTo(String mails) {
        String[] tmp = mails.split(",");
        addressTo = new InternetAddress[tmp.length];
        for (int i = 0; i < tmp.length; i++) {
            try {
                addressTo[i] = new InternetAddress(tmp[i]);
            } catch (AddressException ex) {
                System.out.println(ex);
            }
        }
    }

    public InternetAddress[] getTo() {
        return this.addressTo;
    }

    //titulo correo
    public void setSubject(String value) {
        this.Subject = value;
    }

    public String getSubject() {
        return this.Subject;
    }

    //contenido del mensaje
    public void setMessage(String value) {
        this.MessageMail = value;
    }

    public String getMessage() {
        return this.MessageMail;
    }

    public void sendEmailRegistroEstudiante() {

        from = "leidy.sarmiento20@gmail.com";
        password = "esternocleidoMASTOIDEO861015Donghae";
        setTo(metInfUsuario.getEmailInstitucional());
        Subject = "Confirmación de Solicitud Enviada del SISTEMA MOVILIDAD ESTUDIANTIL KONRAD LORENZ";
        setMessage("Estimado(a)" + metInfUsuario.getNombres() + " " + metInfUsuario.getApellidos() + "<br/>"
                + "\n"
                + "Su solicitud ha sio enviada. Recuerde notificar a su universidad de destino, este proceso debe realizarse a fin de obtener un aval de susolicitud y comenzar con el correspondiente proceso\n"
                + "<br/>"
                + "<br/>"
                + "Recuerde que toda comunicación remitida de manera electrónica desde cancillería se hará al correo " + metInfUsuario.getEmailInstitucional()
                + ", por lo anterior, es de vital importancia revisarlo periódicamente.\n"
                + "<br/>"
                + "<br/>"
                + "Cordial Saludo,\n"
                + "<br/>"
                + "Movilidad Estudiantil Fundación Universitaria Konrad Lorenz\n"
                + "<br/>"
                + "<br/>"
                + "**********************NO RESPONDER - Mensaje Generado Automaticamente**********************\n"
                + "<br/>"
                + "Este correo es unicamente informativo y es de uso exclusivo del destinatario(a), puede contener informacion privilegiada y/o confidencial. Si no es usted el destinatario(a) deberá borrarlo inmediatamente. Queda notificado que el mal uso, divulgación no autorizada, alteración y/o  modificación malintencionada sobre este mensaje y sus anexos quedan estrictamente prohibidos y pueden ser legalmente sancionados.");

        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.user", "usuario");
            props.put("mail.smtp.port", 25);
            //
            SMTPAuthenticator auth = new SMTPAuthenticator(getFrom(), getPassword());
            Session session = Session.getDefaultInstance(props, auth);
            session.setDebug(false);
            //Se crea destino y origen del mensaje
            MimeMessage mimemessage = new MimeMessage(session);
            InternetAddress addressFrom = new InternetAddress(getFrom());
            mimemessage.setFrom(addressFrom);
            mimemessage.setRecipients(Message.RecipientType.TO, addressTo);
            mimemessage.setSubject(getSubject());
            // Se crea el contenido del mensaje
            MimeBodyPart mimebodypart = new MimeBodyPart();
            mimebodypart.setText(getMessage());
            mimebodypart.setContent(getMessage(), "text/html");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimebodypart);
            mimemessage.setContent(multipart);
            mimemessage.setSentDate(new Date());
            Transport.send(mimemessage);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Recibirá una notificación a su correo electrónico"));
        } catch (MessagingException ex) {
            System.out.println(ex);
        }
    }

    public void sendEmailMovilidad() {
        from = "leidy.sarmiento20@gmail.com";
        password = "esternocleidoMASTOIDEO861015Donghae";
        setTo("movilidad@konradlorenz.edu.co");
        Subject = "Confirmación de Solicitud Enviada del SISTEMA MOVILIDAD ESTUDIANTIL KONRAD LORENZ";
        setMessage("Estimado(a) Catherine Torres"
                + "La solicitud del estudiante " + metInfUsuario.getNombres() + " " + metInfUsuario.getApellidos()
                + " a sido enviada. \n"
                + "<br/>"
                + "<br/>"
                + "Cordial Saludo,\n"
                + "<br/>"
                + "Movilidad Estudiantil Fundación Universitaria Konrad Lorenz\n"
                + "<br/>"
                + "<br/>"
                + "**********************NO RESPONDER - Mensaje Generado Automaticamente**********************\n"
                + "<br/>"
                + "Este correo es unicamente informativo y es de uso exclusivo del destinatario(a), puede contener informacion privilegiada y/o confidencial. Si no es usted el destinatario(a) deberá borrarlo inmediatamente. Queda notificado que el mal uso, divulgación no autorizada, alteración y/o  modificación malintencionada sobre este mensaje y sus anexos quedan estrictamente prohibidos y pueden ser legalmente sancionados.");
        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.user", "usuario");
            props.put("mail.smtp.port", 25);
            //
            SMTPAuthenticator auth = new SMTPAuthenticator(getFrom(), getPassword());
            Session session = Session.getDefaultInstance(props, auth);
            session.setDebug(false);
            //Se crea destino y origen del mensaje
            MimeMessage mimemessage = new MimeMessage(session);
            InternetAddress addressFrom = new InternetAddress(getFrom());
            mimemessage.setFrom(addressFrom);
            mimemessage.setRecipients(Message.RecipientType.TO, addressTo);
            mimemessage.setSubject(getSubject());
            // Se crea el contenido del mensaje
            MimeBodyPart mimebodypart = new MimeBodyPart();
            mimebodypart.setText(getMessage());
            mimebodypart.setContent(getMessage(), "text/html");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimebodypart);
            mimemessage.setContent(multipart);
            mimemessage.setSentDate(new Date());
            Transport.send(mimemessage);
        } catch (MessagingException ex) {
            System.out.println(ex);
        }
    }

    public void sendEmailInstitucionOrigen() {
        from = "leidy.sarmiento20@gmail.com";
        password = "esternocleidoMASTOIDEO861015Donghae";
        setTo(metOtraInstitucion.getEmailContacto());
        Subject = "Solicitud SISTEMA MOVILIDAD ESTUDIANTIL KONRAD LORENZ";
        setMessage("Estimado(a) " + metOtraInstitucion.getNombreContacto()
                + " La solicitud del estudiante " + metInfUsuario.getNombres() + " " + metInfUsuario.getApellidos()
                + " a sido enviada. Por favor revisar dicha solicitud y enviar pronta respuesta para la aprobación de la misma.\n"
                + "<br/>"
                + "<br/>"
                + "Cordial Saludo,\n"
                + "<br/>"
                + "Movilidad Estudiantil Fundación Universitaria Konrad Lorenz\n"
                + "<br/>"
                + "<br/>"
                + "**********************NO RESPONDER - Mensaje Generado Automaticamente**********************\n"
                + "<br/>"
                + "Este correo es unicamente informativo y es de uso exclusivo del destinatario(a), puede contener informacion privilegiada y/o confidencial. Si no es usted el destinatario(a) deberá borrarlo inmediatamente. Queda notificado que el mal uso, divulgación no autorizada, alteración y/o  modificación malintencionada sobre este mensaje y sus anexos quedan estrictamente prohibidos y pueden ser legalmente sancionados.");
        try {
            Properties props = new Properties();
            props.put("mail.smtp.host", "smtp.gmail.com");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.user", "usuario");
            props.put("mail.smtp.port", 25);
            //
            SMTPAuthenticator auth = new SMTPAuthenticator(getFrom(), getPassword());
            Session session = Session.getDefaultInstance(props, auth);
            session.setDebug(false);
            //Se crea destino y origen del mensaje
            MimeMessage mimemessage = new MimeMessage(session);
            InternetAddress addressFrom = new InternetAddress(getFrom());
            mimemessage.setFrom(addressFrom);
            mimemessage.setRecipients(Message.RecipientType.TO, addressTo);
            mimemessage.setSubject(getSubject());
            // Se crea el contenido del mensaje
            MimeBodyPart mimebodypart = new MimeBodyPart();
            mimebodypart.setText(getMessage());
            mimebodypart.setContent(getMessage(), "text/html");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(mimebodypart);
            mimemessage.setContent(multipart);
            mimemessage.setSentDate(new Date());
            Transport.send(mimemessage);
        } catch (MessagingException ex) {
            System.out.println(ex);
        }
    }

    /*-----------------------------------------SQL------------------------------------------------------------*/
 /*GUARDAR INFO PERSONAL*/
    public void guardarDatosPersonales() {
        String infoPersonal;
        infoPersonal = servicios.guardarInfPersonal(metInfUsuario);
        if ((infoPersonal.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar datos personales"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Datos Guardados", "DATOS GUARDADOS CON ÉXITO"));
            renderedPersonal = false;
            renderedInstOrigen = true;
            renderedAcademica = false;
            renderedMovilidad = false;
            renderedPrfilMedico = false;
            renderedDocAdjuntos = false;
        }
    }

    public void guardarInstitucion() {
        if (selectOtraInstitucion == true) {
            System.out.println("--******** INSTITUCION EXTERNA");
            String existeOtraInstitucionG;

            existeOtraInstitucionG = servicios.existeOtraInstitucion(pkSolicitud.getSolicitudPK());

            if (!existeOtraInstitucionG.equalsIgnoreCase(Constantes.ZERO)) {
                String actualizarOtraInstitucion;
                actualizarOtraInstitucion = servicios.actualizarOtraInstitucion(metOtraInstitucion);
                if ((actualizarOtraInstitucion.equalsIgnoreCase(Constantes.FAILED))) {
                    System.out.println("Error al actualizar datos de la otra institucion");
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Datos Guardados", "DATOS GUARDADOS CON ÉXITO"));
                    System.out.println("Actualizó datos de la otra institucion");;
                    renderedPersonal = false;
                    renderedInstOrigen = false;
                    renderedAcademica = true;
                    renderedMovilidad = false;
                    renderedPrfilMedico = false;
                    renderedDocAdjuntos = false;
                }
            } else {
                guardarOtraInstitucionExt();
            }
        }
        if (selectOtraInstitucion == false) {
            System.out.println("**------- INSTITUCION ORIGEN PK");
            guardarInstitucionOrigen();
        }
    }

    public void guardarOtraInstitucionExt() {
        System.out.println("pk solicitud " + pkSolicitud.getSolicitudPK());
        metOtraInstitucion.getMetSolicitud().setSolicitudPK(pkSolicitud.getSolicitudPK());
        String infOtraInstitucion;
        infOtraInstitucion = servicios.guardarOtraInstitucion(metOtraInstitucion);
        if ((infOtraInstitucion.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar información de la Institución"));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Datos Guardados", "DATOS GUARDADOS CON ÉXITO"));
            String eliminarPkInstitucion;
            eliminarPkInstitucion = servicios.eliminarPKInstitucion(pkSolicitud.getSolicitudPK());
            if ((eliminarPkInstitucion.equalsIgnoreCase(Constantes.FAILED))) {
                System.out.println("Error al eliminar pk de la Institución en la solicitud");
            } else {
                System.out.println("eliminó pk de la Institución en la solicitud");
            }
            renderedPersonal = false;
            renderedPrfilMedico = false;
            renderedInstOrigen = false;
            renderedAcademica = true;
            renderedMovilidad = false;
        }
    }

    public void guardarInstitucionOrigen() {
        String resMetInstitucionOrigen;
        resMetInstitucionOrigen = servicios.updatePKMetInstConvenio(metSolicitud);
        if ((resMetInstitucionOrigen.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al guardar los datos Institucion Origen, intente de nuevo."));
            System.out.println("ERROR¡ edu.konrad.movilidad.controller.solicitudEntranteMB.guardarInstitucionOrigen()");
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "INFORMACIÓN GUARDADA CON ÉXITO."));
            System.out.println("BIEN¡ edu.konrad.movilidad.controller.solicitudEntranteMB.guardarInstitucionOrigen()");
            String borrarOtraInst;
            borrarOtraInst = servicios.borrarOtraInstitucion(pkSolicitud.getSolicitudPK());
            if ((borrarOtraInst.equalsIgnoreCase(Constantes.FAILED))) {
                System.out.println("Error al eliminar otra Institucion");
            } else {
                System.out.println("eliminó Institución");
            }
            renderedPersonal = false;
            renderedPrfilMedico = false;
            renderedInstOrigen = false;
            renderedAcademica = true;
            renderedMovilidad = false;
        }
    }

    /*---------------------------------GETTER SETTER ----------------------------------------------*/
    public MetOtraInstitucionExt getMetOtraInstitucion() {
        return metOtraInstitucion;
    }

    public void setMetOtraInstitucion(MetOtraInstitucionExt metOtraInstitucion) {
        this.metOtraInstitucion = metOtraInstitucion;
    }

    public boolean isDisOtroNivelEducacion() {
        return disOtroNivelEducacion;
    }

    public void setDisOtroNivelEducacion(boolean disOtroNivelEducacion) {
        this.disOtroNivelEducacion = disOtroNivelEducacion;
    }

    public boolean isDisNivelEducacion() {
        return disNivelEducacion;
    }

    public void setDisNivelEducacion(boolean disNivelEducacion) {
        this.disNivelEducacion = disNivelEducacion;
    }

    public boolean isSelecOtroNivelEdu() {
        return selecOtroNivelEdu;
    }

    public void setSelecOtroNivelEdu(boolean selecOtroNivelEdu) {
        this.selecOtroNivelEdu = selecOtroNivelEdu;
    }

    public void cargarArchivo() {
        System.out.println("Entro a cargar archivo");
        //FileMongoClient.getFileMongoClient().cargarArchivo();
    }

  /*  public void leerArchivo() {
        FileMongoClient.getFileMongoClient().leerArchivo();
    }*/

    private StreamedContent dfile;

    public StreamedContent getDfile() {
        return this.dfile;
    }

    public void setDfile(StreamedContent dFile) {
        this.dfile = dFile;
    }

    public boolean isRenderBtnEnviar() {
        return renderBtnEnviar;
    }

    public void setRenderBtnEnviar(boolean renderBtnEnviar) {
        this.renderBtnEnviar = renderBtnEnviar;
    }

    public void downloadAction() {
        System.out.println("Inio Download");
        try {
            File file = new File("C:\\temp\\Carta de compromiso-1.pdf");
            InputStream input = new FileInputStream(file);
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            setDfile(new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName()));
            System.out.println("PREP = " + this.dfile.getName());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public StreamedContent prepDownload() throws Exception {
        System.out.println("Entor al proceso");
        StreamedContent download = new DefaultStreamedContent();
        File file = new File("C:\\temp\\Carta de compromiso-1.pdf");
        InputStream input = new FileInputStream(file);
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        download = new DefaultStreamedContent(input, externalContext.getMimeType(file.getName()), file.getName());
        System.out.println("PREP = " + download.getName());
        return download;
    }

    public StreamedContent getArchivo() throws IOException {
       StreamedContent miFile;
        System.out.println("CODIGO ARCHIVO PARA DESCARGAR" + selectArchivo.getId());
        File initialFile = FileMongoClient.getFileMongoClient().getArchivo(selectArchivo.getId());
        InputStream targetStream = new FileInputStream(initialFile);
        miFile = new DefaultStreamedContent(targetStream, "application/pdf", selectArchivo.getNombre());
        return miFile;
    }

    public void definirIdioma() {
        System.out.println("IDIOMA EN SOLICITUD 2" + inicioMB.varIdioma);
        if ("en".equals(inicioMB.varIdioma)) {
            campoIngles = true;
            campoEspaniol = false;
        }

        if ("es".equals(inicioMB.varIdioma)) {
            campoIngles = false;
            campoEspaniol = true;
        }
    }

    public void logout() {
        log.info("Logout de la sesion: " + ((inicioMB.getUsuarioSesion().getNamUsuario() == null) ? inicioMB.getUsuarioSesion().getNamUsuario() : ""));
        ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
        String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
        try {
            ((HttpSession) ctx.getSession(false)).invalidate();
            ctx.redirect(ctxPath + "/faces/index.xhtml?faces-redirect=true");
        } catch (IOException ex) {
            System.out.println("Error" + ex.getMessage());
        }
    }

    public void btnSiguiente(int siguiente) {
        System.out.println("************* SIGUIENTE " + siguiente);
        switch (siguiente) {
            case 1:
                renderedPersonal = false;
                renderedInstOrigen = true;
                renderedAcademica = false;
                renderedMovilidad = false;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                break;

            case 2:
                renderedPersonal = false;
                renderedInstOrigen = false;
                renderedAcademica = true;
                renderedMovilidad = false;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                break;

            case 3:
                renderedPersonal = false;
                renderedInstOrigen = false;
                renderedAcademica = false;
                renderedMovilidad = true;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                break;

            case 4:
                renderedPersonal = false;
                renderedInstOrigen = false;
                renderedAcademica = false;
                renderedMovilidad = false;
                renderedPrfilMedico = true;
                renderedDocAdjuntos = false;
                break;

            case 5:
                renderedPersonal = false;
                renderedInstOrigen = false;
                renderedAcademica = false;
                renderedMovilidad = false;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = true;
                break;
        }
    }

    public void btnAtras(int atras) {
        switch (atras) {
            case 2:
                renderedPersonal = true;
                renderedInstOrigen = false;
                renderedAcademica = false;
                renderedMovilidad = false;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                break;

            case 3:
                renderedPersonal = false;
                renderedInstOrigen = true;
                renderedAcademica = false;
                renderedMovilidad = false;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                break;

            case 4:
                renderedPersonal = false;
                renderedInstOrigen = false;
                renderedAcademica = true;
                renderedMovilidad = false;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                break;

            case 5:
                renderedPersonal = false;
                renderedInstOrigen = false;
                renderedAcademica = false;
                renderedMovilidad = true;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                break;

            case 6:
                renderedPersonal = false;
                renderedInstOrigen = false;
                renderedAcademica = false;
                renderedMovilidad = false;
                renderedPrfilMedico = true;
                renderedDocAdjuntos = false;
                break;
        }

    }
    
    
     public void enviarCorreoAprobacion() {
        SendEmail enviarCorreo = new SendEmail();
        if (this.getAdjuntos() == null) {
            this.setAdjuntos(new ArrayList<String>());
        } else {
            if (this.getAdjuntos().size() > 0) {
                this.getAdjuntos().clear();
            }
        }

        if (this.getReceptores() == null) {
            this.setReceptores(new ArrayList<String>());
        } else {
            if (this.getReceptores().size() > 0) {
                this.getReceptores().clear();
            }
        }

        if (this.getReceptoresCopia() == null) {
            this.setReceptoresCopia(new ArrayList<String>());
        } else {
            if (this.getReceptoresCopia().size() > 0) {
                this.getReceptoresCopia().clear();
            }
        }

        try {
            this.servidorCorreo = "smtp.office365.com";
            this.puertoCorreo = "587";
            this.emisor = "movilidad@konradlorenz.edu.co";
            this.contrasena = "Konrad2018";
            this.asunto = "Aprobacion de Solicitud - Plataforma de Movilidad Fundacion Universitaria Konrad Lorenz";
            System.out.println("CORREO SALIENTE institucional: " + metInfUsuario.getEmailInstitucional());
            System.out.println("CORREO SALIENTE alternativo " + metInfUsuario.getEmailAlternativo());
            if (metInfUsuario.getEmailInstitucional() != null) {
                this.getReceptores().add(metInfUsuario.getEmailInstitucional());
            }
            if (metInfUsuario.getEmailAlternativo() != null) {
                this.getReceptores().add(metInfUsuario.getEmailAlternativo());
            }

            this.mensaje = ("<p style=\"text-align: justify;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: 'Calibri Light',sans-serif;\">Estimado(a) <span>" + metInfUsuario.getNombres() + " " + metInfUsuario.getApellidos() + "</span></span></p>\n"
                    + "<p>&iexcl;Felicitaciones! Nos complace informarte que por tus calidades personales y acad&eacute;micas se ha aprobado tu proceso de movilidad. En los pr&oacute;ximos d&iacute;as recibir&aacute;s por v&iacute;a electr&oacute;nica la carta de aceptaci&oacute;n oficial.</p>\n"
                    + "<p>Recuerda que si tienes alguna duda escr&iacute;benos a: <a href=\"mailto:cancilleria@konradlorenz.edu.co\">cancilleria@konradlorenz.edu.co</a></p>");
            this.tls = "true";
            this.authentication = "true";
            enviarCorreo.envia(servidorCorreo, puertoCorreo, emisor, asunto, receptores, receptoresCopia, mensaje, adjuntos, contrasena, tls, authentication);
        } catch (Exception e) {
            e.printStackTrace();
            log.info(e.getMessage());
        }
    }
    

    public void validaInstitucionOrigen() {
        existeOtraInsti = servicios.validaOtraInstitucion(pkSolicitud.getSolicitudPK());

        if (("0".equals(existeOtraInsti))
                && (metSolicitud.getMetInstitucionConvenio().getPk() == null)) {
            System.out.println("** MUESTRA INSTITUCION ORIGEN PK");
            this.selectOtraInstitucion = false;
            this.renderedPOtraInst = false;
            this.disableInstitucion = false;
            this.renderedInsitucionOrigen = true;
        }

        if (!"0".equals(existeOtraInsti)) {
            System.out.println("-- MUESTRA INSTITUCION EXTERNA");
            this.selectOtraInstitucion = true;
            this.renderedPOtraInst = true;
            this.renderedInsitucionOrigen = false;
            this.disableInstitucion = true;
        }

        if ((metSolicitud.getMetInstitucionConvenio().getPk() != null)) {
            System.out.println("** MUESTRA INSTITUCION ORIGEN PK");
            this.selectOtraInstitucion = false;
            this.renderedPOtraInst = false;
            this.disableInstitucion = false;
            this.renderedInsitucionOrigen = true;
        }
    }

    public boolean isCampoIngles() {
        return campoIngles;
    }

    public void setCampoIngles(boolean campoIngles) {
        this.campoIngles = campoIngles;
    }

    public boolean isCampoEspaniol() {
        return campoEspaniol;
    }

    public void setCampoEspaniol(boolean campoEspaniol) {
        this.campoEspaniol = campoEspaniol;
    }

    public List<MetSoporteAdjunto> getListSoporteAdjunto() {
        return listSoporteAdjunto;
    }

    public void setListSoporteAdjunto(List<MetSoporteAdjunto> listSoporteAdjunto) {
        this.listSoporteAdjunto = listSoporteAdjunto;
    }

    public String getServidorCorreo() {
        return servidorCorreo;
    }

    public void setServidorCorreo(String servidorCorreo) {
        this.servidorCorreo = servidorCorreo;
    }

    public String getPuertoCorreo() {
        return puertoCorreo;
    }

    public void setPuertoCorreo(String puertoCorreo) {
        this.puertoCorreo = puertoCorreo;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getTls() {
        return tls;
    }

    public void setTls(String tls) {
        this.tls = tls;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public List<String> getReceptores() {
        return receptores;
    }

    public void setReceptores(List<String> receptores) {
        this.receptores = receptores;
    }

    public List<String> getReceptoresCopia() {
        return receptoresCopia;
    }

    public void setReceptoresCopia(List<String> receptoresCopia) {
        this.receptoresCopia = receptoresCopia;
    }

    public List<String> getAdjuntos() {
        return adjuntos;
    }

    public void setAdjuntos(List<String> adjuntos) {
        this.adjuntos = adjuntos;
    }

    public MetSoporteAdjunto getSelectArchivo() {
        return selectArchivo;
    }

    public void setSelectArchivo(MetSoporteAdjunto selectArchivo) {
        this.selectArchivo = selectArchivo;
    }

    public boolean isRenderedInsitucionOrigen() {
        return renderedInsitucionOrigen;
    }

    public void setRenderedInsitucionOrigen(boolean renderedInsitucionOrigen) {
        this.renderedInsitucionOrigen = renderedInsitucionOrigen;
    }
    
    

    public String onVolver() {
        return "/vistas/formularioAdministrador.xhtml?faces-redirect=true";
    }

    public void aprobarSolicitud() {
        String res;
        this.usuarioSolPK.setSolicitudPK(metInfUsuariosol.getMetsolicitud().getSolicitudPK());
        metSolicitud.setSolicitudPK(usuarioSolPK.getSolicitudPK());
        res = servicios.aprobarSolicitud(metSolicitud);
        if (res.equalsIgnoreCase(Constantes.FAILED)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error.", "Error al aprobar la solicitud, intente de nuevo."));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Solicitud aprobada", "Solicitud aprobada con éxito"));
            enviarCorreoAprobacion();
        }
    }
}
