/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.konrad.movilidad.controller;

import edu.konrad.movilidad.bean.IdiomaBean;
import edu.konrad.movilidad.bean.MetCiudad;
import edu.konrad.movilidad.bean.MetContactoEmergencia;
import edu.konrad.movilidad.bean.MetCursosKL;
import edu.konrad.movilidad.bean.MetDistinciones;
import edu.konrad.movilidad.bean.MetEstadoSolicitud;
import edu.konrad.movilidad.bean.MetFacultadInstOrigen;
import edu.konrad.movilidad.bean.MetGenero;
import edu.konrad.movilidad.bean.MetHomologacionSaliente;
import edu.konrad.movilidad.bean.MetIdiomas;
import edu.konrad.movilidad.bean.MetInfPerfilMedico;
import edu.konrad.movilidad.bean.MetInfUsuario;
import edu.konrad.movilidad.bean.MetInfoCursoKL;
import edu.konrad.movilidad.bean.MetInfoIdioma;
import edu.konrad.movilidad.bean.MetInformacionAdicional;
import edu.konrad.movilidad.bean.MetInstitucionConvenio;
import edu.konrad.movilidad.bean.MetNivelEstudioEntrante;
import edu.konrad.movilidad.bean.MetObservacionSolicitud;
import edu.konrad.movilidad.bean.MetOtraInstitucionExt;
import edu.konrad.movilidad.bean.MetPais;
import edu.konrad.movilidad.bean.MetParentesco;
import edu.konrad.movilidad.bean.MetPerfilMedico;
import edu.konrad.movilidad.bean.MetProgramaInstOrigen;
import edu.konrad.movilidad.bean.MetProgramaKl;
import edu.konrad.movilidad.bean.MetSolicitud;
import edu.konrad.movilidad.bean.MetSoporteAdjunto;
import edu.konrad.movilidad.bean.MetTipoIdentificacion;
import edu.konrad.movilidad.bean.MetTipoMovilidad;
import edu.konrad.movilidad.bean.SMTPAuthenticator;
import edu.konrad.movilidad.client.FileMongoClient;
import edu.konrad.movilidad.constants.Constantes;
import edu.konrad.movilidad.servicios.ServiciosMovilidad;
import edu.konrad.movilidad.servicios.ServiciosMovilidadImpl;
import edu.konrad.movilidad.utils.GenerarFile;
import edu.konrad.movilidad.utils.SendEmail;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.activation.MimetypesFileTypeMap;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author leidy.sarmiento
 */
@ManagedBean
@ViewScoped
public class solicitudEntranteMB {

    private final static Logger log = Logger.getLogger(solicitudEntranteMB.class);

    private String servidorCorreo, puertoCorreo, emisor, asunto, mensaje, contrasena, tls, authentication;
    List<String> receptores;
    List<String> receptoresCopia;
    List<String> adjuntos;

    private boolean renderBtnEnviar;
    private MetSoporteAdjunto selectArchivo;

    private ServiciosMovilidad servicios;
    private MetSolicitud pkSolicitud;
    private MetInfUsuario obtenerIDUsuario;

    private MetInfUsuario metInfUsuario;
    private MetSolicitud usuarioSolPK = new MetSolicitud();
    private MetSolicitud metSolicitud;
    private MetSolicitud selectedSolicitud;
    private MetDistinciones selectedDistincion;
    private MetInfoCursoKL selectedInfoCursoKL;
    private MetInfoCursoKL selectedInfoCursoKLAlt;

    private MetInstitucionConvenio metInstitucionConvenio;
    private MetEstadoSolicitud metEstadoSolicitud = new MetEstadoSolicitud();

    private MetInfoIdioma idiomaEspanol = new MetInfoIdioma();
    private MetInfoIdioma idiomaIngles = new MetInfoIdioma();
    private MetInfoIdioma idiomaOtro = new MetInfoIdioma();

    private MetObservacionSolicitud metObservacion = new MetObservacionSolicitud();
    private MetHomologacionSaliente metHomologacionSaliente = new MetHomologacionSaliente();
    private MetDistinciones metDistinciones = new MetDistinciones();
    private MetContactoEmergencia metContactoEmergencia = new MetContactoEmergencia();
    private MetSoporteAdjunto metSoporteAdjunto = new MetSoporteAdjunto();
    private MetPerfilMedico metPerfilMedico = new MetPerfilMedico();
    private MetInformacionAdicional metInfoAdicional = new MetInformacionAdicional();
    private MetInfoCursoKL metInfoCursoKL = new MetInfoCursoKL();
    private MetInfoCursoKL metInfoCursoKLAlt = new MetInfoCursoKL();
    private MetInfPerfilMedico metInfPerfilMedico = new MetInfPerfilMedico();

    private boolean mostrarFormLista;
    private Date fecha = new Date();

    private List<MetSolicitud> listaSolicitudes;
    private List<MetSolicitud> filtrarSolicitud;
    private List<MetDistinciones> listaDistinciones;
    private List<MetInfoCursoKL> listaInfoCursoKL;
    private List<MetInfoCursoKL> listaInfoCursoKLAlt;

    @ManagedProperty(value = "#{inicioMB}")
    private InicioMB inicioMB;

    List<MetParentesco> tiposParentesco;
    List<MetTipoMovilidad> tiposMovilidad;
    List<MetNivelEstudioEntrante> NivelEstudio;
    List<MetCiudad> listaCiudad;
    List<MetPais> listaPais;
    List<MetCiudad> listaCiudadPais;
    List<MetInstitucionConvenio> listInstitucionesConvenio;
    List<MetTipoIdentificacion> tiposIdentificacion;
    List<MetGenero> tiposGenero;

    List<MetInstitucionConvenio> listaInstitucionesConvenio;
    List<MetFacultadInstOrigen> listaFacultadConvenio;
    List<MetProgramaInstOrigen> listaProgramaConvenio;
    List<MetNivelEstudioEntrante> listaNivelEstudioEntrante;
    List<MetProgramaKl> listaProgramasKL;
    List<MetCursosKL> listaCursosKL;

    private final List<String> itemsDocAdjunto = new ArrayList<String>();
    private final List<String> itemsTipoSangre = new ArrayList<String>();
    private final List<String> itemsSiNo = new ArrayList<String>();

    private boolean renderPCrearSolicitud;
    private boolean renderPTablaSolicitud;
    private boolean disabledIdiomaEsp, disabledIdiomaIng, disabledIdiomaOtr, idiomaNativoEsp, idiomaNativoIng, idiomaNativoOtr;
    private boolean renderedDistinciones;
    private boolean renderedCursosKL, renderedCursosKLAlt;
    private boolean renderedOtroParentesco;
    private boolean renderedPOtraInst;

    private int diasMovilidad, mDuracionMovilidad, dDuracionMovilidad;
    private String duracionMov;

    public ArrayList<String> arrayAspectosMedicos = new ArrayList<String>();

    /*Inf Inst Convenio*/
    private boolean selectOtraInstitucion, disableInstitucion, disableOtrInstitucion, varIptOtraCiudad, disabledCamposContacto, disableCheckOtraInst, disablebtnInst;
    private String convenioInstitucion, existeConvenio;

    /*INF ACADEMICA*/
    private boolean disTipMovilidad, disFecInicioMov, disFecFinMov, disDuracionMov, disMotivoEst, disbtnEspaniol, disbtnIngles, disbtnOtroidioma;

    /*INF ADICIONAL*/
    private boolean disVigenciaDesde, disVigenciaHasta, disDirAlojamiento, disTelAlojamiento;

    private boolean botonCrearSolicitud, tabFormEntrante;
    //  private String existeSolicitud;
    private boolean renderedDuracionMovilidad;

    private boolean renderedPersonal;
    private boolean renderedInstOrigen;
    private boolean renderedAcademica;
    private boolean renderedMovilidad;
    private boolean renderedPrfilMedico;
    private boolean renderedDocAdjuntos;
    private int variablePanel;
    private boolean renderbtnSiguiente;
    private boolean renderbtnEnviar;
    private boolean renderAtras;

    private boolean renderSolicitud;

    private boolean disOtroNivelEducacion, disNivelEducacion, selecOtroNivelEdu;

    private IdiomaBean idiomaBean = new IdiomaBean();

    private StreamedContent fileTest;

    /*---------- SQL ------------*/
    private MetOtraInstitucionExt metOtraInstitucion = new MetOtraInstitucionExt();

    private boolean campoIngles, campoEspaniol;

    private List<MetSoporteAdjunto> listSoporteAdjunto;

    private boolean renderedInsitucionOrigen;

    private String existeOtraInsti;

    private boolean renderTituloEsp;
    private boolean renderTituloIng;

    public boolean validarModuloEstudiante, validarModuloInstOrigen, validarModuloProgAcademico,
            validarModuloAcuerdoApren, validarModuloContactoEmer, validarModuloArchivo;

    private Boolean siguientePagina;

    @PostConstruct
    public void init() {

        this.servicios = new ServiciosMovilidadImpl();

        //----------------------------------------  CONSULTAS -----------------------------------
        this.idiomaBean.doCambioIdiomaLink(inicioMB.varIdioma);
        this.metInfUsuario = servicios.obtenerInfoUsuario(inicioMB.getUsuarioSesion().getNamUsuario());
        this.obtenerIDUsuario = servicios.obtenerDtUsuario(metInfUsuario.getUsuario());
        this.pkSolicitud = servicios.obtenerIDSolicitud(obtenerIDUsuario.getUsuarioPK());
        this.metSolicitud = servicios.obtenerInfSolicitud(pkSolicitud.getSolicitudPK());
        this.metContactoEmergencia = servicios.obtenerContactoEmergenciaFent(pkSolicitud.getSolicitudPK());
        this.metInstitucionConvenio = servicios.obtenerInfInstitucionConvenio(metSolicitud.getMetInstitucionConvenio().getPk());
        this.listaSolicitudes = servicios.listarSolicitudesEnviadasFent(metInfUsuario.getUsuarioPK());

        //-------------------------------------- LISTAS ---------------------------------------
        this.tiposIdentificacion = servicios.tiposIdentificacion();
        this.listaPais = servicios.listaPais();
        this.tiposGenero = servicios.tiposGenero();
        this.tiposParentesco = servicios.tiposParentesco();
        this.tiposMovilidad = servicios.tiposMovilidad();
        this.NivelEstudio = servicios.nivelesEstudio();
        this.listSoporteAdjunto = servicios.selectListArchivos(pkSolicitud.getSolicitudPK());

        //------------------------------------------ VARIABLES DEL FORMULARIO
        this.renderedPersonal = true;
        this.renderbtnSiguiente = true;
        this.renderAtras = true;

        this.usuarioSolPK = servicios.consultarPKSolicitud(metInfUsuario.getUsuarioPK());

        this.listaInstitucionesConvenio = servicios.listaInstitucionesConvenio();
        this.listaFacultadConvenio = servicios.listaFacultadesConvenio();
        this.listaProgramaConvenio = servicios.listaProgramasConvenio();
        this.listaNivelEstudioEntrante = servicios.listaNivelEstudio();
        this.listaProgramasKL = servicios.listarProgramasKL();
        this.listaCursosKL = servicios.listarCursosKL();

        this.idiomaEspanol = servicios.obtenerMetInfoIdiomaEsp(pkSolicitud.getSolicitudPK());
        this.idiomaIngles = servicios.obtenerMetInfoIdiomaIng(pkSolicitud.getSolicitudPK());
        this.idiomaOtro = servicios.obtenerMetInfoIdiomaOtr(pkSolicitud.getSolicitudPK());
        this.listaDistinciones = servicios.obtenerDistincion(pkSolicitud.getSolicitudPK());
        this.listaInfoCursoKL = servicios.obtenerInfoCursoKL(pkSolicitud.getSolicitudPK());
        this.listaInfoCursoKLAlt = servicios.obtenerInfoCursoKLAlt(pkSolicitud.getSolicitudPK());

        this.renderedDistinciones = true;
        this.renderedCursosKL = true;
        this.renderedCursosKLAlt = true;
        this.mostrarFormLista = false;
        this.siguientePagina = false;

        this.disOtroNivelEducacion = true;

        seccionInfMovilidad();
        contDistincion();
        otroParentesco();
        definirIdioma();

        itemsDocAdjunto.add("Pasaporte");
        itemsDocAdjunto.add("Carta de motivación");
        itemsDocAdjunto.add("Certificado original de notas");
        itemsDocAdjunto.add("Solvencia económica");
        itemsDocAdjunto.add("Carta de compromiso");
        itemsDocAdjunto.add("Carta de aplicación");

        itemsTipoSangre.add("A-");
        itemsTipoSangre.add("A+");
        itemsTipoSangre.add("B-");
        itemsTipoSangre.add("B+");
        itemsTipoSangre.add("AB-");
        itemsTipoSangre.add("AB+");
        itemsTipoSangre.add("O-");
        itemsTipoSangre.add("O+");

        /*-------------------------------------------------------------------*/
        this.metOtraInstitucion = servicios.consultarInstitucionExt(pkSolicitud.getSolicitudPK());

        definirEstadoSolicitud();
        validaInstitucionOrigen();

    }

    public Boolean getSiguientePagina() {
        return siguientePagina;
    }

    public void setSiguientePagina(Boolean siguientePagina) {
        this.siguientePagina = siguientePagina;
    }

    public MetInfoCursoKL getSelectedInfoCursoKLAlt() {
        return selectedInfoCursoKLAlt;
    }

    public void setSelectedInfoCursoKLAlt(MetInfoCursoKL selectedInfoCursoKLAlt) {
        this.selectedInfoCursoKLAlt = selectedInfoCursoKLAlt;
    }

    public MetOtraInstitucionExt getMetOtraInstitucion() {
        return metOtraInstitucion;
    }

    public void setMetOtraInstitucion(MetOtraInstitucionExt metOtraInstitucion) {
        this.metOtraInstitucion = metOtraInstitucion;
    }

    public boolean isDisOtroNivelEducacion() {
        return disOtroNivelEducacion;
    }

    public void setDisOtroNivelEducacion(boolean disOtroNivelEducacion) {
        this.disOtroNivelEducacion = disOtroNivelEducacion;
    }

    public boolean isDisNivelEducacion() {
        return disNivelEducacion;
    }

    public void setDisNivelEducacion(boolean disNivelEducacion) {
        this.disNivelEducacion = disNivelEducacion;
    }

    public boolean isSelecOtroNivelEdu() {
        return selecOtroNivelEdu;
    }

    public void setSelecOtroNivelEdu(boolean selecOtroNivelEdu) {
        this.selecOtroNivelEdu = selecOtroNivelEdu;
    }

    private StreamedContent dfile;

    public StreamedContent getDfile() {
        return this.dfile;
    }

    public void setDfile(StreamedContent dFile) {
        this.dfile = dFile;
    }

    public boolean isRenderBtnEnviar() {
        return renderBtnEnviar;
    }

    public void setRenderBtnEnviar(boolean renderBtnEnviar) {
        this.renderBtnEnviar = renderBtnEnviar;
    }

    public boolean isCampoIngles() {
        return campoIngles;
    }

    public void setCampoIngles(boolean campoIngles) {
        this.campoIngles = campoIngles;
    }

    public boolean isCampoEspaniol() {
        return campoEspaniol;
    }

    public void setCampoEspaniol(boolean campoEspaniol) {
        this.campoEspaniol = campoEspaniol;
    }

    public List<MetSoporteAdjunto> getListSoporteAdjunto() {
        return listSoporteAdjunto;
    }

    public void setListSoporteAdjunto(List<MetSoporteAdjunto> listSoporteAdjunto) {
        this.listSoporteAdjunto = listSoporteAdjunto;
    }

    public String getServidorCorreo() {
        return servidorCorreo;
    }

    public void setServidorCorreo(String servidorCorreo) {
        this.servidorCorreo = servidorCorreo;
    }

    public String getPuertoCorreo() {
        return puertoCorreo;
    }

    public void setPuertoCorreo(String puertoCorreo) {
        this.puertoCorreo = puertoCorreo;
    }

    public String getEmisor() {
        return emisor;
    }

    public void setEmisor(String emisor) {
        this.emisor = emisor;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getTls() {
        return tls;
    }

    public void setTls(String tls) {
        this.tls = tls;
    }

    public String getAuthentication() {
        return authentication;
    }

    public void setAuthentication(String authentication) {
        this.authentication = authentication;
    }

    public List<String> getReceptores() {
        return receptores;
    }

    public void setReceptores(List<String> receptores) {
        this.receptores = receptores;
    }

    public List<String> getReceptoresCopia() {
        return receptoresCopia;
    }

    public void setReceptoresCopia(List<String> receptoresCopia) {
        this.receptoresCopia = receptoresCopia;
    }

    public List<String> getAdjuntos() {
        return adjuntos;
    }

    public void setAdjuntos(List<String> adjuntos) {
        this.adjuntos = adjuntos;
    }

    public MetSoporteAdjunto getSelectArchivo() {
        return selectArchivo;
    }

    public void setSelectArchivo(MetSoporteAdjunto selectArchivo) {
        this.selectArchivo = selectArchivo;
    }

    public boolean isRenderedInsitucionOrigen() {
        return renderedInsitucionOrigen;
    }

    public void setRenderedInsitucionOrigen(boolean renderedInsitucionOrigen) {
        this.renderedInsitucionOrigen = renderedInsitucionOrigen;
    }

    public boolean isRenderTituloIng() {
        return renderTituloIng;
    }

    public void setRenderTituloIng(boolean renderTituloIng) {
        this.renderTituloIng = renderTituloIng;
    }

    public boolean isRenderTituloEsp() {
        return renderTituloEsp;
    }

    public void setRenderTituloEsp(boolean renderTituloEsp) {
        this.renderTituloEsp = renderTituloEsp;
    }

    public StreamedContent getFileTest() {
        return fileTest;
    }

    public void setFileTest(StreamedContent fileTest) {
        this.fileTest = fileTest;
    }

    public IdiomaBean getIdiomaBean() {
        return idiomaBean;
    }

    public void setIdiomaBean(IdiomaBean idiomaBean) {
        this.idiomaBean = idiomaBean;
    }

    public boolean isRenderbtnEnviar() {
        return renderbtnEnviar;
    }

    public void setRenderbtnEnviar(boolean renderbtnEnviar) {
        this.renderbtnEnviar = renderbtnEnviar;
    }

    public boolean isRenderedDocAdjuntos() {
        return renderedDocAdjuntos;
    }

    public void setRenderedDocAdjuntos(boolean renderedDocAdjuntos) {
        this.renderedDocAdjuntos = renderedDocAdjuntos;
    }

    public boolean isRenderAtras() {
        return renderAtras;
    }

    public void setRenderAtras(boolean renderAtras) {
        this.renderAtras = renderAtras;
    }

    public boolean isRenderbtnSiguiente() {
        return renderbtnSiguiente;
    }

    public void setRenderbtnSiguiente(boolean renderbtnSiguiente) {
        this.renderbtnSiguiente = renderbtnSiguiente;
    }

    public boolean isRenderedInstOrigen() {
        return renderedInstOrigen;
    }

    public void setRenderedInstOrigen(boolean renderedInstOrigen) {
        this.renderedInstOrigen = renderedInstOrigen;
    }

    public boolean isRenderedAcademica() {
        return renderedAcademica;
    }

    public void setRenderedAcademica(boolean renderedAcademica) {
        this.renderedAcademica = renderedAcademica;
    }

    public boolean isRenderedMovilidad() {
        return renderedMovilidad;
    }

    public void setRenderedMovilidad(boolean renderedMovilidad) {
        this.renderedMovilidad = renderedMovilidad;
    }

    public boolean isRenderedPrfilMedico() {
        return renderedPrfilMedico;
    }

    public void setRenderedPrfilMedico(boolean renderedPrfilMedico) {
        this.renderedPrfilMedico = renderedPrfilMedico;
    }

    public int getVariablePanel() {
        return variablePanel;
    }

    public void setVariablePanel(int variablePanel) {
        this.variablePanel = variablePanel;
    }

    public boolean isRenderedPersonal() {
        return renderedPersonal;
    }

    public void setRenderedPersonal(boolean renderedPersonal) {
        this.renderedPersonal = renderedPersonal;
    }

    public boolean isRenderedDuracionMovilidad() {
        return renderedDuracionMovilidad;
    }

    public void setRenderedDuracionMovilidad(boolean renderedDuracionMovilidad) {
        this.renderedDuracionMovilidad = renderedDuracionMovilidad;
    }

    public boolean isRenderedCursosKLAlt() {
        return renderedCursosKLAlt;
    }

    public void setRenderedCursosKLAlt(boolean renderedCursosKLAlt) {
        this.renderedCursosKLAlt = renderedCursosKLAlt;
    }

    public MetInfoCursoKL getSelectedInfoCursoKL() {
        return selectedInfoCursoKL;
    }

    public void setSelectedInfoCursoKL(MetInfoCursoKL selectedInfoCursoKL) {
        this.selectedInfoCursoKL = selectedInfoCursoKL;
    }

    public List<MetInfoCursoKL> getListaInfoCursoKLAlt() {
        return listaInfoCursoKLAlt;
    }

    public void setListaInfoCursoKLAlt(List<MetInfoCursoKL> listaInfoCursoKLAlt) {
        this.listaInfoCursoKLAlt = listaInfoCursoKLAlt;
    }

    public List<MetInfoCursoKL> getListaInfoCursoKL() {
        return listaInfoCursoKL;
    }

    public void setListaInfoCursoKL(List<MetInfoCursoKL> listaInfoCursoKL) {
        this.listaInfoCursoKL = listaInfoCursoKL;
    }

    public List<MetCursosKL> getListaCursosKL() {
        return listaCursosKL;
    }

    public void setListaCursosKL(List<MetCursosKL> listaCursosKL) {
        this.listaCursosKL = listaCursosKL;
    }

    public int getmDuracionMovilidad() {
        return mDuracionMovilidad;
    }

    public void setmDuracionMovilidad(int mDuracionMovilidad) {
        this.mDuracionMovilidad = mDuracionMovilidad;
    }

    public int getdDuracionMovilidad() {
        return dDuracionMovilidad;
    }

    public void setdDuracionMovilidad(int dDuracionMovilidad) {
        this.dDuracionMovilidad = dDuracionMovilidad;
    }

    public String getDuracionMov() {
        return duracionMov;
    }

    public void setDuracionMov(String duracionMov) {
        this.duracionMov = duracionMov;
    }

    public int getDiasMovilidad() {
        return diasMovilidad;
    }

    public void setDiasMovilidad(int diasMovilidad) {
        this.diasMovilidad = diasMovilidad;
    }

    public List<MetProgramaKl> getListaProgramasKL() {
        return listaProgramasKL;
    }

    public void setListaProgramasKL(List<MetProgramaKl> listaProgramasKL) {
        this.listaProgramasKL = listaProgramasKL;
    }

    public boolean isRenderedPOtraInst() {
        return renderedPOtraInst;
    }

    public void setRenderedPOtraInst(boolean renderedPOtraInst) {
        this.renderedPOtraInst = renderedPOtraInst;
    }

    public boolean isRenderedOtroParentesco() {
        return renderedOtroParentesco;
    }

    public void setRenderedOtroParentesco(boolean renderedOtroParentesco) {
        this.renderedOtroParentesco = renderedOtroParentesco;
    }

    public MetDistinciones getSelectedDistincion() {
        return selectedDistincion;
    }

    public void setSelectedDistincion(MetDistinciones selectedDistincion) {
        this.selectedDistincion = selectedDistincion;
    }

    public List<MetDistinciones> getListaDistinciones() {
        return listaDistinciones;
    }

    public void setListaDistinciones(List<MetDistinciones> listaDistinciones) {
        this.listaDistinciones = listaDistinciones;
    }

    public boolean isRenderedCursosKL() {
        return renderedCursosKL;
    }

    public void setRenderedCursosKL(boolean renderedCursosKL) {
        this.renderedCursosKL = renderedCursosKL;
    }

    public boolean isRenderedDistinciones() {
        return renderedDistinciones;
    }

    public void setRenderedDistinciones(boolean renderedDistinciones) {
        this.renderedDistinciones = renderedDistinciones;
    }

    public boolean isIdiomaNativoIng() {
        return idiomaNativoIng;
    }

    public void setIdiomaNativoIng(boolean idiomaNativoIng) {
        this.idiomaNativoIng = idiomaNativoIng;
    }

    public boolean isIdiomaNativoOtr() {
        return idiomaNativoOtr;
    }

    public void setIdiomaNativoOtr(boolean idiomaNativoOtr) {
        this.idiomaNativoOtr = idiomaNativoOtr;
    }

    public boolean isIdiomaNativoEsp() {
        return idiomaNativoEsp;
    }

    public void setIdiomaNativoEsp(boolean idiomaNativoEsp) {
        this.idiomaNativoEsp = idiomaNativoEsp;
    }

    public boolean isDisabledIdiomaIng() {
        return disabledIdiomaIng;
    }

    public void setDisabledIdiomaIng(boolean disabledIdiomaIng) {
        this.disabledIdiomaIng = disabledIdiomaIng;
    }

    public boolean isDisabledIdiomaOtr() {
        return disabledIdiomaOtr;
    }

    public void setDisabledIdiomaOtr(boolean disabledIdiomaOtr) {
        this.disabledIdiomaOtr = disabledIdiomaOtr;
    }

    public boolean isDisabledIdiomaEsp() {
        return disabledIdiomaEsp;
    }

    public void setDisabledIdiomaEsp(boolean disabledIdiomaEsp) {
        this.disabledIdiomaEsp = disabledIdiomaEsp;
    }

    public boolean isRenderPCrearSolicitud() {
        return renderPCrearSolicitud;
    }

    public void setRenderPCrearSolicitud(boolean renderPCrearSolicitud) {
        this.renderPCrearSolicitud = renderPCrearSolicitud;
    }

    public boolean isRenderPTablaSolicitud() {
        return renderPTablaSolicitud;
    }

    public void setRenderPTablaSolicitud(boolean renderPTablaSolicitud) {
        this.renderPTablaSolicitud = renderPTablaSolicitud;
    }

    public List<MetInstitucionConvenio> getListaInstitucionesConvenio() {
        return listaInstitucionesConvenio;
    }

    public void setListaInstitucionesConvenio(List<MetInstitucionConvenio> listaInstitucionesConvenio) {
        this.listaInstitucionesConvenio = listaInstitucionesConvenio;
    }

    public List<MetFacultadInstOrigen> getListaFacultadConvenio() {
        return listaFacultadConvenio;
    }

    public void setListaFacultadConvenio(List<MetFacultadInstOrigen> listaFacultadConvenio) {
        this.listaFacultadConvenio = listaFacultadConvenio;
    }

    public List<MetProgramaInstOrigen> getListaProgramaConvenio() {
        return listaProgramaConvenio;
    }

    public void setListaProgramaConvenio(List<MetProgramaInstOrigen> listaProgramaConvenio) {
        this.listaProgramaConvenio = listaProgramaConvenio;
    }

    public List<MetNivelEstudioEntrante> getListaNivelEstudioEntrante() {
        return listaNivelEstudioEntrante;
    }

    public void setListaNivelEstudioEntrante(List<MetNivelEstudioEntrante> listaNivelEstudioEntrante) {
        this.listaNivelEstudioEntrante = listaNivelEstudioEntrante;
    }

    public MetInfPerfilMedico getMetInfPerfilMedico() {
        return metInfPerfilMedico;
    }

    public void setMetInfPerfilMedico(MetInfPerfilMedico metInfPerfilMedico) {
        this.metInfPerfilMedico = metInfPerfilMedico;
    }

    public MetObservacionSolicitud getMetObservacion() {
        return metObservacion;
    }

    public void setMetObservacion(MetObservacionSolicitud metObservacion) {
        this.metObservacion = metObservacion;
    }

    public MetHomologacionSaliente getMetHomologacionSaliente() {
        return metHomologacionSaliente;
    }

    public void setMetHomologacionSaliente(MetHomologacionSaliente metHomologacionSaliente) {
        this.metHomologacionSaliente = metHomologacionSaliente;
    }

    public MetDistinciones getMetDistinciones() {
        return metDistinciones;
    }

    public void setMetDistinciones(MetDistinciones metDistinciones) {
        this.metDistinciones = metDistinciones;
    }

    public MetSoporteAdjunto getMetSoporteAdjunto() {
        return metSoporteAdjunto;
    }

    public void setMetSoporteAdjunto(MetSoporteAdjunto metSoporteAdjunto) {
        this.metSoporteAdjunto = metSoporteAdjunto;
    }

    public MetInfoCursoKL getMetInfoCursoKL() {
        return metInfoCursoKL;
    }

    public void setMetInfoCursoKL(MetInfoCursoKL metInfoCursoKL) {
        this.metInfoCursoKL = metInfoCursoKL;
    }

    public MetInfoCursoKL getMetInfoCursoKLAlt() {
        return metInfoCursoKLAlt;
    }

    public void setMetInfoCursoKLAlt(MetInfoCursoKL metInfoCursoKLAlt) {
        this.metInfoCursoKLAlt = metInfoCursoKLAlt;
    }

    public boolean isDisbtnEspaniol() {
        return disbtnEspaniol;
    }

    public void setDisbtnEspaniol(boolean disbtnEspaniol) {
        this.disbtnEspaniol = disbtnEspaniol;
    }

    public boolean isDisbtnIngles() {
        return disbtnIngles;
    }

    public void setDisbtnIngles(boolean disbtnIngles) {
        this.disbtnIngles = disbtnIngles;
    }

    public boolean isDisbtnOtroidioma() {
        return disbtnOtroidioma;
    }

    public void setDisbtnOtroidioma(boolean disbtnOtroidioma) {
        this.disbtnOtroidioma = disbtnOtroidioma;
    }

    public MetSolicitud getSelectedSolicitud() {
        return selectedSolicitud;
    }

    public void setSelectedSolicitud(MetSolicitud selectedSolicitud) {
        this.selectedSolicitud = selectedSolicitud;
    }

    public List<MetSolicitud> getFiltrarSolicitud() {
        return filtrarSolicitud;
    }

    public void setFiltrarSolicitud(List<MetSolicitud> filtrarSolicitud) {
        this.filtrarSolicitud = filtrarSolicitud;
    }

    public List<MetSolicitud> getListaSolicitudes() {
        return listaSolicitudes;
    }

    public void setListaSolicitudes(List<MetSolicitud> listaSolicitudes) {
        this.listaSolicitudes = listaSolicitudes;
    }

    public MetInformacionAdicional getMetInfoAdicional() {
        return metInfoAdicional;
    }

    public void setMetInfoAdicional(MetInformacionAdicional metInfoAdicional) {
        this.metInfoAdicional = metInfoAdicional;
    }

    public MetPerfilMedico getMetPerfilMedico() {
        return metPerfilMedico;
    }

    public void setMetPerfilMedico(MetPerfilMedico metPerfilMedico) {
        this.metPerfilMedico = metPerfilMedico;
    }

    public MetContactoEmergencia getMetContactoEmergencia() {
        return metContactoEmergencia;
    }

    public void setMetContactoEmergencia(MetContactoEmergencia metContactoEmergencia) {
        this.metContactoEmergencia = metContactoEmergencia;
    }

    public MetInstitucionConvenio getMetInstitucionConvenio() {
        return metInstitucionConvenio;
    }

    public void setMetInstitucionConvenio(MetInstitucionConvenio metInstitucionConvenio) {
        this.metInstitucionConvenio = metInstitucionConvenio;
    }

    public MetSolicitud getMetSolicitud() {
        return metSolicitud;
    }

    public void setMetSolicitud(MetSolicitud metSolicitud) {
        this.metSolicitud = metSolicitud;
    }

    public MetEstadoSolicitud getMetEstadoSolicitud() {
        return metEstadoSolicitud;
    }

    public void setMetEstadoSolicitud(MetEstadoSolicitud metEstadoSolicitud) {
        this.metEstadoSolicitud = metEstadoSolicitud;
    }

    public boolean isBotonCrearSolicitud() {
        return botonCrearSolicitud;
    }

    public void setBotonCrearSolicitud(boolean botonCrearSolicitud) {
        this.botonCrearSolicitud = botonCrearSolicitud;
    }

    public boolean isTabFormEntrante() {
        return tabFormEntrante;
    }

    public void setTabFormEntrante(boolean tabFormEntrante) {
        this.tabFormEntrante = tabFormEntrante;
    }

    public MetSolicitud getUsuarioSolPK() {
        return usuarioSolPK;
    }

    public void setUsuarioSolPK(MetSolicitud usuarioSolPK) {
        this.usuarioSolPK = usuarioSolPK;
    }

    public List<String> getItemsDocAdjunto() {
        return itemsDocAdjunto;
    }

    public List<String> getItemsTipoSangre() {
        return itemsTipoSangre;
    }

    public List<String> getItemsSiNo() {
        return itemsSiNo;
    }

    public boolean isSelectOtraInstitucion() {
        return selectOtraInstitucion;
    }

    public void setSelectOtraInstitucion(boolean selectOtraInstitucion) {
        this.selectOtraInstitucion = selectOtraInstitucion;
    }

    public boolean isDisableInstitucion() {
        return disableInstitucion;
    }

    public void setDisableInstitucion(boolean disableInstitucion) {
        this.disableInstitucion = disableInstitucion;
    }

    public boolean isDisableOtrInstitucion() {
        return disableOtrInstitucion;
    }

    public void setDisableOtrInstitucion(boolean disableOtrInstitucion) {
        this.disableOtrInstitucion = disableOtrInstitucion;
    }

    public boolean isVarIptOtraCiudad() {
        return varIptOtraCiudad;
    }

    public void setVarIptOtraCiudad(boolean varIptOtraCiudad) {
        this.varIptOtraCiudad = varIptOtraCiudad;
    }

    public boolean isDisabledCamposContacto() {
        return disabledCamposContacto;
    }

    public void setDisabledCamposContacto(boolean disabledCamposContacto) {
        this.disabledCamposContacto = disabledCamposContacto;
    }

    public boolean isDisableCheckOtraInst() {
        return disableCheckOtraInst;
    }

    public void setDisableCheckOtraInst(boolean disableCheckOtraInst) {
        this.disableCheckOtraInst = disableCheckOtraInst;
    }

    public boolean isDisablebtnInst() {
        return disablebtnInst;
    }

    public void setDisablebtnInst(boolean disablebtnInst) {
        this.disablebtnInst = disablebtnInst;
    }

    public String getConvenioInstitucion() {
        return convenioInstitucion;
    }

    public void setConvenioInstitucion(String convenioInstitucion) {
        this.convenioInstitucion = convenioInstitucion;
    }

    public String getExisteConvenio() {
        return existeConvenio;
    }

    public void setExisteConvenio(String existeConvenio) {
        this.existeConvenio = existeConvenio;
    }

    public boolean isDisTipMovilidad() {
        return disTipMovilidad;
    }

    public void setDisTipMovilidad(boolean disTipMovilidad) {
        this.disTipMovilidad = disTipMovilidad;
    }

    public boolean isDisFecInicioMov() {
        return disFecInicioMov;
    }

    public void setDisFecInicioMov(boolean disFecInicioMov) {
        this.disFecInicioMov = disFecInicioMov;
    }

    public boolean isDisFecFinMov() {
        return disFecFinMov;
    }

    public void setDisFecFinMov(boolean disFecFinMov) {
        this.disFecFinMov = disFecFinMov;
    }

    public boolean isDisDuracionMov() {
        return disDuracionMov;
    }

    public void setDisDuracionMov(boolean disDuracionMov) {
        this.disDuracionMov = disDuracionMov;
    }

    public boolean isDisMotivoEst() {
        return disMotivoEst;
    }

    public void setDisMotivoEst(boolean disMotivoEst) {
        this.disMotivoEst = disMotivoEst;
    }

    public boolean isDisVigenciaDesde() {
        return disVigenciaDesde;
    }

    public void setDisVigenciaDesde(boolean disVigenciaDesde) {
        this.disVigenciaDesde = disVigenciaDesde;
    }

    public boolean isDisVigenciaHasta() {
        return disVigenciaHasta;
    }

    public void setDisVigenciaHasta(boolean disVigenciaHasta) {
        this.disVigenciaHasta = disVigenciaHasta;
    }

    public boolean isDisDirAlojamiento() {
        return disDirAlojamiento;
    }

    public void setDisDirAlojamiento(boolean disDirAlojamiento) {
        this.disDirAlojamiento = disDirAlojamiento;
    }

    public boolean isDisTelAlojamiento() {
        return disTelAlojamiento;
    }

    public void setDisTelAlojamiento(boolean disTelAlojamiento) {
        this.disTelAlojamiento = disTelAlojamiento;
    }

    public MetInfUsuario getMetInfUsuario() {
        return metInfUsuario;
    }

    public void setMetInfUsuario(MetInfUsuario metInfUsuario) {
        this.metInfUsuario = metInfUsuario;
    }

    public ServiciosMovilidad getServicios() {
        return servicios;
    }

    public void setServicios(ServiciosMovilidad servicios) {
        this.servicios = servicios;
    }

    public InicioMB getInicioMB() {
        return inicioMB;
    }

    public void setInicioMB(InicioMB inicioMB) {
        this.inicioMB = inicioMB;
    }

    public List<MetParentesco> getTiposParentesco() {
        return tiposParentesco;
    }

    public void setTiposParentesco(List<MetParentesco> tiposParentesco) {
        this.tiposParentesco = tiposParentesco;
    }

    public List<MetTipoMovilidad> getTiposMovilidad() {
        return tiposMovilidad;
    }

    public void setTiposMovilidad(List<MetTipoMovilidad> tiposMovilidad) {
        this.tiposMovilidad = tiposMovilidad;
    }

    public List<MetNivelEstudioEntrante> getNivelEstudio() {
        return NivelEstudio;
    }

    public void setNivelEstudio(List<MetNivelEstudioEntrante> NivelEstudio) {
        this.NivelEstudio = NivelEstudio;
    }

    public List<MetCiudad> getListaCiudad() {
        return listaCiudad;
    }

    public void setListaCiudad(List<MetCiudad> listaCiudad) {
        this.listaCiudad = listaCiudad;
    }

    public List<MetPais> getListaPais() {
        return listaPais;
    }

    public void setListaPais(List<MetPais> listaPais) {
        this.listaPais = listaPais;
    }

    public List<MetCiudad> getListaCiudadPais() {
        return listaCiudadPais;
    }

    public void setListaCiudadPais(List<MetCiudad> listaCiudadPais) {
        this.listaCiudadPais = listaCiudadPais;
    }

    public List<MetInstitucionConvenio> getListInstitucionesConvenio() {
        return listInstitucionesConvenio;
    }

    public void setListInstitucionesConvenio(List<MetInstitucionConvenio> listInstitucionesConvenio) {
        this.listInstitucionesConvenio = listInstitucionesConvenio;
    }

    public List<MetTipoIdentificacion> getTiposIdentificacion() {
        return tiposIdentificacion;
    }

    public void setTiposIdentificacion(List<MetTipoIdentificacion> tiposIdentificacion) {
        this.tiposIdentificacion = tiposIdentificacion;
    }

    public List<MetGenero> getTiposGenero() {
        return tiposGenero;
    }

    public void setTiposGenero(List<MetGenero> tiposGenero) {
        this.tiposGenero = tiposGenero;
    }

    public ArrayList<String> getArrayAspectosMedicos() {
        return arrayAspectosMedicos;
    }

    public void setArrayAspectosMedicos(ArrayList<String> arrayAspectosMedicos) {
        this.arrayAspectosMedicos = arrayAspectosMedicos;
    }

    public MetInfoIdioma getIdiomaEspanol() {
        return idiomaEspanol;
    }

    public void setIdiomaEspanol(MetInfoIdioma idiomaEspanol) {
        this.idiomaEspanol = idiomaEspanol;
    }

    public MetInfoIdioma getIdiomaIngles() {
        return idiomaIngles;
    }

    public void setIdiomaIngles(MetInfoIdioma idiomaIngles) {
        this.idiomaIngles = idiomaIngles;
    }

    public MetInfoIdioma getIdiomaOtro() {
        return idiomaOtro;
    }

    public void setIdiomaOtro(MetInfoIdioma idiomaOtro) {
        this.idiomaOtro = idiomaOtro;
    }

    public boolean isMostrarFormLista() {
        return mostrarFormLista;
    }

    public void setMostrarFormLista(boolean mostrarFormLista) {
        this.mostrarFormLista = mostrarFormLista;
    }

    public MetSolicitud getPkSolicitud() {
        return pkSolicitud;
    }

    public void setPkSolicitud(MetSolicitud pkSolicitud) {
        this.pkSolicitud = pkSolicitud;
    }

    public MetInfUsuario getObtenerIDUsuario() {
        return obtenerIDUsuario;
    }

    public void setObtenerIDUsuario(MetInfUsuario obtenerIDUsuario) {
        this.obtenerIDUsuario = obtenerIDUsuario;
    }

    /*EDITAR INFORMACIÓN*/
    public boolean isRenderSolicitud() {
        return renderSolicitud;
    }

    public void setRenderSolicitud(boolean renderSolicitud) {
        this.renderSolicitud = renderSolicitud;
    }

    public String onActualizarPagina() {
        return "/vistas/formularioEntrante.xhtml?faces-redirect=true";
    }

    public String onCerrarSesion() {
        return "/index.xhtml?faces-redirect=true";
    }

    public void mostrarInstConvenio() {
        selectOtraInstitucion = false;
        disableOtrInstitucion = true;
        this.metInstitucionConvenio = servicios.obtenerInfInstitucionConvenio(metSolicitud.getMetInstitucionConvenio().getPk());
    }

    public void procOtraInstitucion() {
        if (selectOtraInstitucion == true) {
            metSolicitud.getMetInstitucionConvenio().setPk(null);
            metSolicitud.setExisteConvenio(null);
            metOtraInstitucion.setNombreInstitucion(null);
            metOtraInstitucion.getMetPais().setPk(null);
            metOtraInstitucion.setCiudadInstitucion(null);
            metOtraInstitucion.setNombreContacto(null);
            metOtraInstitucion.setCargoContacto(null);
            metOtraInstitucion.setTelContacto(null);
            metOtraInstitucion.setEmailContacto(null);
            disableInstitucion = true;
            disableOtrInstitucion = false;
            renderedPOtraInst = true;
            renderedInsitucionOrigen = false;
        }

        if (selectOtraInstitucion == false) {
            this.metInstitucionConvenio = servicios.obtenerInfInstitucionConvenio(metSolicitud.getMetInstitucionConvenio().getPk());
            metSolicitud.setNombreContactoEntrante(null);
            metSolicitud.setTelContactoEntrante(null);
            metSolicitud.setEmailContactoEntrante(null);
            metSolicitud.getMetInstitucionConvenio().setPk(null);
            metSolicitud.setExisteConvenio(null);
            disableInstitucion = false;
            disableOtrInstitucion = true;
            renderedPOtraInst = false;
            renderedInsitucionOrigen = true;
        }
    }

    public void seccionInfMovilidad() {
        if (metSolicitud.getMetTipoMovilidad().getPk() == null) {
            disTipMovilidad = false;
        }
        if (metSolicitud.getMetTipoMovilidad().getPk() != null) {
            disTipMovilidad = true;
        }

        if (metSolicitud.getFechaInicioMovilidad() == null) {
            disFecInicioMov = false;
        }
        if (metSolicitud.getFechaInicioMovilidad() != null) {
            disFecInicioMov = true;
        }

        if (metSolicitud.getFechaFinMovilidad() == null) {
            disFecFinMov = false;
        }
        if (metSolicitud.getFechaFinMovilidad() != null) {
            disFecFinMov = true;
        }

        if (metSolicitud.getDuracionMovilidad() == null) {
            disDuracionMov = false;
        }
        if (metSolicitud.getDuracionMovilidad() != null) {
            disDuracionMov = true;
        }

        if (metSolicitud.getMotivacionEstudiante() == null) {
            disMotivoEst = false;
        }
        if (metSolicitud.getMotivacionEstudiante() != null) {
            disMotivoEst = true;
        }
    }

    public void seccionInfAdicional() {

        if (metSolicitud.getVigenciaDesde() == null) {
            disVigenciaDesde = false;
        }
        if (metSolicitud.getVigenciaDesde() != null) {
            disVigenciaDesde = true;
        }

        if (metSolicitud.getVigenciaHasta() == null) {
            disVigenciaHasta = false;
        }
        if (metSolicitud.getVigenciaHasta() != null) {
            disVigenciaHasta = true;
        }

        if (metSolicitud.getDirAlojamiento() == null) {
            disDirAlojamiento = false;
        }
        if (metSolicitud.getDirAlojamiento() != null) {
            disDirAlojamiento = true;
        }

        if (metSolicitud.getTelAlojamiento() == null) {
            disTelAlojamiento = false;
        }
        if (metSolicitud.getTelAlojamiento() != null) {
            disTelAlojamiento = true;
        }
    }

    /*ADJUNTAR ARCHIVO*/
    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public void upload() throws IOException {
        if (file != null) {
            FacesMessage message = new FacesMessage("Succesful", file.getFileName() + " is uploaded.");
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
            log.info("Archivo nulo");
        }
    }

    /*para adjuntar archivos*/
    private String codigoArc;

    public String getCodigoArc() {
        return codigoArc;
    }

    public void setCodigoArc(String codigoArc) {
        this.codigoArc = codigoArc;
    }

    public void definirCodigoArchivo() {
        metSoporteAdjunto.setCodigo(codigoArc);

        if ("CI".equals(codigoArc)) {
            metSoporteAdjunto.setDescripcion(Constantes.DES_CARTA_INCENTIVO);
        }

        if ("CM".equals(codigoArc)) {
            metSoporteAdjunto.setDescripcion(Constantes.DES_CERTIFICADO_MOT);
        }

        if ("CN".equals(codigoArc)) {
            metSoporteAdjunto.setDescripcion(Constantes.DES_CERTIFICADO_NOTAS);
        }

        if ("FC".equals(codigoArc)) {
            metSoporteAdjunto.setDescripcion(Constantes.DES_FOTOCOPIA_CEDULA);
        }

        if ("FP".equals(codigoArc)) {
            metSoporteAdjunto.setDescripcion(Constantes.DES_FOTOCOPIA_PASAPORTE);
        }
    }

    public void cargarDocumento(FileUploadEvent event) {
        System.out.println(" ---- Cargar Documento Entrante");

        if (codigoArc == null) {
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Por favor seleccione el tipo de archivo."));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Please select the file type"));
            }
        }

        if (codigoArc != null) {
            try {
                if (event.getFile() != null) {
                    FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
                    FacesContext.getCurrentInstance().addMessage(null, message);
                    FileMongoClient.getFileMongoClient().cargarArchivo(event.getFile());
                    String nombreArchivo = event.getFile().getFileName();
                    String idArchivoBd = FileMongoClient.getFileMongoClient().idArchivoUpload();
                    log.info("cargo documento con exito - ID: " + idArchivoBd);

                    metSoporteAdjunto.setId(idArchivoBd);
                    metSoporteAdjunto.setNombre(nombreArchivo);
                    metSoporteAdjunto.getMetSolicitud().setSolicitudPK(pkSolicitud.getSolicitudPK());
                    metSoporteAdjunto.setFechaCreado(fecha);
                    metSoporteAdjunto.setFechaModifido(fecha);
                    metSoporteAdjunto.setEstado(Constantes.ACTIVO);

                    log.info("ID solicitud: " + metSoporteAdjunto.getMetSolicitud().getSolicitudPK());
                    log.info("ID documento a cargar: " + metSoporteAdjunto.getCodigo());

                    String res;
                    res = servicios.guardarArchivo(metSoporteAdjunto);
                    if ((res.equalsIgnoreCase(Constantes.FAILED))) {
                        if ("es".equals(inicioMB.varIdioma)) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error al guardar los datos del archivo."));
                        }
                        if ("en".equals(inicioMB.varIdioma)) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error saving file data."));
                        }
                        log.info("ERROR¡  NO GUARDÓ DATOS DE ARCHIVO EN BD");
                    } else {
                        this.listSoporteAdjunto = servicios.selectListArchivos(pkSolicitud.getSolicitudPK());
                        log.info(" GUARDÓ DATOS DE ARCHIVO EN BD");
                    }
                } else {
                    System.out.println("Archivo nulo");
                }

            } catch (IOException e) {
                e.printStackTrace();
                if ("es".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error al cargar el archivo"));
                }
                if ("en".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error upload file."));
                }
            }
        }
    }

    public void cargarDistincion(FileUploadEvent event) {
        log.info("Entro a cargar documento distincion");
        try {
            if (event.getFile() != null) {
                FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
                FacesContext.getCurrentInstance().addMessage(null, message);
                FileMongoClient.getFileMongoClient().cargarArchivo(event.getFile());
                String nombreArchivo = event.getFile().getFileName();
                String idArchivoBd = FileMongoClient.getFileMongoClient().getIdArchivo();
                log.info("ID documento a cargar: " + idArchivoBd);
                metDistinciones.setCodigoArchivo(idArchivoBd);
                metDistinciones.setNombreArchivo(nombreArchivo);
            } else {
                System.out.println("Archivo nulo");
            }

        } catch (IOException e) {
            e.printStackTrace();
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error al cargar el archivo"));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error upload file."));
            }
        }
    }

    public StreamedContent getArchivoDistincion() throws IOException {
        StreamedContent miFile;
        log.info("ID para descargar archivo: " + selectedDistincion.getCodigoArchivo());
        File initialFile = FileMongoClient.getFileMongoClient().getArchivo(selectedDistincion.getCodigoArchivo());
        InputStream targetStream = new FileInputStream(initialFile);
        miFile = new DefaultStreamedContent(targetStream, "application/pdf", selectedDistincion.getNombreArchivo());
        return miFile;
    }

    public void guardarDatosPersonales() {
        if ("".equals(metInfUsuario.getNombres()) || ("".equals(metInfUsuario.getApellidos())) || ("".equals(metInfUsuario.getMetGenero().getPk()))
                || ("".equals(metInfUsuario.getMetTipoIdentificacion().getPk())) || ("".equals(metInfUsuario.getNumeroIdentificacion())) || ("".equals(metInfUsuario.getFechaNacimiento()))
                || ("".equals(metInfUsuario.getOtraCiudad())) || ("".equals(metInfUsuario.getMetPais().getPk())) || ("".equals(metInfUsuario.getNacionalidad()))
                || ("".equals(metInfUsuario.getDireccionResidencia())) || ("".equals(metInfUsuario.getTelCasa())) || ("".equals(metInfUsuario.getCelular()))
                || ("".equals(metInfUsuario.getEmailInstitucional()))) {

            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Existen campos sin completar. Por favor diligencie esos campos e inténtelo de nuevo."));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Empty fields. Please verify and try again."));
            }
        } else {
            String infoPersonal;
            infoPersonal = servicios.guardarInfPersonal(metInfUsuario);
            if ((infoPersonal.equalsIgnoreCase(Constantes.FAILED))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                log.info("Error al guardar datos personales");
            } else {
                if (siguientePagina == false) {
                    if ("es".equals(inicioMB.varIdioma)) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "informacion", "Información guardada con éxito"));
                    }
                    if ("en".equals(inicioMB.varIdioma)) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "information", "Information saved successfully."));
                    }
                }
                renderedPersonal = false;
                renderedInstOrigen = true;
                renderedAcademica = false;
                renderedMovilidad = false;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                this.siguientePagina = false;
            }
        }
    }

    public void guardarInstitucion() {
        if (selectOtraInstitucion == true) {
            if (("".equals(metSolicitud.getExisteConvenio())) || ("".equals(metOtraInstitucion.getNombreInstitucion())) || ("".equals(metOtraInstitucion.getMetPais().getPk()))
                    || ("".equals(metOtraInstitucion.getCiudadInstitucion())) || ("".equals(metOtraInstitucion.getNombreContacto())) || ("".equals(metOtraInstitucion.getCargoContacto()))
                    || ("".equals(metOtraInstitucion.getTelContacto())) || ("".equals(metOtraInstitucion.getNombreInstitucion())) || ("".equals(metOtraInstitucion.getEmailContacto()))) {
                if ("es".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Existen campos sin completar. Por favor diligencie esos campos e inténtelo de nuevo."));
                }
                if ("en".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Empty fields. Please verify and try again."));
                }
            } else {
                metSolicitud.setNombreContactoEntrante(null);
                metSolicitud.setTelContactoEntrante(null);
                metSolicitud.setEmailContactoEntrante(null);
                String existeOtraInstitucionG;
                existeOtraInstitucionG = servicios.existeOtraInstitucion(pkSolicitud.getSolicitudPK());
                if (!existeOtraInstitucionG.equalsIgnoreCase(Constantes.ZERO)) {
                    String actualizarOtraInstitucion;
                    actualizarOtraInstitucion = servicios.actualizarOtraInstitucion(metOtraInstitucion);
                    if ((actualizarOtraInstitucion.equalsIgnoreCase(Constantes.FAILED))) {
                    } else {
                        if (siguientePagina == false) {
                            if ("es".equals(inicioMB.varIdioma)) {
                                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "informacion", "Información guardada con éxito"));
                            }
                            if ("en".equals(inicioMB.varIdioma)) {
                                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "information", "Information saved successfully."));
                            }
                        }
                        renderedPersonal = false;
                        renderedInstOrigen = false;
                        renderedAcademica = true;
                        renderedMovilidad = false;
                        renderedPrfilMedico = false;
                        renderedDocAdjuntos = false;
                        this.siguientePagina = false;
                    }
                } else {
                    metOtraInstitucion.getMetSolicitud().setSolicitudPK(pkSolicitud.getSolicitudPK());
                    String infOtraInstitucion;
                    infOtraInstitucion = servicios.guardarOtraInstitucion(metOtraInstitucion);
                    if ((infOtraInstitucion.equalsIgnoreCase(Constantes.FAILED))) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                    } else {
                        String eliminarPkInstitucion;
                        eliminarPkInstitucion = servicios.eliminarPKInstitucion(pkSolicitud.getSolicitudPK());
                        if ((eliminarPkInstitucion.equalsIgnoreCase(Constantes.FAILED))) {
                            System.out.println("Error al eliminar pk de la Institución en la solicitud");
                        } else {
                            System.out.println("eliminó pk de la Institución en la solicitud");
                        }
                        if (siguientePagina == false) {
                            if ("es".equals(inicioMB.varIdioma)) {
                                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "informacion", "Información guardada con éxito"));
                            }
                            if ("en".equals(inicioMB.varIdioma)) {
                                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "information", "Information saved successfully."));
                            }
                        }
                        renderedPersonal = false;
                        renderedPrfilMedico = false;
                        renderedInstOrigen = false;
                        renderedAcademica = true;
                        renderedMovilidad = false;
                        this.siguientePagina = false;
                    }
                }
            }
        }

        if (selectOtraInstitucion == false) {
            if (("".equals(metSolicitud.getNombreContactoEntrante())) || ("".equals(metSolicitud.getTelContactoEntrante())) || ("".equals(metSolicitud.getEmailContactoEntrante()))
                    || ("".equals(metSolicitud.getMetInstitucionConvenio().getPk())) || ("".equals(metSolicitud.getExisteConvenio()))) {
                if ("es".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Existen campos sin completar. Por favor diligencie esos campos e inténtelo de nuevo."));
                }
                if ("en".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Empty fields. Please verify and try again."));
                }
            } else {
                System.out.println("**------- INSTITUCION ORIGEN PK");
                String resMetInstitucionOrigen;
                resMetInstitucionOrigen = servicios.updatePKMetInstConvenio(metSolicitud);
                if ((resMetInstitucionOrigen.equalsIgnoreCase(Constantes.FAILED))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                    log.info("ERROR¡ edu.konrad.movilidad.controller.solicitudEntranteMB.guardarInstitucionOrigen()");
                } else {
                    String borrarOtraInst;
                    borrarOtraInst = servicios.borrarOtraInstitucion(pkSolicitud.getSolicitudPK());
                    if ((borrarOtraInst.equalsIgnoreCase(Constantes.FAILED))) {
                        log.info("Error al eliminar otra Institucion");
                    } else {
                        log.info("eliminó Institución");
                    }
                    String resMetSolicitud;
                    resMetSolicitud = servicios.updateMetSolicitud(metSolicitud);
                    if ((resMetSolicitud.equalsIgnoreCase(Constantes.FAILED))) {
                        log.info("ERROR¡ edu.konrad.movilidad.controller.solicitudEntranteMB.guardarInstitucionOrigen()");
                    } else {
                    }
                    if (siguientePagina == false) {
                        if ("es".equals(inicioMB.varIdioma)) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "informacion", "Información guardada con éxito"));
                        }
                        if ("en".equals(inicioMB.varIdioma)) {
                            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "information", "Information saved successfully."));
                        }
                    }
                    renderedPersonal = false;
                    renderedPrfilMedico = false;
                    renderedInstOrigen = false;
                    renderedAcademica = true;
                    renderedMovilidad = false;
                }
            }
        }
    }

    public void guardarDistincion() {
        if ((("".equals(metDistinciones.getAnio())) && (!"".equals(metDistinciones.getDescripcion())))
                || ((!"".equals(metDistinciones.getAnio())) && ("".equals(metDistinciones.getDescripcion())))
                || (("".equals(metDistinciones.getAnio())) && ("".equals(metDistinciones.getDescripcion())))) {
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Debe ingresar un año y una descripción del logro."));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Empty fields. Year and achievement are required."));
            }
        } else {
            metDistinciones.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
            log.info("Cargar archivo de distincion");
            log.info("Nombre de archivo a cargar: " + metDistinciones.getNombreArchivo());
            log.info("Codigo de archivo a cargar: " + metDistinciones.getCodigoArchivo());

            String resMetDistinciones;
            resMetDistinciones = servicios.insertDistinciones(metDistinciones);
            if ((resMetDistinciones.equalsIgnoreCase(Constantes.FAILED))) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                log.info("Error a guardar Distincion");
            } else {
                this.metDistinciones.setAnio(null);
                this.metDistinciones.setDescripcion(null);
                this.metDistinciones.setCodigoArchivo(null);
                this.metDistinciones.setNombreArchivo(null);
                renderedDistinciones = true;
                this.listaDistinciones = servicios.obtenerDistincion(pkSolicitud.getSolicitudPK());
                log.info("Luego de cargar archivo distincion");
                log.info("Nombre de archivo a cargar: " + metDistinciones.getNombreArchivo());
                log.info("Codigo de archivo a cargar: " + metDistinciones.getCodigoArchivo());
            }
        }
    }

    public void crearSolicitud() {
        this.tabFormEntrante = true;
        this.botonCrearSolicitud = false;
        String res;
        usuarioSolPK.getMetInfUsuario().setUsuarioPK(metInfUsuario.getUsuarioPK());
        usuarioSolPK.getMetTipoSolicitud().setPk(metInfUsuario.getMetTipoSolicitud().getPk());

        res = servicios.insertNewSolicitud(usuarioSolPK);
        if (res.equalsIgnoreCase(Constantes.FAILED)) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
            log.info("Error al crear solicitud.");
        } else {

            this.usuarioSolPK = servicios.consultarPKSolicitud(metInfUsuario.getUsuarioPK());
            metEstadoSolicitud.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
            metEstadoSolicitud.getMetTipoEstadoSolicitud().setPk(Constantes.ESTADOSOL_GUARDADA);
            //this.metSolicitud.setConsecutivo("EKL_2018_" + "1");

            String resEstadoSolicitud;
            resEstadoSolicitud = servicios.insertEstadoSolicitud(metEstadoSolicitud);
            if (resEstadoSolicitud.equalsIgnoreCase(Constantes.FAILED)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                log.info("Error al crear solicitud.");
            }

            metObservacion.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
            String resObservacion;
            resObservacion = servicios.insertPKObservacion(metObservacion);
            if (resObservacion.equalsIgnoreCase(Constantes.FAILED)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                log.info("Error al crear solicitud.");
            }

            metContactoEmergencia.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
            String resContEmergencia;
            resContEmergencia = servicios.insertPKContactoEmergencia(metContactoEmergencia);
            if (resContEmergencia.equalsIgnoreCase(Constantes.FAILED)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error1", "Error"));
                log.info("Error al crear solicitud.");

            }
        }
    }

    public void guardarAcuerdoAprendizaje() {

        if ((listaInfoCursoKL.size() < 1)) {
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Por favor seleccione los cursos que desearía tomar durante la movilidad entrante en la Konrad Lorenz."));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Please include courses proposal for the exchange period at Konrad Lorenz."));
            }
        } else {
            if ((listaInfoCursoKLAlt.size() < 2)) {
                if ("es".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Por favor seleccione dos asignaturas en caso de que alguna de las anteriores no pueda ser ofertada o haya cruce de horarios."));
                }
                if ("en".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Please include two alternative courses in case there are schedule problems."));
                }
            } else {
                if (("".equals(metSolicitud.getMetProgramaKl().getPk()))
                        || ("".equals(metSolicitud.getMetTipoMovilidad().getPk()))
                        || (metSolicitud.getFechaInicioMovilidad() == null)
                        || (metSolicitud.getFechaFinMovilidad() == null)
                        || ("".equals(metSolicitud.getMotivacionEstudiante()))) {
                    if ("es".equals(inicioMB.varIdioma)) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Existen campos sin completar. Por favor diligencie esos campos e inténtelo de nuevo."));
                    }
                    if ("en".equals(inicioMB.varIdioma)) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Empty fields. Please verify and try again."));
                    }
                } else {
                    calcularDuracionMovilidad();
                    this.usuarioSolPK = servicios.consultarPKSolicitud(metInfUsuario.getUsuarioPK());
                    metSolicitud.setSolicitudPK(usuarioSolPK.getSolicitudPK());
                    guardarTipoMovilidad();
                    guardardDatosSolicitud();
                    renderedPersonal = false;
                    renderedInstOrigen = false;
                    renderedAcademica = false;
                    renderedMovilidad = false;
                    renderedPrfilMedico = true;
                    renderedDocAdjuntos = false;
                }
            }
        }
    }

    public void guadarProgramaAcademico() {
        if (("".equals(metSolicitud.getMetNivelEstEntrante().getPk()))
                || ("".equals(metSolicitud.getOtraFacultadEntrante()))
                || ("".equals(metSolicitud.getSemestreEntrante()))
                || ("".equals(metSolicitud.getOtroProgramaInstConvenio()))
                || ("0.0".equals(metSolicitud.getPromedioAcumulado()))) {
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Existen campos sin completar. Por favor diligencie esos campos e inténtelo de nuevo."));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Empty fields. Please verify and try again."));
            }
        } else {
            if (("false".equals(idiomaEspanol.getIdiomaNativo())) && ("false".equals(idiomaIngles.getIdiomaNativo())) && ("false".equals(idiomaOtro.getIdiomaNativo()))) {
                if ("es".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Debe seleccionar un idioma nativo"));
                }
                if ("en".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Select a native language"));
                }
            } else {
                String resMetNivelEstudiopk;
                if (metSolicitud.getMetNivelEstEntrante().getPk() != null) {
                    resMetNivelEstudiopk = servicios.updatePKMetNivelEstudio(metSolicitud);
                    if ((resMetNivelEstudiopk.equalsIgnoreCase(Constantes.FAILED))) {
                        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                        log.info("Error al registrar nivel de estudio");
                    }
                }
                guardarInfoIdioma();
                guardardDatosSolicitud();
                renderedPersonal = false;
                renderedInstOrigen = false;
                renderedAcademica = false;
                renderedMovilidad = true;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                this.siguientePagina = false;
            }
        }
    }

    public void guardardDatosSolicitud() {
        String resMetSolicitud;
        resMetSolicitud = servicios.updateMetSolicitud(metSolicitud);
        if ((resMetSolicitud.equalsIgnoreCase(Constantes.FAILED))) {
            log.info("Error al actualizar los datos de la solicitud");
        } else {
            if (siguientePagina == false) {
                if ("es".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "informacion", "Información guardada con éxito"));
                }
                if ("en".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "information", "Information saved successfully."));
                }
            }
        }
    }

    public void guardarInfoIdioma() {
        if (("false".equals(idiomaEspanol.getIdiomaNativo())) && ("false".equals(idiomaIngles.getIdiomaNativo())) && ("false".equals(idiomaOtro.getIdiomaNativo()))) {
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Debe seleccionar un idioma nativo"));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Select a native language"));
            }
        } else {
            String metInfIdiomaEsp;
            String metInfIdiomaIng;
            String metInfIdiomaOtr;

            idiomaEspanol.setCodigo(Constantes.COD_IDIOMA_ESPANIOL);
            idiomaEspanol.setNamDescripcion(Constantes.DES_IDIOMA_ESPANIOL);
            idiomaEspanol.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
            metInfIdiomaEsp = servicios.validarInfoIdioma(idiomaEspanol);
            if (metInfIdiomaEsp.equalsIgnoreCase(Constantes.ZERO)) {
                String resInfIdioma;
                resInfIdioma = servicios.insertMetInfoIdiomas(idiomaEspanol);
                if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                    log.info("Error al registrar idioma español");
                }
            } else {
                String resInfIdioma;
                resInfIdioma = servicios.updateInfoIdioma(idiomaEspanol);
                if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                    log.info("Error al actualizar idioma español");
                }
            }

            idiomaIngles.setCodigo(Constantes.COD_IDIOMA_INGLES);
            idiomaIngles.setNamDescripcion(Constantes.DES_IDIOMA_INGLES);
            idiomaIngles.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
            metInfIdiomaIng = servicios.validarInfoIdioma(idiomaIngles);
            if (metInfIdiomaIng.equalsIgnoreCase(Constantes.ZERO)) {
                String resInfIdioma;
                resInfIdioma = servicios.insertMetInfoIdiomas(idiomaIngles);
                if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                    log.info("Error al registrar idioma ingles");
                }
            } else {
                String resInfIdioma;
                resInfIdioma = servicios.updateInfoIdioma(idiomaIngles);
                if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                    log.info("Error al actualizar idioma ingles");
                }
            }

            idiomaOtro.setCodigo(Constantes.COD_IDIOMA_OTRO);
            idiomaOtro.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
            metInfIdiomaOtr = servicios.validarInfoIdioma(idiomaOtro);
            if (metInfIdiomaOtr.equalsIgnoreCase(Constantes.ZERO)) {
                String resInfIdioma;
                resInfIdioma = servicios.insertMetInfoIdiomas(idiomaOtro);
                if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                    log.info("Error al registrar idioma otro");
                }
            } else {
                String resInfIdioma;
                resInfIdioma = servicios.updateInfoIdioma(idiomaOtro);
                if ((resInfIdioma.equalsIgnoreCase(Constantes.FAILED))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                    log.info("Error al actualizar idioma otro");
                }
            }
        }
    }

    public void guardarTipoMovilidad() {
        String resTipoMovilidad;
        resTipoMovilidad = servicios.updatePKMetTipoMovilidad(metSolicitud);
        if ((resTipoMovilidad.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
            log.info("Error al rsgistrar el tipo de movilidad");
        } else {
        }
    }

    public void guardarContactoEmergencia() {
        if (("Si".equals(metSolicitud.getTieneAlergia())) && ("".equals(metSolicitud.getAlergias()))
                || (("Si".equals(metSolicitud.getTieneEnfermedades())) && ("".equals(metSolicitud.getEnfermedades())))
                || (("Si".equals(metSolicitud.getTieneMedicamento())) && ("".equals(metSolicitud.getMedicamentos())))
                || (("Si".equals(metSolicitud.getTieneRestriccionAlm())) && ("".equals(metSolicitud.getRestriccionAlimentaria())))
                || (("Yes".equals(metSolicitud.getTieneAlergia())) && ("".equals(metSolicitud.getAlergias()))
                || (("Yes".equals(metSolicitud.getTieneEnfermedades())) && ("".equals(metSolicitud.getEnfermedades())))
                || (("Yes".equals(metSolicitud.getTieneMedicamento())) && ("".equals(metSolicitud.getMedicamentos())))
                || (("Yes".equals(metSolicitud.getTieneRestriccionAlm())) && ("".equals(metSolicitud.getRestriccionAlimentaria()))))) {
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Indique cuáles"));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning!", "Indicate which"));
            }
        } else {
            this.usuarioSolPK = servicios.consultarPKSolicitud(metInfUsuario.getUsuarioPK());
            metSolicitud.setSolicitudPK(usuarioSolPK.getSolicitudPK());
            String resContactoEmergencia;
            metContactoEmergencia.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
            resContactoEmergencia = servicios.updateContactoEmergencia(metContactoEmergencia);
            if ((resContactoEmergencia.equalsIgnoreCase(Constantes.FAILED))) {
            } else {
                String guardarRH;
                guardarRH = servicios.updateRHusuario(metInfUsuario);
                if ((guardarRH.equalsIgnoreCase(Constantes.FAILED))) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
                    log.info("Error al guardar datos de contacto");
                } else {
                    String resMetSolicitud;
                    resMetSolicitud = servicios.updateMetSolicitud(metSolicitud);
                    if ((resMetSolicitud.equalsIgnoreCase(Constantes.FAILED))) {
                        log.info("ERROR no guardó datos medicos");
                    } else {
                        if (siguientePagina == false) {
                            if ("es".equals(inicioMB.varIdioma)) {
                                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "informacion", "Información guardada con éxito"));
                            }
                            if ("en".equals(inicioMB.varIdioma)) {
                                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "information", "Information saved successfully."));
                            }
                        }
                        renderedPersonal = false;
                        renderedInstOrigen = false;
                        renderedAcademica = false;
                        renderedMovilidad = false;
                        renderedPrfilMedico = false;
                        renderedDocAdjuntos = true;
                        this.siguientePagina = false;
                    }
                }
            }
        }
    }

    public void deshabilitarIdiomaEsp() {
        if ("false".equals(idiomaEspanol.getIdiomaNativo())) {
            disabledIdiomaEsp = false;
            idiomaNativoIng = false;
            idiomaNativoOtr = false;
            idiomaEspanol.setPorcentajeEscribe(null);
            idiomaEspanol.setPorcentajeHabla(null);
            idiomaEspanol.setPorcentajeLee(null);
        }
        if ("true".equals(idiomaEspanol.getIdiomaNativo())) {
            disabledIdiomaEsp = true;
            idiomaNativoIng = true;
            idiomaNativoOtr = true;
            idiomaEspanol.setPorcentajeEscribe("100%");
            idiomaEspanol.setPorcentajeHabla("100%");
            idiomaEspanol.setPorcentajeLee("100%");
            idiomaIngles.setIdiomaNativo("N");
            idiomaOtro.setIdiomaNativo("N");
        }
    }

    public void deshabilitarIdiomaIng() {
        if ("false".equals(idiomaIngles.getIdiomaNativo())) {
            disabledIdiomaIng = false;
            idiomaNativoEsp = false;
            idiomaNativoOtr = false;
            idiomaIngles.setPorcentajeEscribe(null);
            idiomaIngles.setPorcentajeHabla(null);
            idiomaIngles.setPorcentajeLee(null);
        }
        if ("true".equals(idiomaIngles.getIdiomaNativo())) {
            disabledIdiomaIng = true;
            idiomaNativoEsp = true;
            idiomaNativoOtr = true;
            idiomaIngles.setPorcentajeEscribe("100%");
            idiomaIngles.setPorcentajeHabla("100%");
            idiomaIngles.setPorcentajeLee("100%");
            idiomaEspanol.setIdiomaNativo("N");
            idiomaOtro.setIdiomaNativo("N");
        }
    }

    public void deshabilitarIdiomaOtro() {
        if ("false".equals(idiomaOtro.getIdiomaNativo())) {
            disabledIdiomaOtr = false;
            idiomaNativoIng = false;
            idiomaNativoEsp = false;
            idiomaOtro.setPorcentajeEscribe(null);
            idiomaOtro.setPorcentajeHabla(null);
            idiomaOtro.setPorcentajeLee(null);
        }
        if ("true".equals(idiomaOtro.getIdiomaNativo())) {
            disabledIdiomaOtr = true;
            idiomaNativoIng = true;
            idiomaNativoEsp = true;
            idiomaOtro.setPorcentajeEscribe("100%");
            idiomaOtro.setPorcentajeHabla("100%");
            idiomaOtro.setPorcentajeLee("100%");
            idiomaIngles.setIdiomaNativo("N");
            idiomaEspanol.setIdiomaNativo("N");
        }
    }

    public void borrarDistincion() {
        String resMetDistincion;
        resMetDistincion = servicios.deleteDistincion(selectedDistincion.getPk());
        if ((resMetDistincion.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
            log.info("Error al eliminar campo distinción");
        } else {
            renderedDistinciones = true;
            this.listaDistinciones = servicios.obtenerDistincion(pkSolicitud.getSolicitudPK());
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Dato eliminado con éxito."));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Data removed successfully."));
            }
        }
    }

    public void contDistincion() {
        String contDistincion;
        contDistincion = servicios.validaDistincion(pkSolicitud.getSolicitudPK());
        if (!contDistincion.equalsIgnoreCase(Constantes.ZERO)) {
            renderedDistinciones = true;
        } else {
            renderedDistinciones = false;
        }
    }

    public void otroParentesco() {
        if ("11".equals(metContactoEmergencia.getMetParentesco().getPk())) {
            renderedOtroParentesco = true;
        } else {
            renderedOtroParentesco = false;
        }
    }

    public void calcularDuracionMovilidad() {
        if ((metSolicitud.getFechaInicioMovilidad() != null) && (metSolicitud.getFechaFinMovilidad() != null)) {
            Date fechaInicial = metSolicitud.getFechaInicioMovilidad();
            Date fechaFinal = metSolicitud.getFechaFinMovilidad();
            diasMovilidad = (int) ((fechaFinal.getTime() - fechaInicial.getTime()) / 86400000);
            mDuracionMovilidad = (int) (diasMovilidad / 30.44);
            dDuracionMovilidad = (int) (diasMovilidad - mDuracionMovilidad * 30);
            metSolicitud.setDuracionMovilidad(mDuracionMovilidad + " meses, " + dDuracionMovilidad + " días");
            renderedDuracionMovilidad = true;
        }
    }

    public void guardarInfCursoKL() {
        metInfoCursoKL.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
        metInfoCursoKL.setEstado(Constantes.NOALTERNATIVO);
        String resMetCurssosKL;
        resMetCurssosKL = servicios.insertInfoCursoKL(metInfoCursoKL);
        if ((resMetCurssosKL.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
            log.info("Error al guardar curso");
        } else {
            this.metInfoCursoKL.getMetCursoKL().setPk(null);
            renderedCursosKL = true;
            this.listaInfoCursoKL = servicios.obtenerInfoCursoKL(pkSolicitud.getSolicitudPK());
        }
    }

    public void guardarInfCursoKLAlt() {
        metInfoCursoKLAlt.getMetSolicitud().setSolicitudPK(usuarioSolPK.getSolicitudPK());
        metInfoCursoKLAlt.setEstado(Constantes.ALTERNATIVO);
        String resMetCurssosKL;
        resMetCurssosKL = servicios.insertInfoCursoKLAlt(metInfoCursoKLAlt);
        if ((resMetCurssosKL.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
            log.info("Error al guardar curso alternativo");
        } else {
            this.metInfoCursoKLAlt.getMetCursoKL().setPk(null);
            renderedCursosKLAlt = true;
            this.listaInfoCursoKLAlt = servicios.obtenerInfoCursoKLAlt(pkSolicitud.getSolicitudPK());
        }
    }

    public void borrarInfCursoKL() {
        String resMetInfoCurso;
        resMetInfoCurso = servicios.deleteInfoCurso(selectedInfoCursoKL.getPk());
        if ((resMetInfoCurso.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
            log.info("Error al eliminar curso KL");
        } else {
            renderedCursosKL = false;
            renderedCursosKL = true;
            this.listaInfoCursoKL = servicios.obtenerInfoCursoKL(pkSolicitud.getSolicitudPK());
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Dato eliminado con éxito."));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Data removed successfully."));
            }
        }
    }

    public void borrarInfCursoAltKL() {
        String resMetInfoCurso;
        resMetInfoCurso = servicios.deleteInfoCurso(selectedInfoCursoKLAlt.getPk());
        if ((resMetInfoCurso.equalsIgnoreCase(Constantes.FAILED))) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error"));
            log.info("Error al eliminar curso alterno");
        } else {
            renderedCursosKLAlt = false;
            renderedCursosKLAlt = true;
            this.listaInfoCursoKLAlt = servicios.obtenerInfoCursoKLAlt(pkSolicitud.getSolicitudPK());
            this.listaInfoCursoKL = servicios.obtenerInfoCursoKL(pkSolicitud.getSolicitudPK());
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Dato eliminado con éxito."));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Data removed successfully."));
            }
        }
    }

    public String irSolicitud() {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("seleccionEstudiante", selectedSolicitud);
        return "/vistas/consultaSolicitudEntrante.xhtml?faces-redirect=true";
    }

    //---------------------------------------------------------------------------------------------------------//
    public void definirEstadoSolicitud() {
        metEstadoSolicitud = servicios.obtenerIDEstadoSolicitud(pkSolicitud.getSolicitudPK());
    }

    public void otroNivelEducacion() {
        if (selecOtroNivelEdu == true) {
            disNivelEducacion = true;
            disOtroNivelEducacion = false;
            metSolicitud.getMetNivelEstEntrante().setPk(null);
        }

        if (selecOtroNivelEdu == false) {
            disNivelEducacion = false;
            disOtroNivelEducacion = true;
            metSolicitud.setOtroNivelEstudio(null);
        }
    }

    public StreamedContent getArchivo() throws IOException {
        StreamedContent miFile;
        log.info("ID del acrhivo para descarga: " + selectArchivo.getId());
        File initialFile = FileMongoClient.getFileMongoClient().getArchivo(selectArchivo.getId());
        InputStream targetStream = new FileInputStream(initialFile);
        miFile = new DefaultStreamedContent(targetStream, "application/pdf", selectArchivo.getNombre());
        return miFile;
    }

    public void definirIdioma() {
        System.out.println("IDIOMA EN SOLICITUD 2" + inicioMB.varIdioma);
        if ("en".equals(inicioMB.varIdioma)) {
            campoIngles = true;
            campoEspaniol = false;
            renderTituloEsp = false;
            renderTituloIng = true;
        }

        if ("es".equals(inicioMB.varIdioma)) {
            campoIngles = false;
            campoEspaniol = true;
            renderTituloEsp = true;
            renderTituloIng = false;
        }
    }

    public void logout() {
        log.info("Logout de la sesion: " + ((inicioMB.getUsuarioSesion().getNamUsuario() == null) ? inicioMB.getUsuarioSesion().getNamUsuario() : ""));
        ExternalContext ctx = FacesContext.getCurrentInstance().getExternalContext();
        String ctxPath = ((ServletContext) ctx.getContext()).getContextPath();
        try {
            ((HttpSession) ctx.getSession(false)).invalidate();
            ctx.redirect(ctxPath + "/faces/index.xhtml?faces-redirect=true");
        } catch (IOException ex) {
            log.info("Error" + ex.getMessage());
        }
    }

    public void btnSiguiente(int siguiente) {
        switch (siguiente) {
            case 1:
                this.siguientePagina = true;
                guardarDatosPersonales();
                break;

            case 2:
                this.siguientePagina = true;
                guardarInstitucion();
                break;

            case 3:
                this.siguientePagina = true;
                guadarProgramaAcademico();
                break;

            case 4:
                this.siguientePagina = true;
                guardarAcuerdoAprendizaje();
                break;

            case 5:
                this.siguientePagina = true;
                guardarContactoEmergencia();
                break;
        }
    }

    public void btnAtras(int atras) {
        switch (atras) {
            case 2:
                this.siguientePagina = false;
                renderedPersonal = true;
                renderedInstOrigen = false;
                renderedAcademica = false;
                renderedMovilidad = false;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                break;

            case 3:
                this.siguientePagina = false;
                renderedPersonal = false;
                renderedInstOrigen = true;
                renderedAcademica = false;
                renderedMovilidad = false;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                break;

            case 4:
                this.siguientePagina = false;
                renderedPersonal = false;
                renderedInstOrigen = false;
                renderedAcademica = true;
                renderedMovilidad = false;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                break;

            case 5:
                this.siguientePagina = false;
                renderedPersonal = false;
                renderedInstOrigen = false;
                renderedAcademica = false;
                renderedMovilidad = true;
                renderedPrfilMedico = false;
                renderedDocAdjuntos = false;
                break;

            case 6:
                this.siguientePagina = false;
                renderedPersonal = false;
                renderedInstOrigen = false;
                renderedAcademica = false;
                renderedMovilidad = false;
                renderedPrfilMedico = true;
                renderedDocAdjuntos = false;
                break;
        }

    }

    public void enviarReporteMovilidad() {
        SendEmail enviarCorreo = new SendEmail();
        if (this.getAdjuntos() == null) {
            this.setAdjuntos(new ArrayList<String>());
        } else {
            if (this.getAdjuntos().size() > 0) {
                this.getAdjuntos().clear();
            }
        }

        if (this.getReceptores() == null) {
            this.setReceptores(new ArrayList<String>());
        } else {
            if (this.getReceptores().size() > 0) {
                this.getReceptores().clear();
            }
        }

        if (this.getReceptoresCopia() == null) {
            this.setReceptoresCopia(new ArrayList<String>());
        } else {
            if (this.getReceptoresCopia().size() > 0) {
                this.getReceptoresCopia().clear();
            }
        }

        try {
            Map parameters = new HashMap();
            parameters.put("USUARIO_ESTUDIANTE", inicioMB.getUsuarioSesion().getNamUsuario());
            GenerarFile generadorFile = new GenerarFile();

            if ("es".equals(inicioMB.varIdioma)) {
                File archivoGenerado = generadorFile.processRequest(parameters, "ReporteEntrante");
                this.getAdjuntos().add(archivoGenerado.getPath());
                this.asunto = "Formato de Solicitud de Movilidad - Fundacion Universitaria Konrad Lorenz";
            }

            if ("en".equals(inicioMB.varIdioma)) {
                File archivoGenerado = generadorFile.processRequest(parameters, "ReporteEntrante_english");
                this.getAdjuntos().add(archivoGenerado.getPath());
                this.asunto = "Incoming Mobility Form - Konrad Lorenz University Foundation";
            }

            this.listSoporteAdjunto = servicios.selectListArchivos(pkSolicitud.getSolicitudPK());
            File fileAdjuntar;
            for (int i = 0; i < listSoporteAdjunto.size(); i++) {
                MetSoporteAdjunto soporteAdjuntar = listSoporteAdjunto.get(i);
                fileAdjuntar = FileMongoClient.getFileMongoAdjuntar().leerArchivo(soporteAdjuntar.getId());
                log.info("Adjuntó archivo: " + soporteAdjuntar.getNombre() + " - ID: " + soporteAdjuntar.getId());
                this.getAdjuntos().add(fileAdjuntar.getPath());
            }

            this.servidorCorreo = "smtp.office365.com";
            this.puertoCorreo = "587";
            this.emisor = "movilidad@konradlorenz.edu.co";
            this.contrasena = "Konrad2018";

            this.getReceptores().add(metInfUsuario.getEmailInstitucional());

            if (metSolicitud.getEmailContactoEntrante() != null) {
                this.getReceptores().add(metSolicitud.getEmailContactoEntrante());
            }

            if (metOtraInstitucion.getEmailContacto() != null) {
                this.getReceptores().add(metOtraInstitucion.getEmailContacto());
            }

            this.getReceptoresCopia().add("movilidad@konradlorenz.edu.co");
            this.getReceptoresCopia().add("cancilleria@konradlorenz.edu.co");
            if ("en".equals(inicioMB.varIdioma)) {
                this.mensaje = ("<p style=\"text-align: justify;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: 'Calibri Light',sans-serif;\">Dear <span>" + metInfUsuario.getNombres() + " " + metInfUsuario.getApellidos() + "</span></span></p>\n"
                        + "<p style=\"text-align: justify;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: 'Calibri Light',sans-serif;\">Well done! We have received your application to the <em>Incoming Mobility Programme </em>at <em>Fundaci&oacute;n Universitaria Konrad Lorenz</em> (Bogot&aacute;, Colombia), your application number is&nbsp;<strong><span>" + metSolicitud.getConsecutivo() + "</span></strong><span style=\"color: #c00000;\">. </span></span></p>\n"
                        + "<p style=\"text-align: justify;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: 'Calibri Light',sans-serif;\">Your home institution has been also notified about this application. To take into account your application, the International Relations Office (IRO) at your home institution must submit us your approval. This confirmation must be sent the next 8 working days, replying this email. Contact your IRO and make sure this confirmation be sent on time. </span></p>\n"
                        + "<p style=\"text-align: justify;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: 'Calibri Light',sans-serif;\">Thank your for your interest in &ldquo;Fundaci&oacute;n Universitaria Konrad Lorenz&rdquo;, in coming days you will receive a response of your mobility process. If you have any questions please contact us to: </span><a href=\"mailto:cancilleria@konradlorenz.edu.co\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: 'Calibri Light',sans-serif;\">cancilleria@konradlorenz.edu.co</span></a></p>");
            }

            if ("es".equals(inicioMB.varIdioma)) {
                this.mensaje = ("<p style=\"text-align: justify;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: 'Calibri Light',sans-serif;\">Estimado(a) <span>" + metInfUsuario.getNombres() + " " + metInfUsuario.getApellidos() + "</span></span></p>\n"
                        + "<p style=\"text-align: justify;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: 'Calibri Light',sans-serif;\">&iexcl;Bien hecho! Hemos recibido tu postulaci&oacute;n al <em>Programa de Movilidad Konradista</em>, tu n&uacute;mero de solicitud ser&aacute; <strong><span>" + metSolicitud.getConsecutivo() + "</span></strong><span style=\"color: #c00000;\">. </span></span></p>\n"
                        + "<p style=\"text-align: justify;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: 'Calibri Light',sans-serif;\">Tu Universidad tambi&eacute;n ha sido notificada de esta postulaci&oacute;n. Para hacerla efectiva, el encargado de la Oficina de Relaciones Internacionales (ORI) debe confirmarnos que cuentas con autorizaci&oacute;n para llevar a cabo el proceso de movilidad, dentro de los siguientes 8 d&iacute;as h&aacute;biles respondiendo a este correo. Contacta a tu ORI y aseg&uacute;rate de que nos enviar&aacute;n este aval. </span></p>\n"
                        + "<p style=\"text-align: justify;\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: 'Calibri Light',sans-serif;\">Agradecemos tu inter&eacute;s en la <em>Fundaci&oacute;n Universitaria Konrad Lorenz</em>, en los pr&oacute;ximos d&iacute;as recibir&aacute;s respuesta sobre tu movilidad. Si tienes dudas durante el proceso escr&iacute;benos a: </span><a href=\"mailto:cancilleria@konradlorenz.edu.co\"><span style=\"font-size: 12.0pt; line-height: 107%; font-family: 'Calibri Light',sans-serif;\">cancilleria@konradlorenz.edu.co</span></a></p>");
            }
            this.tls = "true";
            this.authentication = "true";
            enviarCorreo.envia(servidorCorreo, puertoCorreo, emisor, asunto, receptores, receptoresCopia, mensaje, adjuntos, contrasena, tls, authentication);

        } catch (Exception e) {
            e.printStackTrace();
            log.info(e.getMessage());
        }
    }

    public void validaInstitucionOrigen() {
        existeOtraInsti = servicios.validaOtraInstitucion(pkSolicitud.getSolicitudPK());

        if (("0".equals(existeOtraInsti))
                && (metSolicitud.getMetInstitucionConvenio().getPk() == null)) {
            this.selectOtraInstitucion = false;
            this.renderedPOtraInst = false;
            this.disableInstitucion = false;
            this.renderedInsitucionOrigen = true;
        }

        if (!"0".equals(existeOtraInsti)) {
            this.selectOtraInstitucion = true;
            this.renderedPOtraInst = true;
            this.renderedInsitucionOrigen = false;
            this.disableInstitucion = true;
        }

        if ((metSolicitud.getMetInstitucionConvenio().getPk() != null)) {
            this.selectOtraInstitucion = false;
            this.renderedPOtraInst = false;
            this.disableInstitucion = false;
            this.renderedInsitucionOrigen = true;
        }
    }

    public void enviarSolicitud() {
        String envioSolicitud;
        envioSolicitud = servicios.insertFechaEnvio(metSolicitud);
        if (envioSolicitud.equalsIgnoreCase(Constantes.FAILED)) {
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error al enviar la solicitud, intente de nuevo."));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error sending the request, try again."));
            }
        } else {
            String res;
            this.usuarioSolPK = servicios.consultarPKSolicitud(metInfUsuario.getUsuarioPK());
            metSolicitud.setSolicitudPK(usuarioSolPK.getSolicitudPK());
            metEstadoSolicitud.getMetTipoEstadoSolicitud().setPk(Constantes.ESTADOSOL_SINREVISAR);
            metEstadoSolicitud.getMetSolicitud().setSolicitudPK(metSolicitud.getSolicitudPK());
            res = servicios.cambiarEstadoSolicitud(metEstadoSolicitud);
            if (res.equalsIgnoreCase(Constantes.FAILED)) {
                if ("es".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error al enviar la solicitud, intente de nuevo."));
                }
                if ("en".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error sending the request, try again."));
                }
            } else {
                HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
                Map parameters = new HashMap();
                parameters.put("USUARIO_ESTUDIANTE", inicioMB.getUsuarioSesion().getNamUsuario());
                if ("en".equals(inicioMB.varIdioma)) {
                    session.setAttribute("reporte", "ReporteEntrante_english");
                }
                if ("es".equals(inicioMB.varIdioma)) {
                    session.setAttribute("reporte", "ReporteEntrante");
                }
                session.setAttribute("parametros", parameters);
                RequestContext.getCurrentInstance().execute("PF('pdReporteEntrante').show()");
                enviarReporteMovilidad();
                if ("es".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "info!", "Solicitud enviada con éxito."));
                }
                if ("en".equals(inicioMB.varIdioma)) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "info!", "Request sent successfully."));
                }
            }

        }
    }

    public void validarCamposInstOrigen() {
        if (selectOtraInstitucion == true) {
            if (("".equals(metSolicitud.getExisteConvenio())) || ("".equals(metOtraInstitucion.getNombreInstitucion())) || ("".equals(metOtraInstitucion.getMetPais().getPk()))
                    || ("".equals(metOtraInstitucion.getCiudadInstitucion())) || ("".equals(metOtraInstitucion.getNombreContacto())) || ("".equals(metOtraInstitucion.getCargoContacto()))
                    || ("".equals(metOtraInstitucion.getTelContacto())) || ("".equals(metOtraInstitucion.getNombreInstitucion())) || ("".equals(metOtraInstitucion.getEmailContacto()))) {
                validarModuloInstOrigen = true;
            } else {
                validarModuloInstOrigen = false;
            }
        }

        if (selectOtraInstitucion == false) {
            if (("".equals(metSolicitud.getNombreContactoEntrante())) || ("".equals(metSolicitud.getTelContactoEntrante())) || ("".equals(metSolicitud.getEmailContactoEntrante()))
                    || ("".equals(metSolicitud.getMetInstitucionConvenio().getPk())) || ("".equals(metSolicitud.getExisteConvenio()))) {
                validarModuloInstOrigen = true;
            } else {
                validarModuloInstOrigen = false;
            }
        }
    }

    public void validarCamposAcuerdoAprendizaje() {
        if (((listaInfoCursoKL.size() < 1) || (listaInfoCursoKL == null))
                || ((listaInfoCursoKLAlt.size() < 2) || (listaInfoCursoKLAlt == null))
                || ("".equals(metSolicitud.getMetProgramaKl().getPk()))
                || ("".equals(metSolicitud.getMetTipoMovilidad().getPk()))
                || (metSolicitud.getFechaInicioMovilidad() == null)
                || (metSolicitud.getFechaFinMovilidad() == null)
                || ("".equals(metSolicitud.getMotivacionEstudiante()))) {
            validarModuloAcuerdoApren = true;
        } else {
            validarModuloAcuerdoApren = false;
        }
    }

    public void validarCampoAdjuntarArchivo() {
        if (listSoporteAdjunto.size() < 2) {
            validarModuloArchivo = true;
        } else {
            validarModuloArchivo = false;
        }
    }

    public String onIngresar() {
        return "/vistas/solicitudEntranteConsultar.xhtml?faces-redirect=true";
    }

    public void eliminarSoporteAdj() {
        String soporteAdjunto;
        soporteAdjunto = servicios.deleteSoporteAdjunto(selectArchivo.getPk());
        if ((soporteAdjunto.equalsIgnoreCase(Constantes.FAILED))) {
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Error al eliminar el archivo."));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "File removed error"));
            }
            log.info("Error al eliminar el archivo");
        } else {
            this.listSoporteAdjunto = servicios.selectListArchivos(pkSolicitud.getSolicitudPK());
            if ("es".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "Archivo eliminado con éxito."));
            }
            if ("en".equals(inicioMB.varIdioma)) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info!", "File removed successfully."));
            }
            log.info("elimino el archivo");;
        }
    }

    public void MostrarListado() {
        Integer seleccionLista = null;
        switch (seleccionLista) {
            case 1:
                itemsDocAdjunto.add("Pasaporte");
                itemsDocAdjunto.add("Carta de motivación");
                itemsDocAdjunto.add("Certificado original de notas");
                itemsDocAdjunto.add("Solvencia económica");
                itemsDocAdjunto.add("Carta de compromiso");
                itemsDocAdjunto.add("Carta de aplicación");
                break;

            //Intercambio académico  
            case 2:
                itemsDocAdjunto.add("Carta de compromiso");
                itemsDocAdjunto.add("Carta de motivación");
                itemsDocAdjunto.add("Certificado de notas original");
                itemsDocAdjunto.add("Carta de solvencia económica");
                itemsDocAdjunto.add("Pasaporte");
                itemsDocAdjunto.add("Plan de homologación");
            
            //Parctica internacional 
            case 3:
                itemsDocAdjunto.add("Carta de compromiso");
                itemsDocAdjunto.add("Carta de motivación");
                itemsDocAdjunto.add("Certificado de notas original");
                itemsDocAdjunto.add("Carta de solvencia económica");
                itemsDocAdjunto.add("Pasaporte");
                itemsDocAdjunto.add("Plan de homologación");

        }
    }

}
